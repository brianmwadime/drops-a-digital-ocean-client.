﻿using Drops.Api.Common;
using System;
using Drops.Api.Extensions;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api.Models
{
    [DataContract]
    public class TLVector<T> : TLObject, ICollection<T>
        where T : TLObject
    {
        //private int constructor = 0xfda9a7b7;

        public const uint Signature = TLConstructors.TLVector;

        [DataMember]
        public IList<T> Items { get; set; }

        public TLVector()
        {
            Items = new List<T>();
        }

        public TLVector(int count)
        {
            Items = new List<T>(count);
        }

        public T this[int index]
        {
            get { return Items[index]; }
        }

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            var length = GetObject<TLInt>(bytes, ref position);
            Items = new List<T>(length.Value);

            for (var i = 0; i < length.Value; i++)
            {
                Items.Add(GetObject<T>(bytes, ref position));
            }

            return this;
        }

        public override byte[] ToBytes()
        {
            return Utils.Combine(
                Utils.SignatureToBytes(Signature),
                BitConverter.GetBytes(Items.Count),
                Utils.Combine(Items.Select(x => x.ToBytes()).ToArray())
                );
        }

        public override TLObject FromStream(Stream input)
        {
            var length = GetObject<TLInt>(input);
            Items = new List<T>(length.Value);

            for (var i = 0; i < length.Value; i++)
            {
                Items.Add(GetObject<T>(input));
            }

            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(Utils.SignatureToBytes(Signature));
            output.Write(new TLInt(Items.Count).ToBytes());
            for (var i = 0; i < Items.Count; i++)
            {
                Items[i].ToStream(output);
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            Items.Add(item);
        }

        public void Clear()
        {
            Items.Clear();
        }

        public bool Contains(T item)
        {
            return Items.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            Items.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            return Items.Remove(item);
        }

        public void RemoveAt(int index)
        {
            Items.RemoveAt(index);
        }

        public void Insert(int index, T item)
        {
            Items.Insert(index, item);
        }

        public int Count { get { return Items.Count; } }
        public bool IsReadOnly { get { return Items.IsReadOnly; } }

        //public int IndexOf(TLDCOption dcOption)
        //{
        //    for (var i = 0; i < Items.Count; i++)
        //    {
        //        if (Items[i] == dcOption)
        //            return i;
        //    }

        //    return -1;
        //}
    }
}
