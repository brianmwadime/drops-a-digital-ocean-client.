﻿using Drops.Api.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api.Models
{
    public class DropsResponse<T> where T : new()
    {
        public T Response { get; set; }
        public Error Error { get; set; }
    }
}
