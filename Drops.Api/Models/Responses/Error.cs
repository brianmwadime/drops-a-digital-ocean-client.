﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api.Models.Responses
{
    public class Error
    {
        /// <summary>
        /// A unique string ID that can be used to identify and reference an error.
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// The error message returned by digital ocean.
        /// </summary>
        public string message { get; set; }
    }
}
