﻿using Caliburn.Micro;
using Drops.Api.Common;
using Drops.Api.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Drops.Api.Models
{
    [DataContract]
    public abstract class TLObject : INotifyPropertyChanged
    {
        public virtual TLObject FromBytes(byte[] bytes, ref int position)
        {
            throw new NotImplementedException();
        }

        public virtual byte[] ToBytes()
        {
            throw new NotImplementedException();
        }

        public virtual TLObject FromStream(Stream input)
        {
            throw new NotImplementedException();
        }

        public virtual void ToStream(Stream output)
        {
            throw new NotImplementedException();
        }

        public static T GetObject<T>(byte[] bytes, ref int position) where T : TLObject
        {
            //try
            //{
            return (T)TLObjectGenerator.GetObject<T>(bytes, position).FromBytes(bytes, ref position);
            //}
            //catch (Exception e)
            //{
            //    TLUtils.WriteLine(e.StackTrace, LogSeverity.Error);
            //}

            //return null;
        }

        public static T GetObject<T>(Stream input) where T : TLObject
        {
            //try
            //{
            return (T)TLObjectGenerator.GetObject<T>(input).FromStream(input);
            //}
            //catch (Exception e)
            //{
            //    TLUtils.WriteLine(e.StackTrace, LogSeverity.Error);
            //}

            //return null;
        }

        public static T GetNullableObject<T>(Stream input) where T : TLObject
        {
            return TLObjectExtensions.NullableFromStream<T>(input);
        }

        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            NotifyOfPropertyChange(propertyName);
            return true;
        }

        protected bool SetField<T>(ref T field, T value, Expression<Func<T>> selectorExpression)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            NotifyOfPropertyChange(selectorExpression);
            return true;
        }

        /// <summary>
        /// Enables/Disables property change notification.
        /// 
        /// </summary>
        [Browsable(false)]
        public bool IsNotifying { get; set; }

        private WriteableBitmap _bitmap;

        public WriteableBitmap Bitmap
        {
            get { return _bitmap; }
            set { SetField(ref _bitmap, value, () => Bitmap); }
        }

        public void SetBitmap(WriteableBitmap bitmap)
        {
            //if (_bitmap == null)
            //{
            Bitmap = bitmap;
            //}
            //else
            //{
            //    _bitmap = bitmap;
            //}
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = (param0, param1) => { };

        /// <summary>
        /// Creates an instance of <see cref="T:Caliburn.Micro.PropertyChangedBase"/>.
        /// 
        /// </summary>
        public TLObject()
        {
            IsNotifying = true;
        }

        /// <summary>
        /// Raises a change notification indicating that all bindings should be refreshed.
        /// 
        /// </summary>
        public void Refresh()
        {
            NotifyOfPropertyChange(string.Empty);
        }

        /// <summary>
        /// Notifies subscribers of the property change.
        /// 
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        public virtual void NotifyOfPropertyChange(string propertyName)
        {
            if (!IsNotifying)
                return;
            Execute.OnUIThread(() => OnPropertyChanged(new PropertyChangedEventArgs(propertyName)));
        }

        /// <summary>
        /// Notifies subscribers of the property change.
        /// 
        /// </summary>
        /// <typeparam name="TProperty">The type of the property.</typeparam><param name="property">The property expression.</param>
        public void NotifyOfPropertyChange<TProperty>(Expression<Func<TProperty>> property)
        {
            this.NotifyOfPropertyChange(ExtensionMethods.GetMemberInfo((Expression)property).Name);
        }

        /// <summary>
        /// Raises the <see cref="E:PropertyChanged"/> event directly.
        /// 
        /// </summary>
        /// <param name="e">The <see cref="T:System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler changedEventHandler = PropertyChanged;
            if (changedEventHandler == null)
                return;
            changedEventHandler(this, e);
        }

        /// <summary>
        /// Called when the object is deserialized.
        /// 
        /// </summary>
        /// <param name="c">The streaming context.</param>
        [OnDeserialized]
        public void OnDeserialized(StreamingContext c)
        {
            IsNotifying = true;
        }

        /// <summary>
        /// Used to indicate whether or not the IsNotifying property is serialized to Xml.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// Whether or not to serialize the IsNotifying property. The default is false.
        /// </returns>
        public virtual bool ShouldSerializeIsNotifying()
        {
            return false;
        }
    }
}
