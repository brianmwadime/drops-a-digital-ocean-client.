﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api.Models
{
    public static class TLConstructors
    {
        public const uint TLNull = 0x56730bcc;
        public const uint TLDroplet = 0xab3a99ac;
        public const uint TLImage = 0x9efc6326;
        public const uint TLDroplets = 0x15ba6c40;
        public const uint TLVector = 0x1cb5c415;
    }
}
