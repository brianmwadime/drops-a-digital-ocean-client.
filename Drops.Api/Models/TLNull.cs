﻿using Drops.Api.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Drops.Api.Extensions;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api.Models
{
    public class TLNull : TLObject
    {
        public const uint Signature = TLConstructors.TLNull;

        public override TLObject FromBytes(byte[] bytes, ref int position)
        {
            bytes.ThrowExceptionIfIncorrect(ref position, Signature);

            return this;
        }

        public override byte[] ToBytes()
        {
            return Utils.SignatureToBytes(Signature);
        }

        public override TLObject FromStream(Stream input)
        {
            return this;
        }

        public override void ToStream(Stream output)
        {
            output.Write(Utils.SignatureToBytes(Signature));
        }
    }
}
