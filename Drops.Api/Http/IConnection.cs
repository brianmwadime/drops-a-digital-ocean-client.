﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;
using Drops.Api.Models;
using Drops.Api.Models.Responses;

namespace Drops.Api.Http {
    public interface IConnection {
        IRestClient Client { get; }
        IRateLimit Rates { get; }

        Task<IRestResponse> ExecuteRaw(string endpoint, IList<Parameter> parameters, Method method = Method.GET);

        Task<T> ExecuteRequest<T>(string endpoint, IList<Parameter> parameters,
            object data = null, string expectedRoot = null, Method method = Method.GET) where T : new();

        Task<DropsResponse<T>> ExecuteRequestExtra<T>(string endpoint, IList<Parameter> parameters,
            object data = null, string expectedRoot = null, Method method = Method.GET) where T : new();

        Task<IReadOnlyList<T>> GetPaginate<T>(string endpoint, IList<Parameter> parameters,
            string expectedRoot = null) where T : new();

        Task<DropsResponse<T>> GetPaginated<T>(string endpoint, IList<Parameter> parameters,
            string expectedRoot = null) where T : new();
    }
}