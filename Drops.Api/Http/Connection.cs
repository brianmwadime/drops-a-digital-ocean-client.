﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Drops.Api.Extensions;
using Drops.Api.Helpers;
using Drops.Api.Models.Responses;
using RestSharp;
using System.Linq;
using RestSharp.Deserializers;
using Drops.Api.Models;
using Newtonsoft.Json;

namespace Drops.Api.Http {
    public class Connection : IConnection {
        public Connection(IRestClient client) {
            Client = client;
        }

        #region IConnection Members

        public IRestClient Client { get; private set; }
        public IRateLimit Rates { get; private set; }

        public async Task<IRestResponse> ExecuteRaw(string endpoint, IList<Parameter> parameters,
            Method method = Method.GET) {
            var request = BuildRequest(endpoint, parameters);
            request.Method = method;
            return await Client.ExecuteTaskRaw(request).ConfigureAwait(false);
        }

        public async Task<T> ExecuteRequest<T>(string endpoint, IList<Parameter> parameters,
            object data = null, string expectedRoot = null, Method method = Method.GET)
            where T : new() {
            var request = BuildRequest(endpoint, parameters);
            request.RootElement = expectedRoot;
            request.Method = method;

            if (data != null && method != Method.GET) {
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = new JsonNetSerializer();
                request.AddBody(data);
            }

            return await Client.ExecuteTask<T>(request).ConfigureAwait(false);
        }

        public async Task<DropsResponse<TReturnType>> ExecuteRequestExtra<TReturnType>(string endpoint, IList<Parameter> parameters,
            object data = null, string expectedRoot = null, Method method = Method.GET)
            where TReturnType : new()
        {
            var request = BuildRequest(endpoint, parameters);
            request.RootElement = expectedRoot;
            request.Method = method;

            if (data != null && method != Method.GET)
            {
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = new JsonNetSerializer();
                request.AddBody(data);
            }

            return await Client.ExecuteTask<DropsResponse<TReturnType>>(request).ConfigureAwait(false);
        }

        public async Task<IReadOnlyList<T>> GetPaginate<T>(string endpoint, IList<Parameter> parameters,
            string expectedRoot = null) where T : new()
        {
            var first = await ExecuteRaw(endpoint, parameters).ConfigureAwait(false);

            return null;
        }

        //public async Task<DropsResponse<List<T>>> GetPaginate<T>(string endpoint, IList<Parameter> parameters,
        //    string expectedRoot = null) where T : new() {
        //    var first = await ExecuteRaw(endpoint, parameters).ConfigureAwait(false);
        //    return null;
            //return ProcessResponse<List<T>>(endpoint, first, expectedRoot);
            // get page information
            //var deserialize = new JsonDeserializer {
            //    RootElement = "links",
            //    DateFormat = first.Request.DateFormat
            //};
            //var page = deserialize.Deserialize<Pagination>(first);

            //// get initial data
            //deserialize.RootElement = expectedRoot;
            //var data = deserialize.Deserialize<List<T>>(first);

            //// loop until we are finished
            //var allItems = new List<T>(data);
            //while (page != null && page.Pages != null && !String.IsNullOrWhiteSpace(page.Pages.Next)) {
            //    endpoint = page.Pages.Next.Replace(DropsClient.DropsApiUrl, "");
            //    var iter = await ExecuteRaw(endpoint, null).ConfigureAwait(false);

            //    deserialize.RootElement = expectedRoot;
            //    allItems.AddRange(deserialize.Deserialize<List<T>>(iter));

            //    deserialize.RootElement = "links";
            //    page = deserialize.Deserialize<Pagination>(iter);
            //}
            //return new ReadOnlyCollection<T>(allItems);
        //}

        #endregion

        private IRestRequest BuildRequest(string endpoint, IEnumerable<Parameter> parameters) {
            var request = new RestRequest(endpoint) {
                OnBeforeDeserialization = r => { Rates = new RateLimit(r.Headers); }
            };

            if (parameters == null) {
                return request;
            }
            foreach (var parameter in parameters) {
                request.AddParameter(parameter);
            }

            return request;
        }

        public async Task<DropsResponse<TReturnType>> GetPaginated<TReturnType>(string endpoint, IList<Parameter> parameters,
            string expectedRoot = null) where TReturnType : new()
        {
            var response = await ExecuteRaw(endpoint, parameters).ConfigureAwait(false);


            if (string.IsNullOrEmpty(response.Content))
            {
                return new DropsResponse<TReturnType> { Error = new Error { id = "nointernet", message = "No internet connection. Please try again." } };
            }

            if (response.Content.Contains("message"))
            {
                var error = JsonConvert.DeserializeObject<Error>(response.Content);
                return new DropsResponse<TReturnType> { Error = error };
            }

            var deserialize = new JsonDeserializer
            {
                RootElement = "links",
                DateFormat = response.Request.DateFormat
            };

            var page = deserialize.Deserialize<Pagination>(response);
            // get initial data
            deserialize.RootElement = expectedRoot;
            var data = deserialize.Deserialize<TReturnType>(response);

            // loop until we are finished
            var allItems = new DropsResponse<TReturnType> { Response = data };
            while (page != null && page.Pages != null && !String.IsNullOrWhiteSpace(page.Pages.Next))
            {
                endpoint = page.Pages.Next.Replace(DropsClient.DropsApiUrl, "");
                var iter = await ExecuteRaw(endpoint, null).ConfigureAwait(false);

                deserialize.RootElement = expectedRoot;
                //allItems.Response.AddRange(deserialize.Deserialize<TReturnType>(iter));

                deserialize.RootElement = "links";
                page = deserialize.Deserialize<Pagination>(iter);
            }

            return allItems;
            //return new DropsResponse<TReturnType> { Response = allItems };
        }
    }
}