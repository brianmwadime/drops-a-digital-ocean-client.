﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api
{
    public static class Constants
    {
        public const int CommitDBInterval = 3;              //seconds
        public const int GetConfigInterval = 60 * 60;       //seconds
        public const int TimeoutInterval = 25;              //seconds

        public const bool IsLongPollEnabled = false;
        public const int CachedDropletsCount = 20;
        public const int WorkersNumber = 4;

        public const string ConfigKey = "Config";
        public static string StateFileName = "state.dat";
    }
}
