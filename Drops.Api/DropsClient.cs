﻿using Drops.Api.Clients;
using Drops.Api.Http;
using RestSharp;

namespace Drops.Api {
    public class DropsClient : IDropsClient {
        public static readonly string DropsApiUrl = "https://api.digitalocean.com/v2/";
        private readonly IConnection _connection;

        public DropsClient(string token) {
            var client = new RestClient(DropsApiUrl) {
                UserAgent = "Drops-api-dotnet"
            };
            client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", token));

            _connection = new Connection(client);

            Actions = new ActionsClient(_connection);
            DomainRecords = new DomainRecordsClient(_connection);
            Domains = new DomainsClient(_connection);
            DropletActions = new DropletActionsClient(_connection);
            Droplets = new DropletsClient(_connection);
            ImageActions = new ImageActionsClient(_connection);
            Images = new ImagesClient(_connection);
            Keys = new KeysClient(_connection);
            Regions = new RegionsClient(_connection);
            Sizes = new SizesClient(_connection);
        }

        #region IDropsClient Members

        public IRateLimit Rates {
            get { return _connection.Rates; }
        }

        public IActionsClient Actions { get; private set; }
        public IDomainRecordsClient DomainRecords { get; private set; }
        public IDomainsClient Domains { get; private set; }
        public IDropletActionsClient DropletActions { get; private set; }
        public IDropletsClient Droplets { get; private set; }
        public IImageActionsClient ImageActions { get; private set; }
        public IImagesClient Images { get; private set; }
        public IKeysClient Keys { get; private set; }
        public IRegionsClient Regions { get; private set; }
        public ISizesClient Sizes { get; private set; }

        #endregion
    }
}