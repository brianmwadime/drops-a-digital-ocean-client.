﻿using Drops.Api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Drops.Api.Extensions;
using System.Threading.Tasks;
using Drops.Api.Common;

namespace Drops.Api.HexRequest
{
    public static class TLObjectExtensions
    {
        public static void NullableToStream(this TLObject obj, Stream output)
        {
            if (obj == null)
            {
                output.Write(new TLNull().ToBytes());
            }
            else
            {
                obj.ToStream(output);
            }
        }

        public static T NullableFromStream<T>(Stream input) where T : TLObject
        {
            var obj = TLObjectGenerator.GetNullableObject<T>(input);

            if (obj == null) return null;

            return (T)obj.FromStream(input);
        }
    }
}
