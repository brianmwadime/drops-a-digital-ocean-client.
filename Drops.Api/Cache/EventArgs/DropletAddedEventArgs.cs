﻿using Drops.Api.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api.Cache.EventArgs
{
    public class DropletAddedEventArgs
    {
        public Droplet Droplet { get; protected set; }

        public DropletAddedEventArgs(Droplet droplet)
        {
            Droplet = droplet;
        }
    }

    public class DropletRemovedEventArgs
    {
        public Droplet Droplet { get; protected set; }

        public DropletRemovedEventArgs(Droplet droplet)
        {
            Droplet = droplet;
        }
    }

    public class UpdatingEventArgs : System.EventArgs { }

    public class UpdateCompletedEventArgs : System.EventArgs { }
}
