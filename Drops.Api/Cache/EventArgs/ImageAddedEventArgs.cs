﻿using Drops.Api.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api.Cache.EventArgs
{
    public class ImageAddedEventArgs
    {
        public Image Image { get; protected set; }

        public ImageAddedEventArgs(Image image)
        {
            Image = image;
        }
    }

    public class ImageRemovedEventArgs
    {
        public Image Image { get; protected set; }

        public ImageRemovedEventArgs(Image image)
        {
            Image = image;
        }
    }

    public class ImageUpdatingEventArgs : System.EventArgs { }

    public class ImageUpdateCompletedEventArgs : System.EventArgs { }
}
