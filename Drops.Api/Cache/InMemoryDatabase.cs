﻿using Caliburn.Micro;
using Drops.Api.Common;
using Drops.Api.Models;
using Drops.Api.Models.Responses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api.Cache
{
    public class InMemoryDatabase : IDisposable
    {
        private volatile bool _isOpened;

        private const string DropletsServiceFileName = "droplets.dat";

        private const string BackupsServiceFileName = "backups.dat";

        private const string SnapshotsServiceFileName = "snapshots.dat";

        private readonly object _dropletsFileSyncRoot = new object();

        private readonly object _dropletsSyncRoot = new object();

        private readonly object _backupsFileSyncRoot = new object();

        private readonly object _backupsSyncRoot = new object();

        private readonly object _snapshotsFileSyncRoot = new object();

        private readonly object _snapshotsSyncRoot = new object();

        public Context<Droplet> DropletsContext = new Context<Droplet>();

        public Context<Image> BackupsContext = new Context<Image>();

        public Context<Image> SnapshotsContext = new Context<Image>();

        public ObservableCollection<Droplet> Droplets { get; set; }

        public ObservableCollection<Image> Backups { get; set; }

        public ObservableCollection<Image> Snapshots { get; set; }

        private readonly IEventAggregator _eventAggregator;

        private readonly IDisposable _commitSubscription;

        public volatile bool HasChanges;

        public InMemoryDatabase(IEventAggregator eventAggregator)
        {
            var commitEvents = Observable.FromEventPattern<EventHandler, System.EventArgs>(
                keh => { CommitInvoked += keh; },
                keh => { CommitInvoked -= keh; });

            _commitSubscription = commitEvents
                .Throttle(TimeSpan.FromSeconds(Constants.CommitDBInterval))
                .Subscribe(e => CommitInternal());

            //commitEvents.Publish()

           Droplets = new ObservableCollection<Droplet>();
           Backups = new ObservableCollection<Image>();
           Snapshots = new ObservableCollection<Image>();
            _eventAggregator = eventAggregator;
        }

        public event EventHandler CommitInvoked;

        protected virtual void RaiseCommitInvoked()
        {
            var handler = CommitInvoked;
            if (handler != null) handler(this, System.EventArgs.Empty);
        }

        public Droplet GetDroplet(int Id)
        {
            lock (_dropletsSyncRoot)
            {
                foreach (var d in Droplets)
                {
                    if (d.Id.Equals(Id))
                    {
                        return d as Droplet;
                    }
                }

            }

            return null;
        }

        public Image GetBackup(int Id)
        {
            lock (_backupsSyncRoot)
            {
                foreach (var d in Backups)
                {
                    if (d.Id.Equals(Id))
                    {
                        return d as Image;
                    }
                }

            }

            return null;
        }

        public Image GetSnapshot(int Id)
        {
            lock (_snapshotsSyncRoot)
            {
                foreach (var d in Snapshots)
                {
                    if (d.Id.Equals(Id))
                    {
                        return d as Image;
                    }
                }

            }

            return null;
        }

        public void ReplaceDroplet(int index, Droplet droplet)
        {
            var cachedDroplet = DropletsContext[index];
            if (cachedDroplet == null)
            {
                DropletsContext[index] = droplet;
            }
            else
            {
                DropletsContext[index] = droplet;

                lock (_dropletsSyncRoot)
                {
                    foreach (var _droplet in Droplets)
                    {
                        if (_droplet.Id == index)
                        {
                            _droplet.Status = droplet.Status;
                            _droplet.BackupIds = droplet.BackupIds;
                            _droplet.Name = droplet.Name;
                            _droplet.Networks = droplet.Networks;
                            _droplet.Memory = droplet.Memory;
                            _droplet.Locked = droplet.Locked;
                            _droplet.Kernel = droplet.Kernel;
                            _droplet.Region = droplet.Region;
                            _droplet.SizeSlug = droplet.SizeSlug;
                            _droplet.SnapshotIds = droplet.SnapshotIds;
                            _droplet.Vcpus = droplet.Vcpus;
                            _droplet.Disk = droplet.Disk;
                        }
                    }
                }

            }
        }

        public void ReplaceSnapshot(int index, Image image)
        {
            var cachedImage = SnapshotsContext[index];
            if (cachedImage == null)
            {
                SnapshotsContext[index] = image;
            }
            else
            {
                SnapshotsContext[index] = image;

                lock (_snapshotsSyncRoot)
                {
                    foreach (var _image in Snapshots)
                    {
                        if (_image.Id == index)
                        {
                            _image.CreatedAt = image.CreatedAt;
                            _image.Distribution = image.Distribution;
                            _image.MinDiskSize = image.MinDiskSize;
                            _image.Name = image.Name;
                            _image.Public = image.Public;
                            _image.Regions = image.Regions;
                            _image.Slug = image.Slug;
                            _image.Type= image.Type;
                            
                        }
                    }
                }

            }
        }

        public void ReplaceBackup(int index, Image image)
        {
            var cachedImage = BackupsContext[index];
            if (cachedImage == null)
            {
                BackupsContext[index] = image;
            }
            else
            {
                BackupsContext[index] = image;

                lock (_backupsSyncRoot)
                {
                    foreach (var _image in Backups)
                    {
                        if (_image.Id == index)
                        {
                            _image.CreatedAt = image.CreatedAt;
                            _image.Distribution = image.Distribution;
                            _image.MinDiskSize = image.MinDiskSize;
                            _image.Name = image.Name;
                            _image.Public = image.Public;
                            _image.Regions = image.Regions;
                            _image.Slug = image.Slug;
                            _image.Type= image.Type;
                            
                        }
                    }
                }

            }
        }

        public void AddDroplet(Droplet droplet)
        {
            if (droplet != null)
            {
                DropletsContext[droplet.Id] = droplet;

                

                // add in desc order by Date
                var isAdded = false;

                lock (_dropletsSyncRoot)
                {
                    for (var i = 0; i < Droplets.Count; i++)
                    {
                        var d = Droplets[i] as Droplet;
                        if (d != null)
                        {
                                isAdded = true;
                                Droplets.Insert(i, droplet);
                                break;
                            }
                        }
                        
                }

                if (!isAdded)
                {
                    Droplets.Add(droplet);
                }
            }

        }

        public void AddSnapshot(Image image)
        {
            if (image != null)
            {
                SnapshotsContext[image.Id] = image;
                // add in desc order by Date
                var isAdded = false;

                lock (_snapshotsSyncRoot)
                {
                    for (var i = 0; i < Snapshots.Count; i++)
                    {
                        var d = Snapshots[i] as Image;
                        if (d != null)
                        {
                                isAdded = true;
                                Snapshots.Insert(i, image);
                                break;
                            }
                        }
                        
                }

                if (!isAdded)
                {
                    Snapshots.Add(image);
                }
            }

        }

        public void AddBackup(Image image)
        {
            if (image != null)
            {
                BackupsContext[image.Id] = image;
                // add in desc order by Date
                var isAdded = false;

                lock (_backupsSyncRoot)
                {
                    for (var i = 0; i < Backups.Count; i++)
                    {
                        var d = Backups[i] as Image;
                        if (d != null)
                        {
                                isAdded = true;
                                Backups.Insert(i, image);
                                break;
                            }
                        }
                }

                if (!isAdded)
                {
                    Backups.Add(image);
                }
            }

        }

        public void Open()
        {
            try
            {

                lock (_dropletsSyncRoot)
                {

                    var dropletsTimer = Stopwatch.StartNew();
                    var droplets = Utils.OpenObjectFromFile<TLVector<Droplet>>(_dropletsFileSyncRoot, DropletsServiceFileName) ?? new TLVector<Droplet>();
                    Droplets = new ObservableCollection<Droplet>(droplets);
#if DEBUG
                    Debug.WriteLine(string.Format("Open droplets time ({0}) : {1}", droplets.Count, dropletsTimer.Elapsed));
#endif
                    DropletsContext = new Context<Droplet>(Droplets, x => x.Id);
                }

                lock (_snapshotsSyncRoot)
                {

                    var snapshotsTimer = Stopwatch.StartNew();
                    var snapshots = Utils.OpenObjectFromFile<TLVector<Image>>(_snapshotsFileSyncRoot, SnapshotsServiceFileName) ?? new TLVector<Image>();
                    Snapshots = new ObservableCollection<Image>(snapshots);
#if DEBUG
                    Debug.WriteLine(string.Format("Open snapshots time ({0}) : {1}", snapshots.Count, snapshotsTimer.Elapsed));
#endif
                    SnapshotsContext = new Context<Image>(Snapshots, x => x.Id);
                }

                lock (_backupsSyncRoot)
                {

                    var backupsTimer = Stopwatch.StartNew();
                    var backups = Utils.OpenObjectFromFile<TLVector<Image>>(_backupsFileSyncRoot, BackupsServiceFileName) ?? new TLVector<Image>();
                    Backups = new ObservableCollection<Image>(backups);
#if DEBUG
                    Debug.WriteLine(string.Format("Open backups time ({0}) : {1}", backups.Count, backupsTimer.Elapsed));
#endif

                    BackupsContext = new Context<Image>(Backups, x => x.Id);
                }

                _isOpened = true;

            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine("DB ERROR: open DB error");
                Debug.WriteLine(e);
#endif
            }
        }

        public void Commit()
        {
            RaiseCommitInvoked();
            HasChanges = true;
        }

        public void CommitInternal()
        {
            if (!_isOpened) return;

            var timer = Stopwatch.StartNew();

            //truncate commiting DB
            List<Droplet> droplets;
            lock (_dropletsFileSyncRoot)
            {
                droplets = Droplets.ToList();
                droplets = Droplets.OrderByDescending(x => x.CreatedAt).Take(Constants.CachedDropletsCount).ToList();
                foreach (var d in droplets)
                {
                    var droplet = d as Droplet;
                }
            }

            var serviceDroplets = new TLVector<Droplet> { Items = droplets };
            Utils.SaveObjectToFile(_dropletsFileSyncRoot, DropletsServiceFileName, new TLVector<Droplet> { Items = DropletsContext.Values.ToList() });
            Utils.SaveObjectToFile(_backupsFileSyncRoot, BackupsServiceFileName, new TLVector<Image> { Items = BackupsContext.Values.ToList() });
            Utils.SaveObjectToFile(_snapshotsFileSyncRoot, SnapshotsServiceFileName, new TLVector<Image> { Items = SnapshotsContext.Values.ToList() });

            HasChanges = false;

#if DEBUG
            Debug.WriteLine("Commit DB time ({0}d, {1}b, {2}s): {3}", DropletsContext.Count, BackupsContext.Count, SnapshotsContext.Count, timer.Elapsed);
#endif
        }

        public int CountRecords<T>() where T : TLObject
        {
            var result = 0;
            if (typeof(T) == typeof(Droplet))
            {
                result += Droplets.Count;
            }
            else if(typeof(T) == typeof(Image))
            {
                result += Backups.Count;
            }
            else if (typeof(T) == typeof(Image))
            {
                result += Snapshots.Count;
            }
            else
            {
                throw new NotImplementedException();
            }

            return result;
        }

        public void Dispose()
        {
            _commitSubscription.Dispose();
        }

        public void Clear()
        {
            var timer = Stopwatch.StartNew();

            Droplets.Clear();

            DropletsContext.Clear();
            ClearInternal(_dropletsFileSyncRoot, DropletsServiceFileName);
            ClearInternal(_backupsFileSyncRoot, BackupsServiceFileName);
            ClearInternal(_snapshotsFileSyncRoot, SnapshotsServiceFileName);

            Debug.WriteLine("Clear DB time: " + timer.Elapsed);
        }

        private void ClearInternal(object syncRoot, string fileName)
        {
            try
            {
                lock (syncRoot)
                {
                    var file = IsolatedStorageFile.GetUserStoreForApplication();

                    file.DeleteFile(fileName);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("DB ERROR: cannot clear " + fileName);
            }
        }

        public void DeleteDroplet(Droplet droplet)
        {
            lock (_dropletsSyncRoot)
            {
                var dbDialog = Droplets.FirstOrDefault(x => x.Id == droplet.Id);

                Droplets.Remove(dbDialog);
            }
        }

        public void DeleteSnapshot(Image image)
        {
            lock (_snapshotsSyncRoot)
            {
                var dbSnapshot = Snapshots.FirstOrDefault(x => x.Id == image.Id);

                Snapshots.Remove(dbSnapshot);
            }
        }

        public void DeleteBackup(Image image)
        {
            lock (_backupsSyncRoot)
            {
                var dbBackup = Backups.FirstOrDefault(x => x.Id == image.Id);

                Backups.Remove(dbBackup);
            }
        }
    }
    
}
