﻿using Caliburn.Micro;
using Drops.Api.Models.Responses;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Drops.Api.Extensions;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Drops.Api.Cache
{
    public class InMemoryCacheService : ICacheService
    {
        private readonly object _databaseSyncRoot = new object();

        private InMemoryDatabase _database;

        private Context<Droplet> DropletsContext
        {
            get { return _database != null ? _database.DropletsContext : null; }
        }

        private Context<Image> SnapshotsContext
        {
            get { return _database != null ? _database.SnapshotsContext : null; }
        }

        private Context<Image> BackupsContext
        {
            get { return _database != null ? _database.BackupsContext : null; }
        }

        public void Init()
        {
            _database = new InMemoryDatabase(_eventAggregator);
            _database.Open();
        }

        private readonly IEventAggregator _eventAggregator;

        public ExceptionInfo LastSyncBackupException { get; set; }

        public ExceptionInfo LastSyncSnapshotException { get; set; }

        public ExceptionInfo LastSyncDropletException { get; set; }

        public InMemoryCacheService(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
        }

        public IList<Droplet> GetDroplets()
        {
            var result = new List<Droplet>();

            if (_database == null) Init();

            if (DropletsContext == null)
            {

                return result;
            }
            var timer = Stopwatch.StartNew();

            IList<Droplet> droplets = new ObservableCollection<Droplet>();

            try
            {
                droplets = _database.Droplets;

            }
            catch (Exception e)
            {
                Debug.WriteLine("DB ERROR:");
                Debug.WriteLine(e.ToString());
            }

            //TLUtils.WritePerformance(string.Format("GetCachedDialogs time ({0} from {1}): {2}", dialogs.Count, _database.CountRecords<TLDialog>(), timer.Elapsed));
            return droplets.OrderByDescending(x => x.CreatedAt).ToList();
        }

        public IList<Image> GetSnapshots()
        {
            var result = new List<Image>();

            if (_database == null) Init();

            if (SnapshotsContext == null)
            {

                return result;
            }
            var timer = Stopwatch.StartNew();

            IList<Image> snapshots = new ObservableCollection<Image>();

            try
            {
                snapshots = _database.Snapshots;

            }
            catch (Exception e)
            {
                Debug.WriteLine("DB ERROR:");
                Debug.WriteLine(e.ToString());
            }

            //TLUtils.WritePerformance(string.Format("GetCachedDialogs time ({0} from {1}): {2}", dialogs.Count, _database.CountRecords<TLDialog>(), timer.Elapsed));
            return snapshots.OrderByDescending(x => x.CreatedAt).ToList();
        }

        public IList<Image> GetBackups()
        {
            var result = new List<Image>();

            if (_database == null) Init();

            if (BackupsContext == null)
            {

                return result;
            }
            var timer = Stopwatch.StartNew();

            IList<Image> backups = new ObservableCollection<Image>();

            try
            {
                backups = _database.Backups;

            }
            catch (Exception e)
            {
                Debug.WriteLine("DB ERROR:");
                Debug.WriteLine(e.ToString());
            }

            //TLUtils.WritePerformance(string.Format("GetCachedDialogs time ({0} from {1}): {2}", dialogs.Count, _database.CountRecords<TLDialog>(), timer.Elapsed));
            return backups.OrderByDescending(x => x.CreatedAt).ToList();
        }

        public void ClearAsync(System.Action callback = null)
        {
            ThreadPool.QueueUserWorkItem(
                state =>
                {
                    lock (_databaseSyncRoot)
                    {
                        if (_database != null) _database.Clear();
                    }
                    callback.SafeInvoke();
                });
        }

        public void SyncDroplets(Droplet droplet, Action<Droplet> callback)
        {
            if (droplet == null)
            {
                callback(new Droplet());
                return;
            }


            //var result = droplet;
            var result = new Droplet();
            if (_database == null) Init();


            var timer = Stopwatch.StartNew();
            //SyncMessagesInternal(dialogs.Messages, result.Messages);
            SyncDropletsInternal(droplet, result);
            //TLUtils.WritePerformance("Dialogs:: sync dialogs " + timer.Elapsed);


            _database.Commit();

            Debug.WriteLine("SyncDroplets time: " + timer.Elapsed);
            callback.SafeInvoke(result);
        }

        public void SyncSnapshots(Image snapshot, Action<Image> callback)
        {
            if (snapshot == null)
            {
                callback(new Image());
                return;
            }


            //var result = droplet;
            var result = new Image();
            if (_database == null) Init();


            var timer = Stopwatch.StartNew();
            //SyncMessagesInternal(dialogs.Messages, result.Messages);
            SyncSnapshotsInternal(snapshot, result);
            //TLUtils.WritePerformance("Dialogs:: sync dialogs " + timer.Elapsed);


            _database.Commit();

            Debug.WriteLine("SyncSnapshots time: " + timer.Elapsed);
            callback.SafeInvoke(result);
        }

        public void SyncBackups(Image backup, Action<Image> callback)
        {
            if (backup == null)
            {
                callback(new Image());
                return;
            }


            //var result = droplet;
            var result = new Image();
            if (_database == null) Init();


            var timer = Stopwatch.StartNew();
            //SyncMessagesInternal(dialogs.Messages, result.Messages);
            SyncBackupsInternal(backup, result);
            //TLUtils.WritePerformance("Dialogs:: sync dialogs " + timer.Elapsed);


            _database.Commit();

            Debug.WriteLine("SyncBackups time: " + timer.Elapsed);
            callback.SafeInvoke(result);
        }

        private void SyncDropletsInternal(Droplet droplet, Droplet result)
        {
            // set TopMessage properties
            var timer = Stopwatch.StartNew();
            //MergeMessagesAndDialogs(dialogs);
            //TLUtils.WritePerformance("Dialogs:: merge dialogs and messages " + timer.Elapsed);
            Droplet cachedDroplet = null;
            if (DropletsContext != null)
            {
                cachedDroplet = DropletsContext[droplet.Id] as Droplet;
            }

            if (cachedDroplet != null)
            {
                cachedDroplet = droplet;

                _database.ReplaceDroplet(droplet.Id, droplet);
                result = droplet;
            }
            else
            {
                result = droplet;
                _database.AddDroplet(droplet);

            }
           
            // add object to cache
            //TLUtils.WritePerformance("Dialogs:: foreach dialogs " + timer.Elapsed);
        }

        private void SyncBackupsInternal(Image image, Image result)
        {
            var timer = Stopwatch.StartNew();
            Image cachedBackup = null;
            if (BackupsContext != null)
            {
                cachedBackup = BackupsContext[image.Id] as Image;
            }

            if (cachedBackup != null)
            {
                cachedBackup = image;

                _database.ReplaceBackup(image.Id, image);
                result = image;
            }
            else
            {
                result = image;
                _database.AddBackup(image);

            }

            // add object to cache
#if DEBUG
            Debug.WriteLine("Backups:: foreach backups " + timer.Elapsed);
#endif
        }

        private void SyncSnapshotsInternal(Image image, Image result)
        {
            var timer = Stopwatch.StartNew();
            Image cachedSnapshot = null;
            if (SnapshotsContext != null)
            {
                cachedSnapshot = SnapshotsContext[image.Id] as Image;
            }

            if (cachedSnapshot != null)
            {
                cachedSnapshot = image;

                _database.ReplaceSnapshot(image.Id, image);
                result = image;
            }
            else
            {
                result = image;
                _database.AddSnapshot(image);

            }

            // add object to cache
#if DEBUG
            Debug.WriteLine("Snapshots:: foreach snapshots " + timer.Elapsed);
#endif
        }

        public void GetDropletsAsync(Action<IList<Droplet>> callback)
        {
            ThreadPool.QueueUserWorkItem(
                state =>
                {
                    var result = new List<Droplet>();

                    if (_database == null) Init();

                    if (DropletsContext == null)
                    {
                        callback(result);
                        return;
                    }
                    var timer = Stopwatch.StartNew();

                    IList<Droplet> droplets = new ObservableCollection<Droplet>();

                    try
                    {
                        droplets = _database.Droplets;

                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("DB ERROR:");
                        Debug.WriteLine(e.ToString());
                    }

                    //TLUtils.WritePerformance(string.Format("GetCachedDialogs time ({0} from {1}): {2}", dialogs.Count, _database.CountRecords<TLDialog>(), timer.Elapsed));
                    callback(droplets.OrderByDescending(x => x.CreatedAt).ToList());
                });
        }

        public void GetBackupsAsync(Action<IList<Image>> callback)
        {
            ThreadPool.QueueUserWorkItem(
                state =>
                {
                    var result = new List<Image>();

                    if (_database == null) Init();

                    if (BackupsContext == null)
                    {
                        callback(result);
                        return;
                    }
                    var timer = Stopwatch.StartNew();

                    IList<Image> images = new ObservableCollection<Image>();

                    try
                    {
                        images = _database.Backups;

                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("DB ERROR:");
                        Debug.WriteLine(e.ToString());
                    }

                    //TLUtils.WritePerformance(string.Format("GetCachedDialogs time ({0} from {1}): {2}", dialogs.Count, _database.CountRecords<TLDialog>(), timer.Elapsed));
                    callback(images.OrderByDescending(x => x.CreatedAt).ToList());
                });
        }

        public void GetSnapshotsAsync(Action<IList<Image>> callback)
        {
            ThreadPool.QueueUserWorkItem(
                state =>
                {
                    var result = new List<Image>();

                    if (_database == null) Init();

                    if (SnapshotsContext == null)
                    {
                        callback(result);
                        return;
                    }
                    var timer = Stopwatch.StartNew();

                    IList<Image> images = new ObservableCollection<Image>();

                    try
                    {
                        images = _database.Snapshots;

                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine("DB ERROR:");
                        Debug.WriteLine(e.ToString());
                    }

                    //TLUtils.WritePerformance(string.Format("GetCachedDialogs time ({0} from {1}): {2}", dialogs.Count, _database.CountRecords<TLDialog>(), timer.Elapsed));
                    callback(images.OrderByDescending(x => x.CreatedAt).ToList());
                });
        }

        public void ReplaceDroplet(Droplet droplet)
        {
            if (droplet != null)
            {
                _database.ReplaceDroplet(droplet.Id,droplet);

                _database.Commit();
            }
        }

        public void ReplaceSnapshot(Image image)
        {
            if (image != null)
            {
                _database.ReplaceSnapshot(image.Id, image);

                _database.Commit();
            }
        }

        public void ReplaceBackup(Image image)
        {
            if (image != null)
            {
                _database.ReplaceBackup(image.Id, image);

                _database.Commit();
            }
        }

        public Droplet GetDroplet(int Id)
        {
            return _database.GetDroplet(Id);
        }

        public Image GetSnapshot(int Id)
        {
            return _database.GetSnapshot(Id);
        }

        public Image GetBackup(int Id)
        {
            return _database.GetBackup(Id);
        }

        public void DeleteDroplet(Droplet droplet)
        {
            if (droplet != null)
            {
                _database.DeleteDroplet(droplet);

                _database.Commit();
            }
        }

        public void DeleteSnapshot(Image image)
        {
            if (image != null)
            {
                _database.DeleteSnapshot(image);

                _database.Commit();
            }
        }

        public void DeleteBackup(Image image)
        {
            if (image != null)
            {
                _database.DeleteBackup(image);

                _database.Commit();
            }
        }

        public void Commit()
        {
            if (_database != null)
            {
                _database.Commit();
            }
        }

        public bool TryCommit()
        {
            if (_database != null && _database.HasChanges)
            {
                _database.CommitInternal();
                return true;
            }

            return false;
        }
    }
}
