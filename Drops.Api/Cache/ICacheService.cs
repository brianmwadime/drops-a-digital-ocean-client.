﻿using Drops.Api.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api.Cache
{
    public interface ICacheService
    {
        ExceptionInfo LastSyncDropletException { get; }

        void Commit();
        bool TryCommit();

        void GetDropletsAsync(Action<IList<Droplet>> callback);
        IList<Droplet> GetDroplets();

        void GetSnapshotsAsync(Action<IList<Image>> callback);
        IList<Image> GetSnapshots();

        void GetBackupsAsync(Action<IList<Image>> callback);
        IList<Image> GetBackups();

        void ClearAsync(System.Action callback = null);

        void SyncDroplets(Droplet droplet, Action<Droplet> callback);

        void DeleteDroplet(Droplet droplet);

        void ReplaceDroplet(Droplet droplet);

        Droplet GetDroplet(int id);

        void SyncSnapshots(Image image, Action<Image> callback);

        void DeleteSnapshot(Image image);

        void ReplaceSnapshot(Image image);

        Image GetSnapshot(int id);

        void SyncBackups(Image image, Action<Image> callback);

        void DeleteBackup(Image image);

        void ReplaceBackup(Image image);

        Image GetBackup(int id);

        void Init();
    }

    public class ExceptionInfo
    {
        public Exception Exception { get; set; }

        public DateTime Timestamp { get; set; }

        public string Caption { get; set; }

        public override string ToString()
        {
            return string.Format("Caption={2}\nTimestamp={0}\nException={1}", Timestamp, Exception, Caption);
        }
    }
}
