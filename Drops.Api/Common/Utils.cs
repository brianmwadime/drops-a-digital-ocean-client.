﻿using Drops.Api.Helpers;
using Drops.Api.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api.Common
{
    public static partial class Utils
    {

        public static bool DeleteFile(object syncRoot, string fileName)
        {
            try
            {
                lock (syncRoot)
                {
                    var storage = IsolatedStorageFile.GetUserStoreForApplication();

                    if (storage.FileExists(fileName))
                    {
                        storage.DeleteFile(fileName);
                    }
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine("FILE ERROR: cannot delete file " + fileName);
                Debug.WriteLine(e);
#endif

                return false;
            }
            return true;
        }

        public static T OpenObjectFromFile<T>(object syncRoot, string fileName)
            where T : class
        {
            try
            {
                lock (syncRoot)
                {
                    var file = IsolatedStorageFile.GetUserStoreForApplication();

                    using (var fileStream = new IsolatedStorageFileStream(fileName, FileMode.OpenOrCreate, file))
                    {
                        if (fileStream.Length > 0)
                        {
                            var serializer = new DataContractSerializer(typeof(T));
                            return serializer.ReadObject(fileStream) as T;
                        }

                    }
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine("FILE ERROR: cannot read " + typeof(T) + " from file " + fileName);
                Debug.WriteLine(e);
#endif
            }
            return default(T);
        }

        public static void SaveObjectToFile<T>(object syncRoot, string fileName, T data)
        {
            try
            {
                lock (syncRoot)
                {
                    var file = IsolatedStorageFile.GetUserStoreForApplication();

                    using (var fileStream = new IsolatedStorageFileStream(fileName, FileMode.Create, file))
                    {
                        var dcs = new DataContractSerializer(typeof(T));
                        dcs.WriteObject(fileStream, data);
                    }
                }
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine("FILE ERROR: cannot write " + typeof(T) + " to file " + fileName);
                Debug.WriteLine(e);
#endif
            }
        }

        public static bool InsertItem<T>(IList<T> items, T item, Func<T, long> getField, Func<T, long> equalitysField = null)
            where T : TLObject
        {
            var fieldValue = getField(item);
            for (var i = 0; i < items.Count; i++)
            {
                if (getField(items[i]) > fieldValue)
                {
                    items.Insert(i, item);
                    return true;
                }
                if (getField(items[i]) == fieldValue
                    && equalitysField != null
                    && equalitysField(items[i]) == equalitysField(item))
                {
                    return false;
                }
            }

            items.Add(item);
            return true;
        }

        public static bool InsertItemByDesc<T>(IList<T> items, T item, Func<T, long> getField, Func<T, long> equalityField = null)
            where T : TLObject
        {
            var fieldValue = getField(item);
            for (var i = 0; i < items.Count; i++)
            {
                if (getField(items[i]) < fieldValue)
                {
                    items.Insert(i, item);
                    return true;
                }
                if (getField(items[i]) == fieldValue
                    && equalityField != null
                    && equalityField(items[i]) == equalityField(item))
                {
                    return false;
                }
            }

            items.Add(item);
            return true;
        }

        public static int MergeItemsDesc<T>(Func<T, int> indexFunc, IList<T> current, IList<T> updated, int offset, int maxId, int count, out IList<T> removedItems, Func<T, int> skipFunc = null)
        {
            removedItems = new List<T>();

            var currentIndex = 0;
            var updatedIndex = 0;


            //skip just added or sending items
            while (updatedIndex < updated.Count
                && currentIndex < current.Count
                && indexFunc(updated[updatedIndex]) < indexFunc(current[currentIndex]))
            {
                currentIndex++;
            }

            // insert before current items
            while (updatedIndex < updated.Count
                && (current.Count < currentIndex
                    || (currentIndex < current.Count && indexFunc(updated[updatedIndex]) > indexFunc(current[currentIndex]))))
            {
                if (indexFunc(current[currentIndex]) == 0)
                {
                    currentIndex++;
                    continue;
                }

                current.Insert(currentIndex, updated[updatedIndex]);
                updatedIndex++;
                currentIndex++;
            }

            // update existing items
            if (updatedIndex < updated.Count)
            {
                for (; currentIndex < current.Count; currentIndex++)
                {
                    if (skipFunc != null
                        && skipFunc(current[currentIndex]) == 0)
                    {
                        currentIndex++;
                        continue;
                    }

                    for (; updatedIndex < updated.Count; updatedIndex++)
                    {
                        // missing item at current list
                        if (indexFunc(updated[updatedIndex]) > indexFunc(current[currentIndex]))
                        {
                            current.Insert(currentIndex, updated[updatedIndex]);
                            updatedIndex++;
                            break;
                        }
                        // equal item
                        if (indexFunc(updated[updatedIndex]) == indexFunc(current[currentIndex]))
                        {
                            updatedIndex++;
                            break;
                        }
                        // deleted item
                        if (indexFunc(updated[updatedIndex]) < indexFunc(current[currentIndex]))
                        {
                            var removedItem = current[currentIndex];
                            removedItems.Add(removedItem);
                            current.RemoveAt(currentIndex);
                            currentIndex--;
                            break;
                        }
                    }

                    // at the end of updated list
                    if (updatedIndex == updated.Count)
                    {
                        currentIndex++;
                        break;
                    }
                }
            }


            // all other items were deleted
            if (updated.Count < count && current.Count != currentIndex)
            {
                for (var i = current.Count - 1; i >= updatedIndex; i--)
                {
                    current.RemoveAt(i);
                }
                return currentIndex - 1;
            }

            // add after current items
            while (updatedIndex < updated.Count)
            {
                current.Add(updated[updatedIndex]);
                updatedIndex++;
                currentIndex++;
            }

            return currentIndex - 1;
        }
 


        public static byte[] Combine(params byte[][] arrays)
        {
            var length = 0;
            for (int i = 0; i < arrays.Length; i++)
            {
                length += arrays[i].Length;
            }

            var result = new byte[length]; ////[arrays.Sum(a => a.Length)];
            var offset = 0;
            foreach (var array in arrays)
            {
                Buffer.BlockCopy(array, 0, result, offset, array.Length);
                offset += array.Length;
            }
            return result;
        }

        public static string Signature<T>(this T obj)
            where T : TLObject
        {
            var attr = typeof(T).GetCustomAttributes(typeof(SignatureAttribute), true);
            return ((SignatureAttribute)attr[0]).Value;
        }

        public static DateTime ToDateTime(TLInt date)
        {
            var ticks = date.Value;
            return Utils.UnixTimestampToDateTime(ticks >> 32);
        }

        public static void ThrowNotSupportedException(this byte[] bytes, string objectType)
        {
            throw new NotSupportedException(String.Format("Not supported {0} signature: {1}", objectType, BitConverter.ToString(bytes.SubArray(0, 4))));
        }

        public static void ThrowNotSupportedException(this byte[] bytes, int position, string objectType)
        {
            throw new NotSupportedException(String.Format("Not supported {0} signature: {1}", objectType, BitConverter.ToString(bytes.SubArray(position, position + 4))));
        }

        public static void ThrowExceptionIfIncorrect(this byte[] bytes, ref int position, uint signature)
        {
            //if (!bytes.SubArray(position, 4).StartsWith(signature))
            //{
            //    throw new ArgumentException(String.Format("Incorrect signature: actual - {1}, expected - {0}", SignatureToBytesString(signature), BitConverter.ToString(bytes.SubArray(0, 4))));
            //}
            position += 4;
        }

        public static void ThrowExceptionIfIncorrect(this byte[] bytes, ref int position, string signature)
        {
            //if (!bytes.SubArray(position, 4).StartsWith(signature))
            //{
            //    throw new ArgumentException(String.Format("Incorrect signature: actual - {1}, expected - {0}", SignatureToBytesString(signature), BitConverter.ToString(bytes.SubArray(0, 4))));
            //}
            position += 4;
        }

        private static bool StartsWith(this byte[] array, byte[] startArray)
        {
            for (var i = 0; i < startArray.Length; i++)
            {
                if (array[i] != startArray[i]) return false;
            }
            return true;
        }

        private static bool StartsWith(this byte[] array, int position, byte[] startArray)
        {
            for (var i = 0; i < startArray.Length; i++)
            {
                if (array[position + i] != startArray[i]) return false;
            }
            return true;
        }

        public static bool StartsWith(this byte[] bytes, uint signature)
        {
            var sign = BitConverter.ToUInt32(bytes, 0);

            return sign == signature;
        }

        public static bool StartsWith(this Stream input, uint signature)
        {
            var bytes = new byte[4];
            input.Read(bytes, 0, 4);
            var sign = BitConverter.ToUInt32(bytes, 0);

            return sign == signature;
        }

        public static bool StartsWith(this byte[] bytes, string signature)
        {
            if (signature[0] != '#') throw new ArgumentException("Incorrect first symbol of signature: expexted - #, actual - " + signature[0]);

            var signatureBytes = SignatureToBytes(signature);

            return bytes.StartsWith(signatureBytes);
        }

        public static bool StartsWith(this byte[] bytes, int position, uint signature)
        {
            var sign = BitConverter.ToUInt32(bytes, position);

            return sign == signature;
        }

        public static bool StartsWith(this byte[] bytes, int position, string signature)
        {
            if (signature[0] != '#') throw new ArgumentException("Incorrect first symbol of signature: expexted - #, actual - " + signature[0]);

            var signatureBytes = SignatureToBytes(signature);

            return bytes.StartsWith(position, signatureBytes);
        }

        public static string SignatureToBytesString(string signature)
        {
            if (signature[0] != '#') throw new ArgumentException("Incorrect first symbol of signature: expexted - #, actual - " + signature[0]);

            return BitConverter.ToString(SignatureToBytes(signature));
        }

        public static byte[] SignatureToBytes(uint signature)
        {
            return BitConverter.GetBytes(signature);
        }

        public static byte[] SignatureToBytes(string signature)
        {
            if (signature[0] != '#') throw new ArgumentException("Incorrect first symbol of signature: expexted - #, actual - " + signature[0]);

            var bytesString =
                signature.Length % 2 == 0 ?
                new string(signature.Replace("#", "0").ToArray()) :
                new string(signature.Replace("#", String.Empty).ToArray());

            var bytes = Utils.StringToByteArray(bytesString);
            Array.Reverse(bytes);
            return bytes;
        }

        public static double DateTimeToUnixTimestamp(DateTime dateTime)
        {
            // From local DateTime to UTC0 UnixTime

            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            DateTime.SpecifyKind(dtDateTime, DateTimeKind.Utc);

            return (dateTime.ToUniversalTime() - dtDateTime).TotalSeconds;
        }

        public static DateTime UnixTimestampToDateTime(double unixTimeStamp)
        {
            // From UTC0 UnixTime to local DateTime

            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            DateTime.SpecifyKind(dtDateTime, DateTimeKind.Utc);

            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length / 2;
            byte[] bytes = new byte[NumberChars];
            StringReader sr = new StringReader(hex);
            for (int i = 0; i < NumberChars; i++)
                bytes[i] = Convert.ToByte(new string(new char[2] { (char)sr.Read(), (char)sr.Read() }), 16);
            sr.Dispose();
            return bytes;
        }

        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            var result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

    }
}
