﻿using Drops.Api.Models;
using Drops.Api.Models.Responses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api.Common
{
    class TLObjectGenerator
    {
        private static readonly Dictionary<Type, Func<TLObject>> _baredTypes =
            new Dictionary<Type, Func<TLObject>>
                {
                    {typeof (TLInt), () => new TLInt()},

                };

        private static readonly Dictionary<uint, Func<TLObject>> _clothedTypes =
            new Dictionary<uint, Func<TLObject>>
                {       
                    {Droplet.Signature, () => new Droplet()},
                
                };

        public static TimeSpan ElapsedClothedTypes;

        public static TimeSpan ElapsedBaredTypes;

        public static TimeSpan ElapsedVectorTypes;

        public static T GetObject<T>(byte[] bytes, int position) where T : TLObject
        {

            //var stopwatch = Stopwatch.StartNew();

            // bared types


            var stopwatch2 = Stopwatch.StartNew();
            try
            {

                if (_baredTypes.ContainsKey(typeof(T)))
                {
                    return (T)_baredTypes[typeof(T)].Invoke();
                }
            }
            finally
            {
                ElapsedBaredTypes += stopwatch2.Elapsed;
            }

            var stopwatch = Stopwatch.StartNew();
            uint signature;
            try
            {
                // clothed types
                //var signatureBytes = bytes.SubArray(position, 4);
                //Array.Reverse(signatureBytes);
                signature = BitConverter.ToUInt32(bytes, position);
                Func<TLObject> getInstance;


                // exact matching
                if (_clothedTypes.TryGetValue(signature, out getInstance))
                {
                    return (T)getInstance.Invoke();
                }


                //// matching with removed leading 0
                //while (signature.StartsWith("0"))
                //{
                //    signature = signature.Remove(0, 1);
                //    if (_clothedTypes.TryGetValue("#" + signature, out getInstance))
                //    {
                //        return (T)getInstance.Invoke();
                //    }
                //}
            }
            finally
            {
                ElapsedClothedTypes += stopwatch.Elapsed;
            }




            var stopwatch3 = Stopwatch.StartNew();
            //throw new Exception("Signature exception");
            try
            {
                // TLVector
                if (bytes.StartsWith(position, TLConstructors.TLVector))
                {

                        return (T)Activator.CreateInstance(typeof(T));
                    
                }

            }
            finally
            {
                ElapsedVectorTypes += stopwatch3.Elapsed;
            }

            if (typeof(T) == typeof(TLObject))
            {
                Debug.WriteLine("  ERROR TLObjectGenerator: Cannot find signature #" + BitConverter.ToString(BitConverter.GetBytes(signature)).Replace("-", string.Empty).ToLowerInvariant() + " (" + signature + ")");
            }
            else
            {
                Debug.WriteLine("  ERROR TLObjectGenerator: Incorrect signature #" + BitConverter.ToString(BitConverter.GetBytes(signature)).Replace("-", string.Empty).ToLowerInvariant() + " (" + signature + ")" + " for type " + typeof(T));
            }
            Debug.WriteLine("Position");

            return null;
        }

        public static T GetNullableObject<T>(Stream input) where T : TLObject
        {
            // clothed types
            var signatureBytes = new byte[4];
            input.Read(signatureBytes, 0, 4);
            uint signature = BitConverter.ToUInt32(signatureBytes, 0);

            if (signature == TLNull.Signature) return null;

            input.Position = input.Position - 4;
            return GetObject<T>(input);
        }

        public static T GetObject<T>(Stream input) where T : TLObject
        {

            //var stopwatch = Stopwatch.StartNew();

            // bared types


            var stopwatch2 = Stopwatch.StartNew();
            try
            {

                if (_baredTypes.ContainsKey(typeof(T)))
                {
                    return (T)_baredTypes[typeof(T)].Invoke();
                }
            }
            finally
            {
                ElapsedBaredTypes += stopwatch2.Elapsed;
            }

            var stopwatch = Stopwatch.StartNew();
            uint signature;
            try
            {
                // clothed types
                var signatureBytes = new byte[4];
                input.Read(signatureBytes, 0, 4);
                signature = BitConverter.ToUInt32(signatureBytes, 0);
                Func<TLObject> getInstance;


                // exact matching
                if (_clothedTypes.TryGetValue(signature, out getInstance))
                {
                    return (T)getInstance.Invoke();
                }
            }
            finally
            {

                ElapsedClothedTypes += stopwatch.Elapsed;
            }




            var stopwatch3 = Stopwatch.StartNew();
            //throw new Exception("Signature exception");
            try
            {
                // TLVector
                if (signature == TLConstructors.TLVector)
                {
                    //TODO: remove workaround for TLRPCRESULT: TLVECTOR<TLINT>
                    if (typeof(T) == typeof(TLObject))
                    {
                        Debug.WriteLine("TLVecto<TLInt>  hack ");
                        return (T)Activator.CreateInstance(typeof(TLVector<TLInt>));
                    }
                    else
                    {
                        return (T)Activator.CreateInstance(typeof(T));
                    }
                }

            }
            finally
            {
                ElapsedVectorTypes += stopwatch3.Elapsed;
            }

            if (typeof(T) == typeof(TLObject))
            {
                Debug.WriteLine("  ERROR TLObjectGenerator: Cannot find signature #" + BitConverter.ToString(BitConverter.GetBytes(signature)).Replace("-", string.Empty).ToLowerInvariant() + " (" + signature + ")");
            }
            else
            {
                Debug.WriteLine("  ERROR TLObjectGenerator: Incorrect signature #" + BitConverter.ToString(BitConverter.GetBytes(signature)).Replace("-", string.Empty).ToLowerInvariant() + " (" + signature + ")" + " for type " + typeof(T));
            }
            Debug.WriteLine("Position");

            return null;
        }

        // Instead of visiting each field of stackFrame,
        // the StackFrame.ToString() method could be used, 
        // but the returned text would not include the class name.
        private static String MethodCallLog(System.Diagnostics.StackFrame p_MethodCall)
        {
            System.Text.StringBuilder l_MethodCallLog =
                        new System.Text.StringBuilder();

            var l_Method = p_MethodCall.GetMethod();
            l_MethodCallLog.Append(l_Method.DeclaringType.ToString());
            l_MethodCallLog.Append(".");
            l_MethodCallLog.Append(p_MethodCall.GetMethod().Name);

            var l_MethodParameters = l_Method.GetParameters();
            l_MethodCallLog.Append("(");
            for (Int32 x = 0; x < l_MethodParameters.Length; ++x)
            {
                if (x > 0)
                    l_MethodCallLog.Append(", ");
                var l_MethodParameter = l_MethodParameters[x];
                l_MethodCallLog.Append(l_MethodParameter.ParameterType.Name);
                l_MethodCallLog.Append(" ");
                l_MethodCallLog.Append(l_MethodParameter.Name);
            }
            l_MethodCallLog.Append(")");

            var l_SourceFileName = p_MethodCall.GetFileName();
            if (!String.IsNullOrEmpty(l_SourceFileName))
            {
                l_MethodCallLog.Append(" in ");
                l_MethodCallLog.Append(l_SourceFileName);
                l_MethodCallLog.Append(": line ");
                l_MethodCallLog.Append(p_MethodCall.GetFileLineNumber().ToString());
            }

            return l_MethodCallLog.ToString();
        }
    }
}
