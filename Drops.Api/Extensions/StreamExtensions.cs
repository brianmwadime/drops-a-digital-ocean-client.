﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Api.Extensions
{
    public static class StreamExtensions
    {
        public static void Write(this Stream output, byte[] buffer)
        {
            output.Write(buffer, 0, buffer.Length);
        }
    }
}
