﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Drops.Api.Models.Responses;
using Drops.Api.Models;

namespace Drops.Api.Clients {
    public interface IActionsClient {
        /// <summary>
        /// Retrieve all actions that have been executed on the current account.
        /// </summary>
        Task<DropsResponse<List<Action>>> GetAll();

        /// <summary>
        /// Retrieve an existing action
        /// </summary>
        Task<Action> Get(int actionId);
    }
}