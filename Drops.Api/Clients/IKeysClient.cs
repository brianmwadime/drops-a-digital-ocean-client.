﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Drops.Api.Models.Responses;
using Drops.Api.Models;

namespace Drops.Api.Clients {
    public interface IKeysClient {
        /// <summary>
        /// Retrieve all keys in your account
        /// </summary>
        Task<DropsResponse<List<Key>>> GetAll();

        /// <summary>
        /// Create a new key entry
        /// </summary>
        Task<Key> Create(Models.Requests.Key key);

        /// <summary>
        /// Retrieve an existing key in your account
        /// </summary>
        Task<Key> Get(object keyIdOrFingerprint);

        /// <summary>
        /// Update an existing key in your account
        /// </summary>
        Task<Key> Update(object keyIdOrFingerprint, Models.Requests.Key key);

        /// <summary>
        /// Delete an existing key in your account
        /// </summary>
        Task Delete(object keyIdOrFingerprint);
    }
}