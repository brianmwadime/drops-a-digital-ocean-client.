﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Drops.Api.Models.Responses;
using Drops.Api.Models;

namespace Drops.Api.Clients {
    public interface IDropletsClient {
        /// <summary>
        /// Retrieve all Droplets in your account.
        /// </summary>
        //Task<IReadOnlyList<Droplet>> GetAll();
        Task<DropsResponse<List<Droplet>>> GetAll();

        /// <summary>
        /// Retrieve all kernels available to a Droplet.
        /// </summary>
        Task<DropsResponse<List<Kernel>>> GetKernels(int dropletId);

        /// <summary>
        /// Retrieve all snapshots that have been created for a Droplet.
        /// </summary>
        Task<DropsResponse<List<Image>>> GetSnapshots(int dropletId);

        /// <summary>
        /// Retrieve all backups that have been created for a Droplet.
        /// </summary>
        Task<DropsResponse<List<Image>>> GetBackups(int dropletId);

        /// <summary>
        /// Retrieve all actions that have been executed on a Droplet.
        /// </summary>
        Task<DropsResponse<List<Action>>> GetActions(int dropletId);

        /// <summary>
        /// Create a new Droplet
        /// </summary>
        Task<Droplet> Create(Models.Requests.Droplet droplet);

        /// <summary>
        /// Retrieve an existing Droplet
        /// </summary>
        Task<Droplet> Get(int dropletId);

        /// <summary>
        /// Delete an existing Droplet
        /// </summary>
        Task Delete(int dropletId);

        /// <summary>
        /// Retrieve a list of droplets that are scheduled to be upgraded
        /// </summary>
        Task<DropsResponse<List<DropletUpgrade>>> GetUpgrades();
    }
}