﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Drops.Api.Http;
using Drops.Api.Models.Responses;
using Drops.Api.Models;

namespace Drops.Api.Clients {
    public class SizesClient : ISizesClient {
        private readonly IConnection _connection;

        public SizesClient(IConnection connection) {
            _connection = connection;
        }

        #region ISizesClient Members

        /// <summary>
        /// Retrieve all Drops Droplet Sizes
        /// </summary>
        public Task<DropsResponse<List<Size>>> GetAll() {
            return _connection.GetPaginated<List<Size>>("sizes", null, "sizes");
        }

        #endregion
    }
}