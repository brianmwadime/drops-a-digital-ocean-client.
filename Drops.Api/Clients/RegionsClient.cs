﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Drops.Api.Http;
using Drops.Api.Models.Responses;
using Drops.Api.Models;

namespace Drops.Api.Clients {
    public class RegionsClient : IRegionsClient {
        private readonly IConnection _connection;

        public RegionsClient(IConnection connection) {
            _connection = connection;
        }

        #region IRegionsClient Members

        /// <summary>
        /// Retrieve all Drops regions
        /// </summary>
        public Task<DropsResponse<List<Region>>> GetAll() {
            return _connection.GetPaginated<List<Region>>("regions", null, "regions");
        }

        #endregion
    }
}