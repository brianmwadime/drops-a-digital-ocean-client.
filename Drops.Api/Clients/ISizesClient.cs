﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Drops.Api.Models.Responses;
using Drops.Api.Models;

namespace Drops.Api.Clients {
    public interface ISizesClient {
        /// <summary>
        /// Retrieve all Drops Droplet Sizes
        /// </summary>
        Task<DropsResponse<List<Size>>> GetAll();
    }
}