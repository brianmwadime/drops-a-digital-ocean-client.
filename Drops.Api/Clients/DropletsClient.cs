﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Drops.Api.Http;
using Drops.Api.Models.Responses;
using RestSharp;
using Drops.Api.Models;

namespace Drops.Api.Clients {
    public class DropletsClient : IDropletsClient {
        private readonly IConnection _connection;

        public DropletsClient(IConnection connection) {
            _connection = connection;
        }

        #region IDropletsClient Members

        /// <summary>
        /// Retrieve all Droplets in your account.
        /// </summary>
        public Task<DropsResponse<List<Droplet>>> GetAll()
        {
            return _connection.GetPaginated<List<Droplet>>("droplets", null, "droplets");
        }

        /// <summary>
        /// Retrieve all kernels available to a Droplet.
        /// </summary>
        public Task<DropsResponse<List<Kernel>>> GetKernels(int dropletId) {
            var parameters = new List<Parameter> {
                new Parameter { Name = "id", Value = dropletId, Type = ParameterType.UrlSegment }
            };
            return _connection.GetPaginated<List<Kernel>>("droplets/{id}/kernels", parameters, "kernels");
        }

        /// <summary>
        /// Retrieve all snapshots that have been created for a Droplet.
        /// </summary>
        public Task<DropsResponse<List<Image>>> GetSnapshots(int dropletId)
        {
            var parameters = new List<Parameter> {
                new Parameter { Name = "id", Value = dropletId, Type = ParameterType.UrlSegment }
            };
            return _connection.GetPaginated<List<Image>>("droplets/{id}/snapshots", parameters, "snapshots");
        }

        /// <summary>
        /// Retrieve all backups that have been created for a Droplet.
        /// </summary>
        public Task<DropsResponse<List<Image>>> GetBackups(int dropletId) {
            var parameters = new List<Parameter> {
                new Parameter { Name = "id", Value = dropletId, Type = ParameterType.UrlSegment }
            };
            return _connection.GetPaginated<List<Image>>("droplets/{id}/backups", parameters, "backups");
        }

        /// <summary>
        /// Retrieve all actions that have been executed on a Droplet.
        /// </summary>
        public Task<DropsResponse<List<Action>>> GetActions(int dropletId) {
            var parameters = new List<Parameter> {
                new Parameter { Name = "id", Value = dropletId, Type = ParameterType.UrlSegment }
            };
            return _connection.GetPaginated<List<Action>>("droplets/{id}/actions", parameters, "actions");
        }

        /// <summary>
        /// Create a new Droplet
        /// </summary>
        public Task<Droplet> Create(Models.Requests.Droplet droplet) {
            return _connection.ExecuteRequest<Droplet>("droplets", null, droplet, "droplet", Method.POST);
        }

        /// <summary>
        /// Retrieve an existing Droplet
        /// </summary>
        public Task<Droplet> Get(int dropletId)
        {
            var parameters = new List<Parameter> {
                new Parameter { Name = "id", Value = dropletId, Type = ParameterType.UrlSegment }
            };
            return _connection.ExecuteRequest<Droplet>("droplets/{id}", parameters, null, "droplet");
        }

        /// <summary>
        /// Delete an existing Droplet
        /// </summary>
        public Task Delete(int dropletId) {
            var parameters = new List<Parameter> {
                new Parameter { Name = "id", Value = dropletId, Type = ParameterType.UrlSegment }
            };
            return _connection.ExecuteRaw("droplets/{id}", parameters, Method.DELETE);
        }

        /// <summary>
        /// Retrieve a list of droplets that are scheduled to be upgraded
        /// </summary>
        public Task<DropsResponse<List<DropletUpgrade>>> GetUpgrades() {
            return _connection.GetPaginated<List<DropletUpgrade>>("droplet_upgrades", null);
        }

        #endregion
    }
}