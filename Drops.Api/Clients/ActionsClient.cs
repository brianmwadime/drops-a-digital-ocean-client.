﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Drops.Api.Http;
using Drops.Api.Models.Responses;
using RestSharp;
using Drops.Api.Models;

namespace Drops.Api.Clients {
    public class ActionsClient : IActionsClient {
        private readonly IConnection _connection;

        public ActionsClient(IConnection connection) {
            _connection = connection;
        }

        #region IActionsClient Members

        /// <summary>
        /// Retrieve all actions that have been executed on the current account.
        /// </summary>
        public Task<DropsResponse<List<Action>>> GetAll() {
            return _connection.GetPaginated<List<Action>>("actions", null, "actions");
        }

        /// <summary>
        /// Retrieve an existing Action
        /// </summary>
        public Task<Action> Get(int actionId) {
            var parameters = new List<Parameter> {
                new Parameter { Name = "id", Value = actionId, Type = ParameterType.UrlSegment }
            };
            return _connection.ExecuteRequest<Action>("actions/{id}", parameters, null, "action");
        }

        #endregion
    }
}