﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Drops.Api.Models.Responses;
using Drops.Api.Models;

namespace Drops.Api.Clients {
    public interface IRegionsClient {
        /// <summary>
        /// Retrieve all Drops regions
        /// </summary>
        Task<DropsResponse<List<Region>>> GetAll();
    }
}