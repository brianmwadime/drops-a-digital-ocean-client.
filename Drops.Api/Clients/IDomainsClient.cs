﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Drops.Api.Models.Responses;
using Drops.Api.Models;

namespace Drops.Api.Clients {
    public interface IDomainsClient {
        /// <summary>
        /// Retrieve a list of all domains in your account
        /// </summary>
        Task<DropsResponse<List<Domain>>> GetAll();

        /// <summary>
        /// Create a new domain
        /// </summary>
        Task<Domain> Create(Models.Requests.Domain domain);

        /// <summary>
        /// Retrieve a specific domain
        /// </summary>
        Task<Domain> Get(string domainName);

        /// <summary>
        /// Delete an existing domain
        /// </summary>
        Task Delete(string domainName);
    }
}