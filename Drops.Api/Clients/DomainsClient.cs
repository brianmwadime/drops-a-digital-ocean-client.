﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Drops.Api.Http;
using Drops.Api.Models.Responses;
using RestSharp;
using Drops.Api.Models;

namespace Drops.Api.Clients {
    public class DomainsClient : IDomainsClient {
        private readonly IConnection _connection;

        public DomainsClient(IConnection connection) {
            _connection = connection;
        }

        #region IDomainsClient Members

        /// <summary>
        /// Retrieve a list of all domains in your account
        /// </summary>
        public Task<DropsResponse<List<Domain>>> GetAll() {
            return _connection.GetPaginated<List<Domain>>("domains", null, "domains");
        }

        /// <summary>
        /// Create a new domain
        /// </summary>
        public Task<Domain> Create(Models.Requests.Domain domain) {
            return _connection.ExecuteRequest<Domain>("domains", null, domain, "domain", Method.POST);
        }

        /// <summary>
        /// Retrieve a specific domain
        /// </summary>
        public Task<Domain> Get(string domainName) {
            var parameters = new List<Parameter> {
                new Parameter { Name = "name", Value = domainName, Type = ParameterType.UrlSegment }
            };
            return _connection.ExecuteRequest<Domain>("domains/{name}", parameters, null, "domain");
        }

        /// <summary>
        /// Delete an existing domain
        /// </summary>
        public Task Delete(string domainName) {
            var parameters = new List<Parameter> {
                new Parameter { Name = "name", Value = domainName, Type = ParameterType.UrlSegment }
            };
            return _connection.ExecuteRaw("domains/{name}", parameters, Method.DELETE);
        }

        #endregion
    }
}