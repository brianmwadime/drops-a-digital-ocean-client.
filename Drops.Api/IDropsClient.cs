﻿using Drops.Api.Clients;
using Drops.Api.Http;

namespace Drops.Api {
    public interface IDropsClient {
        IActionsClient Actions { get; }
        IDomainRecordsClient DomainRecords { get; }
        IDomainsClient Domains { get; }
        IDropletActionsClient DropletActions { get; }
        IDropletsClient Droplets { get; }
        IImageActionsClient ImageActions { get; }
        IImagesClient Images { get; }
        IKeysClient Keys { get; }
        IRegionsClient Regions { get; }
        ISizesClient Sizes { get; }

        IRateLimit Rates { get; }
    }
}