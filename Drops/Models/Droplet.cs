﻿using Caliburn.Micro;
using Drops.Api.Models.Responses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Models
{
    public enum DropletStatus
    {
        active,
        off
    }

    public class Droplet : PropertyChangedBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Image { get; set; }
        public string Region { get; set; }
        public string Size { get; set; }
        public DropletStatus Status { get; set; }
        public string StatusColor { get; set; }
        public string ActionColor { get; set; }

        public string Website
        {
            get { return string.Format("https://cloud.digitalocean.com/droplets/{0}", Id); }
        }
    }
}
