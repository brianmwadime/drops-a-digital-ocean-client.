﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Models
{
    public class Settings : PropertyChangedBase
    {
        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            NotifyOfPropertyChange(propertyName);
            return true;
        }

        protected bool SetField<T>(ref T field, T value, Expression<Func<T>> selectorExpression)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            NotifyOfPropertyChange(selectorExpression);
            return true;
        }

        #region Auth
        public string _token = string.Empty;

        public string Token
        {
            get { return _token; }
            set { SetField(ref _token, value, () => Token); }
        }

        #endregion

        #region In-App Notifications

        public bool _inAppVibration = true;

        public bool InAppVibration
        {
            get { return _inAppVibration; }
            set { SetField(ref _inAppVibration, value, () => InAppVibration); }
        }

        public bool _inAppSound = true;

        public bool InAppSound
        {
            get { return _inAppSound; }
            set { SetField(ref _inAppSound, value, () => InAppSound); }
        }

        public bool _inAppMessagePreview = true;

        public bool InAppMessagePreview
        {
            get { return _inAppMessagePreview; }
            set { SetField(ref _inAppMessagePreview, value, () => InAppMessagePreview); }
        }

        #endregion

        public bool _locationServices = false;

        public bool LocationServices
        {
            get { return _locationServices; }
            set { SetField(ref _locationServices, value, () => LocationServices); }
        }
    }
}
