﻿using Caliburn.Micro;
using Drops.Api.Cache;
using Drops.Common;
using Drops.Services;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Drops.ViewModels
{
    public class TokenViewModel : ViewModelBase,
        IHandle<string>
    {
        private string _token;

        public string Token
        {
            get { return _token; }
            set
            {
                SetField(ref _token, value.Trim(), () => Token);
                StateService.GetNotifySettingsAsync(settings =>
                {
                    settings.Token = value.Trim();
                    SettingsHelper.SetValue(Constants.TokenKey, value.Trim());
                    StateService.SaveNotifySettingsAsync(settings);
                });
            }
        }

        public void BackwardInAnimationComplete()
        {
            BeginOnUIThread(() =>
            {
                if (StateService.RemoveBackEntry)
                {
                    StateService.RemoveBackEntry = false;
                    NavigationService.RemoveBackEntry();
                }
            });
        }

        public void ForwardInAnimationComplete()
        {

            BeginOnUIThread(() =>
            {
                if (StateService.RemoveBackEntry)
                {
                    StateService.RemoveBackEntry = false;
                    NavigationService.RemoveBackEntry();
                }

                if (StateService.RemoveBackEntries)
                {
                    var backEntry = NavigationService.BackStack.FirstOrDefault();
                    while (backEntry != null
                        && !backEntry.Source.ToString().EndsWith("DashboardView.xaml"))
                    {
                        NavigationService.RemoveBackEntry();
                        backEntry = NavigationService.BackStack.FirstOrDefault();
                    }


                    StateService.RemoveBackEntries = false;
                }
            });

        }

        public TokenViewModel(ICacheService cacheService, INavigationService navigationService, IStateService stateService, IPushService pushService, ICommonErrorHandler errorHandler, IEventAggregator eventAggregator) :
            base(cacheService, navigationService, stateService, pushService, errorHandler, eventAggregator)
        {
            eventAggregator.Subscribe(this);

            StateService.GetNotifySettingsAsync(
                settings =>
                {
                    _token = settings.Token;

                    BeginOnUIThread(() =>
                    {
                        NotifyOfPropertyChange(() => Token);
                    });
                });

            PropertyChanged += OnPropertyChanged;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        public void Confirm()
        {
            if (string.IsNullOrEmpty(_token))
            {
                //new ToastNotifyHelper().ShowCoding4FunToastNotify("Please enter your Personal Access Token.", "DROPS");
                var toast = new Drops.Controls.Notifications.ToastPrompt
                {
                    TextOrientation = Orientation.Horizontal,
                    Background = (SolidColorBrush)Application.Current.Resources["drops_blue"],
                    Foreground = (SolidColorBrush)Application.Current.Resources["drops_white"],
                    FontSize = (double)Application.Current.Resources["PhoneFontSizeSmall"],
                    Title = "Drops",
                    Message = "Please enter your Personal Access Token.",
                    ImageHeight = 48,
                    ImageWidth = 48,
                    ImageSource = new BitmapImage(new Uri("/ToastPromptIcon.png", UriKind.Relative))
                };

                toast.Tap += (sender, args) =>
                {
#if DEBUG
                    Debug.WriteLine("Toast Notification in Dashboard clicked");
#endif
                };

                toast.Show();
                return;
            }

            StateService.ClearNavigationStack = true;
            NavigationService.UriFor<DashboardViewModel>().Navigate();
        }

        public void Handle(string command)
        {
            if (string.Equals(command, Commands.LogOutCommand))
            {
                Token = string.Empty;
                IsWorkingAction = string.Empty;
                IsWorking = false;
            }
        }

        protected override void OnActivate()
        {
            PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;

            if (StateService.ClearNavigationStack)
            {
                StateService.ClearNavigationStack = false;
                while (NavigationService.RemoveBackEntry() != null) { }
            }

            if (StateService.RemoveBackEntry)
            {
                StateService.RemoveBackEntry = false;
                NavigationService.RemoveBackEntry();
            }

            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Enabled;

            base.OnDeactivate(close);
        }
    }
}
