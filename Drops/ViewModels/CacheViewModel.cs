﻿using Caliburn.Micro;
using Drops.Api.Cache;
using Drops.Converters;
using Drops.Resources;
using Drops.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Execute = Drops.Common.Execute;
using Drops.Extensions;
using Drops.Common;

namespace Drops.ViewModels
{
    class CacheViewModel : ViewModelBase
    {
        private string _status = AppResources.Calculating + "...";

        public string Status
        {
            get { return _status; }
            set { SetField(ref _status, value, () => Status); }
        }

        readonly DispatcherTimer _timer = new DispatcherTimer();

        private volatile bool _isCalculating;

        public CacheViewModel(ICacheService cacheService, INavigationService navigationService, IStateService stateService, IPushService pushService, ICommonErrorHandler errorHandler, IEventAggregator eventAggregator) :
            base(cacheService, navigationService, stateService, pushService, errorHandler, eventAggregator)
        {
            _timer.Interval = TimeSpan.FromSeconds(5.0);
            _timer.Tick += OnTimerTick;

            CalculateCacheSizeAsync(size =>
            {
                Status = FileSizeConverter.Convert(size);
            });

            PropertyChanged += (sender, e) =>
            {
                if (Property.NameEquals(e.PropertyName, () => IsWorking))
                {
                    NotifyOfPropertyChange(() => CanClearCache);
                }
            };
        }

        protected override void OnActivate()
        {
            _timer.Start();

            base.OnActivate();
        }

        protected override void OnDeactivate(bool close)
        {
            _timer.Stop();

            base.OnDeactivate(close);
        }

        private void OnTimerTick(object sender, System.EventArgs e)
        {
            if (_isCalculating) return;

            CalculateCacheSizeAsync(result =>
            {
                Status = FileSizeConverter.Convert(result);
            });
        }

        private void CalculateCacheSizeAsync(Action<long> callback)
        {
            BeginOnThreadPool(() =>
            {
                _isCalculating = true;

                var length = 0L;
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    var fileNames = store.GetFileNames();

                    foreach (var fileName in fileNames)
                    {
                        try
                        {
                            if (store.FileExists(fileName))
                            {
                                using (var file = new IsolatedStorageFileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read, store))
                                {
                                    length += file.Length;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
#if DEBUG
                            Debug.WriteLine("CalculateCacheSizeAsync OpenFile: " + fileName, ex);
#endif
                        }
                    }
                }

                _isCalculating = false;

                callback.SafeInvoke(length);
            });
        }

        public bool CanClearCache
        {
            get { return !IsWorking; }
        }

        public void ClearCache()
        {
            if (IsWorking) return;

            IsWorking = true;
            BeginOnThreadPool(() =>
            {
                var length = 0L;
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    var fileNames = store.GetFileNames();
                    foreach (var fileName in fileNames)
                    {
                        if (!fileName.EndsWith(".dat") && (fileName.StartsWith("document") || fileName.StartsWith("video")))
                        {
                            try
                            {
                                store.DeleteFile(fileName);
                            }
                            catch (Exception ex)
                            {
#if DEBUG
                                Debug.WriteLine(ex);
#endif
                            }
                        }
                        else
                        {
                            try
                            {
                                using (var file = store.OpenFile(fileName, FileMode.Open, FileAccess.Read))
                                {
                                    length += file.Length;
                                }
                            }
                            catch (Exception ex)
                            {
#if DEBUG
                                Debug.WriteLine(ex);
#endif
                            }
                        }
                    }
                }
                Status = FileSizeConverter.Convert(length);
                IsWorking = false;
            });
        }
    }
}
