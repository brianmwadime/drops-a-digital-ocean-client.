﻿using Caliburn.Micro;
using Drops.Api;
using Drops.Api.Cache;
using Drops.Common;
using Drops.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Drops.ViewModels
{
    public class ViewModelBase : Screen
    {

        protected readonly INavigationService NavigationService;
        public IStateService StateService { get; private set; }
        protected readonly ICommonErrorHandler ErrorHandler;
        protected readonly IEventAggregator EventAggregator;
        protected readonly ICacheService CacheService;

        private bool _isAuthorized;

        public bool IsAuthorized
        {
            get { return _isAuthorized; }
            set
            {
                _isAuthorized = value;
                NotifyOfPropertyChange(() => IsAuthorized);
            }
            
        }

        public ViewModelBase(ICacheService cacheService,INavigationService navigationService, IStateService stateService, IPushService PushService, ICommonErrorHandler errorHandler, IEventAggregator eventAggregator)
        {
            CacheService = cacheService;
            NavigationService = navigationService;
            StateService = stateService;
            ErrorHandler = errorHandler;
            EventAggregator = eventAggregator;

        }

        protected override void OnActivate()
        {
            base.OnActivate();
        }

        protected override void OnInitialize()
        {
            //SetPropertiesFromNavigation();
            base.OnInitialize();
        }

        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            NotifyOfPropertyChange(propertyName);
            return true;
        }

        protected bool SetField<T>(ref T field, T value, Expression<Func<T>> selectorExpression)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            NotifyOfPropertyChange(selectorExpression);
            return true;
        }

        private bool _isLoadingError;

        public bool IsLoadingError
        {
            get { return _isLoadingError; }
            set { SetField(ref _isLoadingError, value, () => IsLoadingError); }
        }

        private bool _isWorking;

        public bool IsWorking
        {
            get { return _isWorking; }
            set { SetField(ref _isWorking, value, () => IsWorking); }
        }

        private string _isWorkingAction;

        public string IsWorkingAction
        {
            get { return _isWorkingAction; }
            set { SetField(ref _isWorkingAction, value, () => IsWorkingAction); }
        }

        /// <summary>
        /// Waits for action to complete.
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="actionId">The action identifier.</param>
        /// <returns></returns>
        protected static async Task WaitForAction(IDropsClient client, int actionId)
        {
            while (true)
            {
                var @event = await client.Actions.Get(actionId);
                if (@event.CompletedAt != null)
                {
                    break;
                }
                Thread.Sleep(TimeSpan.FromSeconds(5));
            }
        }

        public void BeginOnUIThread(System.Action action)
        {
            Drops.Common.Execute.BeginOnUIThread(action);
        }

        public void BeginOnUIThread(System.Action action, TimeSpan delay)
        {
            Drops.Common.Execute.BeginOnUIThread(delay, action);
        }

        public void BeginOnThreadPool(System.Action action)
        {
            Drops.Common.Execute.BeginOnThreadPool(action);
        }

        public void Subscribe()
        {
            EventAggregator.Subscribe(this);
        }

        public void Unsubscribe()
        {
            EventAggregator.Unsubscribe(this);
        }

        private void SetPropertiesFromNavigation()
        {
            //Retrive list of properties of the current screen
            PropertyInfo[] properties = this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (string key in NavigationParams.Keys)
            {
                PropertyInfo prop = properties.FirstOrDefault(x => x.Name == key);
                if (prop != null)
                {
                    //If cache contains a property with such a name - set it.
                    object value = NavigationParams.GetAndRemove(key);
                    if (value != null)
                    {
                        prop.SetValue(this, value);
                    }
                }
            }

            //NavigationParams is used only to send data from one page to another.
            //There is no need to keep any data in it after navigation.
            NavigationParams.Clear();
        }     
    }
}
