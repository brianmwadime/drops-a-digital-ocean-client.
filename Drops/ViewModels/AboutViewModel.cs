﻿using Caliburn.Micro;
using Drops.Api.Cache;
using Drops.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.ViewModels
{
    public class AboutViewModel : ViewModelBase
    {

        public AboutViewModel(ICacheService cacheService, INavigationService navigationService, IStateService stateService, IPushService pushService, ICommonErrorHandler errorHandler, IEventAggregator eventAggregator) :
            base (cacheService, navigationService, stateService, pushService, errorHandler, eventAggregator)
        {

        }
    }
}
