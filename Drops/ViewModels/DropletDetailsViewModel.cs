﻿using Caliburn.Micro;
using Drops.Api;
using Drops.Api.Cache;
using Drops.Api.Cache.EventArgs;
using Drops.Api.Models.Responses;
using Drops.Common;
using Drops.Services;
using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Execute = Drops.Common.Execute;
using Action = Drops.Api.Models.Responses.Action;

namespace Drops.ViewModels
{
    public class DropletDetailsViewModel : ViewModelBase,
        IHandle<string>,
        IHandle<UpdateCompletedEventArgs>
    {
        private Droplet _droplet;

        public Droplet Droplet
        {
            get { return _droplet; }
            set
            {
                _droplet = value;
                NotifyOfPropertyChange(() => Droplet);
            }
        }

        private string _token;

        public string Token
        {
            get { return _token; }
            set
            {
                _token = value;
                NotifyOfPropertyChange(() => Token);
            }
        }

        public int Id { get; set; }

        public void BackwardInAnimationComplete()
        {
            BeginOnUIThread(() =>
            {
                if (StateService.RemoveBackEntry)
                {
                    StateService.RemoveBackEntry = false;
                    NavigationService.RemoveBackEntry();
                }
            });
        }

        public void ForwardInAnimationComplete()
        {

            BeginOnUIThread(() =>
            {
                if (StateService.RemoveBackEntry)
                {
                    StateService.RemoveBackEntry = false;
                    NavigationService.RemoveBackEntry();
                }

                if (StateService.RemoveBackEntries)
                {
                    var backEntry = NavigationService.BackStack.FirstOrDefault();
                    while (backEntry != null
                        && !backEntry.Source.ToString().EndsWith("DashboardView.xaml"))
                    {
                        NavigationService.RemoveBackEntry();
                        backEntry = NavigationService.BackStack.FirstOrDefault();
                    }


                    StateService.RemoveBackEntries = false;
                }
            });

        }

        public void NavigateToDashboardViewModel()
        {
            StateService.ClearNavigationStack = true;
            NavigationService.UriFor<DashboardViewModel>().Navigate();
        }

        public DropletDetailsViewModel(ICacheService cacheService, INavigationService navigationService, IStateService stateService, IPushService pushService, ICommonErrorHandler errorHandler, IEventAggregator eventAggregator)
            : base(cacheService, navigationService, stateService, pushService, errorHandler, eventAggregator)
        {
            EventAggregator.Subscribe(this);

            BeginOnUIThread(() =>
            {
                if (Id.Equals(0) || Id.Equals(null))
                {
                    return;
                }

                // Load Cache
                Droplet = CacheService.GetDroplet(Id);

                // Update Cache
                if (Util.IsNetworkAvailable)
                    GetDroplet(Id);
            });
        }

        private async void GetDroplet(int dropletId)
        {
            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));
            IsWorking = true;
            var droplet = await client.Droplets.Get(dropletId);

            Droplet = droplet;
            CacheService.ReplaceDroplet(droplet);
            Execute.BeginOnThreadPool(() => EventAggregator.Publish(new UpdateCompletedEventArgs()));

            IsWorking = false;

        }


        public void ViewSnapshots()
        {
            NavigationService.UriFor<SnapshotsViewModel>().WithParam(x => x.Id, Id).Navigate();
        }

        public void ViewBackups()
        {
            NavigationService.UriFor<BackupsViewModel>().WithParam(x => x.Id, Id).Navigate();
        }

        public void Handle(string command)
        {
            if (string.Equals(command, Commands.LogOutCommand))
            {
                Droplet = null;
                IsWorkingAction = string.Empty;
                IsWorking = false;
            }
        }

        public void ChangeKernel()
        {
            throw new NotImplementedException();
        }

        public async void EnableIpv6()
        {
            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));
            IsWorking = true;
            IsWorkingAction = string.Format("Enable Ipv6 for droplet to {0}", Droplet.Name);
#if DEBUG
            Debug.WriteLine(string.Format("Enable Ipv6 for Droplet : {0}", Droplet.Name));
#endif
            var action = await client.DropletActions.EnableIpv6(Droplet.Id);

            if (action.Status.Equals("completed"))
            {
                IsWorking = false;
                IsWorkingAction = string.Empty;
                GetDroplet(Id);
            }
            else if (action.Status.Equals("errored"))
            {
                IsWorking = false;
                IsWorkingAction = string.Empty;
                GetDroplet(Id);
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {

                    var toast = new Drops.Controls.Notifications.ToastPrompt
                    {
                        DataContext = Droplet,
                        TextOrientation = Orientation.Horizontal,
                        Foreground = (SolidColorBrush)Application.Current.Resources["drops_white"],
                        Background = (SolidColorBrush)Application.Current.Resources["drops_blue"],
                        FontSize = (double)Application.Current.Resources["PhoneFontSizeSmall"],
                        Title = "Drops",
                        Message = string.Format("{0} failed..", action.Type),
                        ImageHeight = 48,
                        ImageWidth = 48,
                        ImageSource = new BitmapImage(new Uri("/ToastPromptIcon.png", UriKind.Relative))
                    };

                    toast.Tap += (sender, args) =>
                    {
#if DEBUG
                        Debug.WriteLine("Toast Notification in Dashboard clicked");
#endif
                    };

                    toast.Show();
                });
            }
            else if (action.Status.Equals("in-progress"))
            {
                GetDroplet(Id);
                IsWorking = false;
                IsWorkingAction = string.Empty;
            }

            IsWorking = false;
            IsWorkingAction = string.Empty;
        }

        public void Reset()
        {
#if DEBUG
            Debug.WriteLine(string.Format("Reset Password for Droplet : {0}", Droplet.Name));
#endif
            IsWorking = true;
            IsWorkingAction = string.Format("Rebooting Droplet {0}", Droplet.Name);

            var _action = ResetDroplet(Droplet);

            _action.Subscribe(action =>
            {
                if (action.Status.Equals("completed"))
                {
                    IsWorking = false;
                    IsWorkingAction = string.Empty;
                    GetDroplet(Id);
                }
                else if (action.Status.Equals("errored"))
                {
                    IsWorking = false;
                    IsWorkingAction = string.Empty;
                    GetDroplet(Id);
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        var toast = new Drops.Controls.Notifications.ToastPrompt
                        {
                            DataContext = Droplet,
                            TextOrientation = Orientation.Horizontal,
                            Foreground = (SolidColorBrush)Application.Current.Resources["drops_white"],
                            Background = (SolidColorBrush)Application.Current.Resources["drops_blue"],
                            FontSize = (double)Application.Current.Resources["PhoneFontSizeSmall"],
                            Title = "Drops",
                            Message = string.Format("{0} failed..", action.Type),
                            ImageHeight = 48,
                            ImageWidth = 48,
                            ImageSource = new BitmapImage(new Uri("/ToastPromptIcon.png", UriKind.Relative))
                        };

                        toast.Tap += (sender, args) =>
                        {
#if DEBUG
                            Debug.WriteLine("Toast Notification in Dashboard clicked");
#endif
                        };

                        toast.Show();
                    });
                }
                else if (action.Status.Equals("in-progress"))
                {
                    IsWorking = true;
                    IsWorkingAction = string.Empty;
                    GetDroplet(Id);

                }
            });
        }

        private IObservable<Action> ResetDroplet(Droplet droplet)
        {
            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));
            return Observable.StartAsync(ct => Task.Run(async () =>
            {
                var reboot = await client.DropletActions.ResetPassword(droplet.Id);
                await WaitForAction(client, reboot.Id);
                return reboot;
            }, ct));
        }

        public void Reboot()
        {
#if DEBUG
            Debug.WriteLine(string.Format("Rebooting Droplet : {0}", Droplet.Name));
#endif
            IsWorking = true;
            IsWorkingAction = string.Format("Rebooting Droplet {0}", Droplet.Name);

            var _action = RebootDroplet(Droplet);

            _action.Subscribe(action =>
            {
                if (action.Status.Equals("completed"))
                {
                    IsWorking = false;
                    IsWorkingAction = string.Empty;
                    GetDroplet(Id);
                }
                else if (action.Status.Equals("errored"))
                {
                    IsWorking = false;
                    IsWorkingAction = string.Empty;
                    GetDroplet(Id);
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {

                        var toast = new Drops.Controls.Notifications.ToastPrompt
                        {
                            DataContext = Droplet,
                            TextOrientation = Orientation.Horizontal,
                            Foreground = (SolidColorBrush)Application.Current.Resources["drops_white"],
                            Background = (SolidColorBrush)Application.Current.Resources["drops_blue"],
                            FontSize = (double)Application.Current.Resources["PhoneFontSizeSmall"],
                            Title = "Drops",
                            Message = string.Format("{0} failed..", action.Type),
                            ImageHeight = 48,
                            ImageWidth = 48,
                            ImageSource = new BitmapImage(new Uri("/ToastPromptIcon.png", UriKind.Relative))
                        };

                        toast.Tap += (sender, args) =>
                        {
#if DEBUG
                            Debug.WriteLine("Toast Notification in Dashboard clicked");
#endif
                        };

                        toast.Show();
                    });
                }
                else if (action.Status.Equals("in-progress"))
                {
                    IsWorking = false;
                    IsWorkingAction = string.Empty;
                    GetDroplet(Id);
                }
            });

        }

        /// <summary>
        /// Rebbots the droplet.
        /// </summary>
        /// <param name="dropletId">Droplet Id.</param>
        /// <returns></returns>
        private IObservable<Action> RebootDroplet(Droplet droplet)
        {
            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));
            return Observable.StartAsync(ct => Task.Run(async () =>
            {
                var reboot = await client.DropletActions.Reboot(droplet.Id);
                await WaitForAction(client, reboot.Id);
                return reboot;
            }, ct));
        }


        /// <summary>Ipv6s the droplet.</summary>
        /// <param name="droplet">The droplet.</param>
        /// <returns></returns>
        private IObservable<Action> Ipv6Droplet(Droplet droplet)
        {
            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));
            return Observable.StartAsync(ct => Task.Run(async () =>
            {
                var reboot = await client.DropletActions.EnableIpv6(droplet.Id);
                await WaitForAction(client, reboot.Id);
                return reboot;
            }, ct));
        }
        
        public void Rename()
        {
            TextBox textBox = new TextBox()
            {
                Name = "txtRename",
                Text = string.Empty,
            };

            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Caption = "Droplet Details",
                Message = "Rename Droplet",
                Content = textBox,
                LeftButtonContent = "Cancel",
                RightButtonContent = "Ok",
                IsFullScreen = false
            };

            messageBox.Dismissed += (s1, e1) =>
            {
                switch (e1.Result)
                {
                    case CustomMessageBoxResult.LeftButton:

                        messageBox.Dismiss();
                        break;
                    case CustomMessageBoxResult.RightButton:
                        RenamingDroplet(Droplet.Id, textBox.Text.Trim());
                        messageBox.Dismiss();
                        break;
                    default:
                        break;
                }
            };

            messageBox.Show();  
        }

        public void RenamingDroplet(int dropletId, string name)
        {
#if DEBUG
            Debug.WriteLine(string.Format("Renaming Droplet : {0}", Droplet.Name));
#endif
            IsWorking = true;
            IsWorkingAction = string.Format("Renaming droplet to {0}", Droplet.Name);

            var _action = RenameDroplet(Droplet.Id, name);

            _action.Subscribe(action =>
            {
                if (action.Status.Equals("completed"))
                {
                    IsWorking = false;
                    IsWorkingAction = string.Empty;
                    GetDroplet(Id);
                }
                else if (action.Status.Equals("errored"))
                {
                    IsWorking = false;
                    IsWorkingAction = string.Empty;
                    GetDroplet(Id);
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {

                        var toast = new Drops.Controls.Notifications.ToastPrompt
                        {
                            DataContext = Droplet,
                            TextOrientation = Orientation.Horizontal,
                            Foreground = (SolidColorBrush)Application.Current.Resources["drops_white"],
                            Background = (SolidColorBrush)Application.Current.Resources["drops_blue"],
                            FontSize = (double)Application.Current.Resources["PhoneFontSizeSmall"],
                            Title = "Drops",
                            Message = string.Format("{0} failed..", action.Type),
                            ImageHeight = 48,
                            ImageWidth = 48,
                            ImageSource = new BitmapImage(new Uri("/ToastPromptIcon.png", UriKind.Relative))
                        };

                        toast.Tap += (sender, args) =>
                        {
#if DEBUG
                            Debug.WriteLine("Toast Notification in Dashboard clicked");
#endif
                        };

                        toast.Show();
                    });
                }
                else if (action.Status.Equals("in-progress"))
                {
                    IsWorking = true;
                    IsWorkingAction = string.Empty;
                    GetDroplet(Id);

                }
            });
        }


        /// <summary>Renames the droplet.</summary>
        /// <param name="id">The identifier.</param>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        private IObservable<Action> RenameDroplet(int id, string name)
        {
            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));
            return Observable.StartAsync(ct => Task.Run(async () =>
            {
                var reboot = await client.DropletActions.Rename(id, name);
                await WaitForAction(client, reboot.Id);
                return reboot;
            }, ct));
        }

        public void Handle(UpdateCompletedEventArgs message)
        {
            var droplet = CacheService.GetDroplet(Droplet.Id);
        }
    }
}
