﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Drops.Api.Models.Responses;
using System.Threading.Tasks;
using Drops.Api.Cache;
using Caliburn.Micro;
using Drops.Services;
using Drops.Api.Cache.EventArgs;
using System.Collections.ObjectModel;
using Drops.Common;
using System.Diagnostics;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using Drops.Resources;
using Drops.Api;
using Image = Drops.Api.Models.Responses.Image;
using Execute = Drops.Common.Execute;

namespace Drops.ViewModels
{
    public class BackupsViewModel : ItemsViewModelBase<Image>,
    IHandle<string>,
    IHandle<ImageRemovedEventArgs>,
    IHandle<ImageAddedEventArgs>,
    IHandle<ImageUpdateCompletedEventArgs>
    {
        public string EmptyDashboardImageSource
        {
            get
            {
                return "/Assets/General/drops.droplets-WXGA.png";
            }
        }

        private bool _isEmptyDashboard;

        public bool IsEmptyDashboard
        {
            get { return _isEmptyDashboard; }
            set { SetField(ref _isEmptyDashboard, value, () => IsEmptyDashboard); }
        }

        public bool FirstRun { get; set; }
        public int Id {get;set;}

        public BackupsViewModel(ICacheService cacheService, INavigationService navigationService, IStateService stateService, IPushService pushService, ICommonErrorHandler errorHandler, IEventAggregator eventAggregator)
            : base(cacheService, navigationService, stateService, pushService, errorHandler, eventAggregator)
        {
            Items = new ObservableCollection<Image>();

            EventAggregator.Subscribe(this);

            BeginOnThreadPool(() =>
            {
                var isAuthorized = SettingsHelper.GetValue<string>(Constants.TokenKey);
                if (!string.IsNullOrEmpty(isAuthorized))
                {
                    var backups = CacheService.GetBackups();

                    IsEmptyDashboard = backups.Count == 0;

                    var backupsCache = new Dictionary<int, Image>();
                    var clearedBackups = new List<Image>();

                    foreach (var backup in backups)
                    {
                        if (!backupsCache.ContainsKey(backup.Id))
                        {
                            clearedBackups.Add(backup);
                            backupsCache[backup.Id] = backup;
                        }
                        else
                        {
                            var cachedBackup = backupsCache[backup.Id];
                            if (cachedBackup is Image)
                            {
                                CacheService.DeleteBackup(backup);
                                continue;
                            }
                        }
                    }

                    // load cache
                    // IsWorking = true;
                    // IsWorkingAction = droplets.Count == 0 ? AppResources.Loading : string.Empty;
                    LazyItems.Clear();
                    {
                        foreach (var backup in clearedBackups)
                        {
                            LazyItems.Add(backup);
                        }
                    }

                    if (LazyItems.Count == 0)
                    {
                        if (Util.IsNetworkAvailable)
                            UpdateBackupsAsync();
                    }
                    else
                    {
                        BeginOnUIThread(() => PopulateItems(() =>
                        {
                            if (Util.IsNetworkAvailable)
                                UpdateBackupsAsync();
                        }));
                    }
                }
            });
        }

        private async void UpdateBackupsAsync()
        {
            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));

            IsWorking = true;
            var backups = await client.Droplets.GetBackups(Id);

            IsEmptyDashboard = backups.Response.Count == 0;

            IsWorkingAction = backups.Response.Count == 0 ? AppResources.Loading : string.Empty;

            if (backups.Error != null)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {

                    var toast = new Drops.Controls.Notifications.ToastPrompt
                    {
                        DataContext = backups,
                        TextOrientation = Orientation.Horizontal,
                        Foreground = (SolidColorBrush)Application.Current.Resources["drops_white"],
                        Background = (SolidColorBrush)Application.Current.Resources["drops_blue"],
                        FontSize = (double)Application.Current.Resources["PhoneFontSizeSmall"],
                        Title = "Drops",
                        Message = backups.Error.message,
                        ImageHeight = 48,
                        ImageWidth = 48,
                        ImageSource = new BitmapImage(new Uri("/ToastPromptIcon.png", UriKind.Relative))
                    };

                    toast.Tap += (sender, args) =>
                    {
#if DEBUG
                        Debug.WriteLine("Toast Notification in Backups clicked");
#endif
                    };

                    toast.Show();
                });
            }
            else
            {
                BeginOnUIThread(() =>
                {
                    Items.Clear();
                    foreach (Image backup in backups.Response.OrderByDescending(x => x.CreatedAt))
                    {
                        Items.Add(backup);
                        CacheService.SyncBackups(backup, null);

                    }
                });
            }


            IsWorking = false;
            IsWorkingAction = string.Empty;
        }

        public void Handle(ImageRemovedEventArgs message)
        {
            throw new NotImplementedException();
        }

        public void Handle(ImageAddedEventArgs message)
        {
            throw new NotImplementedException();
        }

        public void Handle(ImageUpdateCompletedEventArgs message)
        {
            throw new NotImplementedException();
        }

        public void Handle(string message)
        {
            if (string.Equals(message, Commands.LogOutCommand))
            {
                LazyItems.Clear();
                BeginOnUIThread(() => Items.Clear());
                IsWorkingAction = string.Empty;
                IsWorking = false;
            }
        }
    }
}
