﻿using Caliburn.Micro;
using Drops.Api;
using Drops.Api.Models.Responses;
using Drops.Common;
using Drops.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using Microsoft.Phone.Tasks;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Drops.Api.Cache;
using Drops.Resources;
using Drops.Api.Cache.EventArgs;
using Execute = Drops.Common.Execute;
using Drops.Models;
using Droplet = Drops.Api.Models.Responses.Droplet;
using Drops.Controls.Notifications;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Controls;


namespace Drops.ViewModels
{

    /// <summary>
    /// Dashboard View Model
    /// </summary>
    public class DashboardViewModel : ItemsViewModelBase<Droplet>,
        IHandle<string>,
        IHandle<DropletRemovedEventArgs>,
        IHandle<DropletAddedEventArgs>,
        IHandle<UpdateCompletedEventArgs>
    {
        private IPushService _pushService;

        public string EmptyDashboardImageSource
        {
            get
            {
                return "/Assets/General/drops.droplets-WXGA.png";
            }
        }

        private bool _isEmptyDashboard;

        public bool IsEmptyDashboard
        {
            get { return _isEmptyDashboard; }
            set { SetField(ref _isEmptyDashboard, value, () => IsEmptyDashboard); }
        }

        public bool FirstRun { get; set; }

        private bool _registerDeviceOnce;

        public DashboardViewModel(ICacheService cacheService, INavigationService navigationService, IStateService stateService, IPushService pushService, ICommonErrorHandler errorHandler, IEventAggregator eventAggregator)
            : base(cacheService, navigationService, stateService, pushService, errorHandler, eventAggregator)
        {

            Items = new ObservableCollection<Droplet>();

            _pushService = pushService;

            EventAggregator.Subscribe(this);

            BeginOnThreadPool(() =>
            {
                var isAuthorized = SettingsHelper.GetValue<string>(Constants.TokenKey);
                if (!string.IsNullOrEmpty(isAuthorized))
                {
                    var droplets = CacheService.GetDroplets();

                    IsEmptyDashboard = droplets.Count == 0;

                    var dropletsCache = new Dictionary<int, Droplet>();
                    var clearedDroplets = new List<Droplet>();

                    foreach (var droplet in droplets)
                    {
                        if (!dropletsCache.ContainsKey(droplet.Id))
                        {
                            clearedDroplets.Add(droplet);
                            dropletsCache[droplet.Id] = droplet;
                        }
                        else
                        {
                            var cachedDroplet = dropletsCache[droplet.Id];
                            if (cachedDroplet is Droplet)
                            {
                                CacheService.DeleteDroplet(droplet);
                                continue;
                            }
                        }
                    }

                    // load cache
                    // IsWorking = true;
                    // IsWorkingAction = droplets.Count == 0 ? AppResources.Loading : string.Empty;
                    LazyItems.Clear();
                    {
                        foreach (var droplet in clearedDroplets)
                        {
                            LazyItems.Add(droplet);
                        }
                    }

                    if (LazyItems.Count == 0)
                    {
                        if(Util.IsNetworkAvailable)
                            LoadDroplets();
                    }
                    else
                    {
                        BeginOnUIThread(() => PopulateItems(() =>
                        {
                            if (Util.IsNetworkAvailable)
                                LoadDroplets();
                        }));
                    }
                }
            });
        }

        public async void LoadDroplets()
        {
            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Loading Droplets", "viewing droplets", null, 0);

            //GoogleAnalytics.EasyTracker.GetTracker().SendException(exc.Message, false);

            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));

            IsWorking = true;
            var droplets = await client.Droplets.GetAll();

            IsEmptyDashboard = droplets.Response.Count == 0;

            IsWorkingAction = droplets.Response.Count == 0 ? AppResources.Loading : string.Empty;

            if (droplets.Error != null)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {

                    var toast = new Drops.Controls.Notifications.ToastPrompt
                    {
                        DataContext = droplets,
                        TextOrientation = Orientation.Horizontal,
                        Foreground = (SolidColorBrush)Application.Current.Resources["drops_white"],
                        Background = (SolidColorBrush)Application.Current.Resources["drops_blue"],
                        FontSize = (double)Application.Current.Resources["PhoneFontSizeSmall"],
                        Title = "Drops",
                        Message = droplets.Error.message,
                        ImageHeight = 48,
                        ImageWidth = 48,
                        ImageSource = new BitmapImage(new Uri("/ToastPromptIcon.png", UriKind.Relative))
                    };

                    toast.Tap += (sender, args) =>
                    {
#if DEBUG
                        Debug.WriteLine("Toast Notification in Dashboard clicked");
#endif
                    };

                    toast.Show();
                });
            }
            else
            {
                BeginOnUIThread(() =>
                {
                    Items.Clear();
                    // Recreate Cache
                    CacheService.ClearAsync();
                    foreach (Droplet droplet in droplets.Response.OrderByDescending(x => x.CreatedAt))
                    {
                        Items.Add(droplet);
                        
                        CacheService.SyncDroplets(droplet, null);

                    }
                });
            }


            IsWorking = false;
        }

        public void AddDroplet()
        {

        }

        public void Settings()
        {
            NavigationService.UriFor<SettingsViewModel>().Navigate();
        }

        public bool ViewDroplet(Droplet droplet)
        {
            if (droplet == null) return false;
            NavigationService.UriFor<DropletDetailsViewModel>().WithParam(x => x.Id, droplet.Id).Navigate();

            return true;
        }

        public async void DeleteDroplet(Droplet droplet)
        {
            if (droplet == null) return;

            if (!Util.IsNetworkAvailable) return;

#if DEBUG
            Debug.WriteLine("Deleting droplet Name {0}", droplet.Name);
#endif
            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));
            IsWorking = true;
            IsWorkingAction = string.Format("Deleting droplet #{0}", droplet.Name);
            await client.Droplets.Delete(droplet.Id);

            if (droplet != null)
            {
                CacheService.DeleteDroplet(droplet);
                Execute.BeginOnThreadPool(() => EventAggregator.Publish(new DropletRemovedEventArgs(droplet)));
            }

            IsWorking = false;
            IsWorkingAction = string.Empty;

        }

        public void PowerAction(Droplet droplet)
        {
            if (droplet == null) return;

            if (!Util.IsNetworkAvailable) return;

            IsWorking = true;
            IsWorkingAction = string.Format("Powering {0} droplet #{1}", droplet.Status.Equals("active") ? "Off" : "On", droplet.Name);
            if (droplet.Status.Equals("active"))
            {
                var _droplet = PowerOffDroplet(droplet);

                _droplet.Subscribe(x =>
                {
                    IsWorking = false;
                    IsWorkingAction = string.Empty;
                    GetDroplet(x.Id);

#if DEBUG
                    Debug.WriteLine("Power off droplet Name {0}", droplet.Name);
#endif
                });

            }
            else
            {
                var _droplet = PowerOnDroplet(droplet);

                _droplet.Subscribe(x =>
                {
                    IsWorking = false;
                    IsWorkingAction = string.Empty;
                    GetDroplet(x.Id);
#if DEBUG
                    Debug.WriteLine("Power on droplet Name {0}", droplet.Name);
#endif
                });

            }

        }

        private async void GetDroplet(int dropletId)
        {
            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));
            IsWorking = true;
            IsWorkingAction = string.Format("Updating droplet #{0}", dropletId);
            var droplet = await client.Droplets.Get(dropletId);

            if (droplet != null)
            {
                CacheService.ReplaceDroplet(droplet);
                Execute.BeginOnThreadPool(() => EventAggregator.Publish(new UpdateCompletedEventArgs()));
            }

            IsWorking = false;
            IsWorkingAction = string.Empty;

        }

        /// <summary>
        /// Powers the off droplet.
        /// </summary>
        /// <param name="droplet">The droplet.</param>
        /// <returns></returns>
        private IObservable<Droplet> PowerOffDroplet(Droplet droplet)
        {
            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));
            return Observable.StartAsync(ct => Task.Run(async () =>
            {
                var power = await client.DropletActions.PowerOff(droplet.Id);
                await WaitForAction(client, power.Id);
                return droplet;
            }, ct));
        }


        /// <summary>
        /// Powers the on droplet.
        /// </summary>
        /// <param name="droplet">The droplet.</param>
        /// <returns></returns>
        private IObservable<Droplet> PowerOnDroplet(Droplet droplet)
        {
            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));
            return Observable.StartAsync(ct => Task.Run(async () =>
            {
                var power = await client.DropletActions.PowerOn(droplet.Id);
                await WaitForAction(client, power.Id);
                return droplet;
            }, ct));
        }       

        public void OnAnimationComplete()
        {
            BeginOnThreadPool(() =>
            {
                //var isAuthorized = SettingsHelper.GetValue<string>(Configs.TokenKey);
                StateService.GetNotifySettingsAsync(
                settings =>
                {
                    var _token = settings.Token;

                    if (string.IsNullOrEmpty(_token))
                    {
                        Util.IsLogEnabled = true;
                        Drops.Common.Execute.BeginOnUIThread(() =>
                        {
                            StateService.ClearNavigationStack = true;
                            NavigationService.UriFor<TokenViewModel>().Navigate();
                        });
                    }
                    else
                    {
                        if (_registerDeviceOnce) return;

                        _registerDeviceOnce = true;
                        _pushService.RegisterDeviceAsync();

                        Util.IsLogEnabled = false;
                    }



                });


            });
        }

        protected override void OnActivate()
        {
            base.OnActivate();

            if (FirstRun)
            {
                OnInitialize();
            }
        }

        protected override void OnInitialize()
        {
            BeginOnThreadPool(() =>
            {
                var isAuthorized = SettingsHelper.GetValue<string>(Constants.TokenKey);

#if DEBUG
                Debug.WriteLine("Application Token : {0}", isAuthorized);
#endif

                if (string.IsNullOrEmpty(isAuthorized))
                {
                    return;
                }
            });

            if (StateService.ClearNavigationStack)
            {
                StateService.ClearNavigationStack = false;
                while (NavigationService.RemoveBackEntry() != null) { }
            }

            base.OnInitialize();
        }

        private void OnAuthorizationRequired(object sender, System.EventArgs e)
        {

            SettingsViewModel.LogOutCommon(
                EventAggregator,
                CacheService,
                StateService,
                _pushService,
                NavigationService);
        }


        /// <summary>Reviews this App.</summary>
        public void Review()
        {
            EventAggregator.RequestTask<MarketplaceReviewTask>();
        }

        public void Handle(string command)
        {
            if (string.Equals(command, Commands.LogOutCommand))
            {
                LazyItems.Clear();
                BeginOnUIThread(() => Items.Clear());
                IsWorkingAction = string.Empty;
                IsWorking = false;
            }
        }

        public void Handle(DropletAddedEventArgs eventArgs)
        {
            OnDropletAdded(this, eventArgs);
        }

        private void OnDropletAdded(object sender, DropletAddedEventArgs e)
        {
            var droplet = e.Droplet;
            if (droplet == null) return;

            BeginOnUIThread(() =>
            {
                var index = -1;
                for (var i = 0; i < Items.Count; i++)
                {
                    if (Items[i] == e.Droplet)
                    {
                        return;
                    }

                }

                if (index == -1)
                {
                    Items.Add(droplet);
                }
                else
                {
                    Items.Insert(index, droplet);
                }
                IsWorkingAction = Items.Count == 0 || LazyItems.Count == 0 ? string.Empty : IsWorkingAction;
            });
        }

        public void Handle(DropletRemovedEventArgs args)
        {
            var droplet = Items.FirstOrDefault(x => x.Id == args.Droplet.Id);

            if (droplet != null)
            {
                BeginOnUIThread(() => Items.Remove(droplet));
            }
        }
        
        public void Handle(UpdateCompletedEventArgs args)
        {
            var dialogs = CacheService.GetDroplets();

            var updateRequired = true;

            //for (var i = 0; i < Items.Count; i++)
            //{
            //    var dialog = dialogs[i];
            //    if (Items[i] != dialog)
            //    {
            //        updateRequired = true;
            //        break;
            //    }
            //}

            if (!updateRequired) return;

            Execute.BeginOnUIThread(() =>
            {
                Items.Clear();
                foreach (var dialog in dialogs.OrderByDescending(x => x.CreatedAt))
                {
                    Items.Add(dialog);
                }
            });
        }

        public override void RefreshItems()
        {
            if (Util.IsNetworkAvailable)
            {
                LoadDroplets();
            }
            else
            {

            }
        }

        /// <summary>
        /// Show confirmation message for deleting a droplet        
        /// </summary>
        /// <param name="caption">Message title</param>
        /// <param name="message">User action message</param>
        public void ConfirmDeleteDroplet(Droplet droplet)
        {
            if (droplet == null) return;
            //Create a new custom message box
            CustomMessageBox messageBox = new CustomMessageBox()
            {
                Caption = "Delete droplet..",
                Message = string.Format("Are you sure you want to delete droplet {0}", droplet.Name),
                Background = (Brush)App.Current.Resources["drops_background"],
                Foreground = (Brush)App.Current.Resources["drops_dark_text"],
                RightButtonContent = "Go ahead!",
                LeftButtonContent = "Cancel",
                IsFullScreen = false
            };

            //Define the dismissed event handler
            messageBox.Dismissed += (s1, e1) =>
            {
                switch (e1.Result)
                {
                    case CustomMessageBoxResult.RightButton:
                        //Dismiss
                        messageBox.Dismiss();
                        //Delete
                        DeleteDroplet(droplet);
                        break;
                    case CustomMessageBoxResult.LeftButton:
                        messageBox.Dismiss();
                        break;
                    default:
                        break;
                }
            };

            // Launch the task
            messageBox.Show();
        }
    }
}