﻿using Caliburn.Micro;
using Drops.Api.Cache;
using Drops.Common;
using Drops.Resources;
using Drops.Services;
using Microsoft.Phone.Shell;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Execute = Drops.Common.Execute;

namespace Drops.ViewModels
{
    public class SettingsViewModel : ViewModelBase
    {
        private readonly IPushService _pushService;

        private bool _locationServices;

        public bool LocationServices
        {
            get { return _locationServices; }
            set
            {
                SetField(ref _locationServices, value, () => LocationServices);
                StateService.GetNotifySettingsAsync(settings =>
                {
                    settings.LocationServices = value;
                    StateService.SaveNotifySettingsAsync(settings);
                });
            }
        }

        private string _token;

        public string Token
        {
            get { return _token; }
            set
            {
                SetField(ref _token, value.Trim(), () => Token);
                StateService.GetNotifySettingsAsync(settings =>
                {
                    settings.Token = value.Trim();
                    StateService.SaveNotifySettingsAsync(settings);
                });
            }
        }

        public SettingsViewModel(ICacheService cacheService, INavigationService navigationService, IStateService stateService, IPushService pushService, ICommonErrorHandler errorHandler, IEventAggregator eventAggregator) :
            base (cacheService, navigationService, stateService, pushService, errorHandler, eventAggregator)
        {
            eventAggregator.Subscribe(this);

            //tombstoning
            //if (stateService.CurrentDroplet == null)
            //{
            //    stateService.ClearNavigationStack = true;
            //    navigationService.UriFor<DashboardViewModel>().Navigate();
            //    return;
            //}

            //CurrentItem = stateService.CurrentDroplet;
            //stateService.CurrentContact = null;

            _pushService = pushService;

            StateService.GetNotifySettingsAsync(
                settings =>
                {
                    _locationServices = settings.LocationServices;
                    _token = settings.Token;

                    BeginOnUIThread(() =>
                    {
                        NotifyOfPropertyChange(() => LocationServices);
                        NotifyOfPropertyChange(() => Token);
                    });
                });

            PropertyChanged += OnPropertyChanged;

        }

        public async void OpenLockScreenSettings()
        {
#if WP8
            await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings-lock:"));
#endif
        }

        public void OpenCacheSettings()
        {
            NavigationService.UriFor<CacheViewModel>().Navigate();
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
        }

        public void LogOut()
        {
            var result = MessageBox.Show(AppResources.LogOutConfirmation, AppResources.Confirm, MessageBoxButton.OKCancel);
            if (result != MessageBoxResult.OK) return;

            _pushService.UnregisterDeviceAsync(() =>

                Execute.BeginOnUIThread(() =>
                {
                    foreach (var activeTile in ShellTile.ActiveTiles)
                    {
                        if (activeTile.NavigationUri.ToString().Contains("Action=SecondaryTile"))
                        {
                            activeTile.Delete();
                        }
                    }
                }));

            LogOutCommon(EventAggregator, CacheService, StateService, _pushService, NavigationService);
        }

        public static void LogOutCommon(IEventAggregator eventAggregator, ICacheService cacheService, IStateService stateService, IPushService pushService, INavigationService navigationService)
        {
            eventAggregator.Publish(Commands.LogOutCommand);

            SettingsHelper.SetValue(Constants.TokenKey, string.Empty);
            //SettingsHelper.RemoveValue(Constants.ConfigKey);
            cacheService.ClearAsync();

            if (navigationService.CurrentSource == navigationService.UriFor<TokenViewModel>().BuildUri())
            {
                return;
            }

            stateService.ClearNavigationStack = true;
            Caliburn.Micro.Execute.OnUIThread(() => navigationService.UriFor<TokenViewModel>().Navigate());
        }

    }
}
