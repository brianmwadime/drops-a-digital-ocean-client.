﻿using Caliburn.Micro;
using Drops.Api;
using Drops.Api.Cache;
using Drops.Api.Cache.EventArgs;
using Drops.Api.Models.Responses;
using Drops.Common;
using Drops.Resources;
using Drops.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Execute = Drops.Common.Execute;
using Image = Drops.Api.Models.Responses.Image;

namespace Drops.ViewModels
{
    public class SnapshotsViewModel : ItemsViewModelBase<Image>,
        IHandle<string>,
        IHandle<ImageRemovedEventArgs>,
        IHandle<ImageAddedEventArgs>,
        IHandle<ImageUpdateCompletedEventArgs>
    {
        public string EmptyDashboardImageSource
        {
            get
            {
                return "/Assets/General/drops.droplets-WXGA.png";
            }
        }

        private bool _isEmptyDashboard;

        public bool IsEmptyDashboard
        {
            get { return _isEmptyDashboard; }
            set { SetField(ref _isEmptyDashboard, value, () => IsEmptyDashboard); }
        }

        public bool FirstRun { get; set; }

        public int Id { get; set; }

        public SnapshotsViewModel(ICacheService cacheService, INavigationService navigationService, IStateService stateService, IPushService pushService, ICommonErrorHandler errorHandler, IEventAggregator eventAggregator)
            : base(cacheService, navigationService, stateService, pushService, errorHandler, eventAggregator)
        {
            Items = new ObservableCollection<Image>();

            EventAggregator.Subscribe(this);

            BeginOnThreadPool(() =>
            {
                var isAuthorized = SettingsHelper.GetValue<string>(Constants.TokenKey);
                if (!string.IsNullOrEmpty(isAuthorized))
                {
                    var snapshots = CacheService.GetSnapshots();

                    IsEmptyDashboard = snapshots.Count == 0;

                    var snapshotsCache = new Dictionary<int, Image>();
                    var clearedSnapshots = new List<Image>();

                    foreach (var snapshot in snapshots)
                    {
                        if (!snapshotsCache.ContainsKey(snapshot.Id))
                        {
                            clearedSnapshots.Add(snapshot);
                            snapshotsCache[snapshot.Id] = snapshot;
                        }
                        else
                        {
                            var cachedSnapshot = snapshotsCache[snapshot.Id];
                            if (cachedSnapshot is Image)
                            {
                                CacheService.DeleteSnapshot(snapshot);
                                continue;
                            }
                        }
                    }

                    // load cache
                    // IsWorking = true;
                    // IsWorkingAction = droplets.Count == 0 ? AppResources.Loading : string.Empty;
                    LazyItems.Clear();
                    {
                        foreach (var snapshot in clearedSnapshots)
                        {
                            LazyItems.Add(snapshot);
                        }
                    }

                    if (LazyItems.Count == 0)
                    {
                        if (Util.IsNetworkAvailable)
                            UpdateSnapshotsAsync();
                    }
                    else
                    {
                        BeginOnUIThread(() => PopulateItems(() =>
                        {
                            if (Util.IsNetworkAvailable)
                                UpdateSnapshotsAsync();
                        }));
                    }
                }
            });

        }

        private async void UpdateSnapshotsAsync()
        {
            var client = new DropsClient(SettingsHelper.GetValue<string>(Constants.TokenKey));

            IsWorking = true;
            var snapshots = await client.Droplets.GetSnapshots(Id);

            IsEmptyDashboard = snapshots.Response.Count == 0;

            IsWorkingAction = snapshots.Response.Count == 0 ? AppResources.Loading : string.Empty;

            if (snapshots.Error != null)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {

                    var toast = new Drops.Controls.Notifications.ToastPrompt
                    {
                        DataContext = snapshots,
                        TextOrientation = Orientation.Horizontal,
                        Foreground = (SolidColorBrush)Application.Current.Resources["drops_white"],
                        Background = (SolidColorBrush)Application.Current.Resources["drops_blue"],
                        FontSize = (double)Application.Current.Resources["PhoneFontSizeSmall"],
                        Title = "Drops",
                        Message = snapshots.Error.message,
                        ImageHeight = 48,
                        ImageWidth = 48,
                        ImageSource = new BitmapImage(new Uri("/ToastPromptIcon.png", UriKind.Relative))
                    };

                    toast.Tap += (sender, args) =>
                    {
#if DEBUG
                        Debug.WriteLine("Toast Notification in Snapshots clicked");
#endif
                    };

                    toast.Show();
                });
            }
            else
            {
                BeginOnUIThread(() =>
                {
                    Items.Clear();
                    foreach (Image snapshot in snapshots.Response.OrderByDescending(x => x.CreatedAt))
                    {
                        Items.Add(snapshot);
                        CacheService.SyncSnapshots(snapshot, null);

                    }
                });
            }


            IsWorking = false;
            IsWorkingAction = string.Empty;
        }

        public void Handle(ImageUpdateCompletedEventArgs message)
        {
            throw new NotImplementedException();
        }

        public void Handle(ImageAddedEventArgs message)
        {
            throw new NotImplementedException();
        }

        public void Handle(ImageRemovedEventArgs message)
        {
            throw new NotImplementedException();
        }

        public void Handle(string message)
        {
            if (string.Equals(message, Commands.LogOutCommand))
            {
                LazyItems.Clear();
                BeginOnUIThread(() => Items.Clear());
                IsWorkingAction = string.Empty;
                IsWorking = false;
            }
        }
    }
}
