﻿using Caliburn.Micro;
using Drops.Api.Cache;
using Drops.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Drops.ViewModels
{
    public abstract class ItemsViewModelBase : ViewModelBase
    {
        private string _status;

        public string Status
        {
            get { return _status; }
            set { SetField(ref _status, value, () => Status); }
        }

        private bool _isScrolling;

        public bool IsScrolling
        {
            get { return _isScrolling; }
            set { SetField(ref _isScrolling, value, () => IsScrolling); }
        }

        protected volatile bool IsLastSliceLoaded;

        protected ItemsViewModelBase(ICacheService cacheService, INavigationService navigationService, IStateService stateService, IPushService pushService, ICommonErrorHandler errorHandler, IEventAggregator eventAggregator)
            : base(cacheService, navigationService, stateService, pushService, errorHandler, eventAggregator)
        {

        }

        protected virtual void OnPopulateCompleted()
        {
        }

        protected virtual void OnPopulateItemCompleted(object item)
        {

        }

        public virtual void RefreshItems()
        {

        }
    }

    public abstract class ItemsViewModelBase<T> : ItemsViewModelBase
    {


        #region Private fields

        private T _selectedItem;

        protected readonly ObservableCollection<T> LazyItems = new ObservableCollection<T>();
        #endregion

        #region Protected fields

        #endregion

        #region Public fields

        public ObservableCollection<T> Items { get; protected set; }

        public T SelectedItem
        {
            get { return _selectedItem; }
            set { SetField(ref _selectedItem, value, () => SelectedItem); }
        }
        #endregion

        private bool _populateToBegin;

        #region Constructor
        protected ItemsViewModelBase(ICacheService cacheService, INavigationService navigationService, IStateService stateService, IPushService pushService, ICommonErrorHandler errorHandler, IEventAggregator eventAggregator)
            : base(cacheService, navigationService, stateService, pushService, errorHandler, eventAggregator)
        {
            Items = new BindableCollection<T>();

            _timer.Tick += (s, e) =>
            {
                /*if (IsScrolling)
                {
                    _timer.Stop();
                    return;
                }*/

                if (LazyItems.Count <= 0)
                {

                    _populateToBegin = false;
                    _timer.Stop();
                    //OnPopulateCompleted();
                    return;
                }

                var addedItem = LazyItems.First();
                if (_populateToBegin)
                {
                    Items.Insert(0, addedItem);
                }
                else
                {
                    Items.Add(addedItem);
                }
                OnPopulateItemCompleted(addedItem);
                LazyItems.RemoveAt(0);

                if (LazyItems.Count <= 0)
                {
                    _populateToBegin = false;
                    _timer.Stop();
                    OnPopulateCompleted();
                }
            };
        }
        #endregion

        private readonly DispatcherTimer _timer = new DispatcherTimer { Interval = TimeSpan.FromTicks(20) };

        protected void PopulateItems()
        {
            _timer.Start();
        }

        protected void PopulateItems(System.Action callback)
        {
            _populateCallback = callback;
            _timer.Start();
        }

        private System.Action _populateCallback;

        protected override void OnPopulateCompleted()
        {
            if (_populateCallback != null)
            {
                var action = _populateCallback;
                _populateCallback = null;
                action();
            }

            base.OnPopulateCompleted();
        }

        protected void PopulateItemsToBegin()
        {
            _populateToBegin = true;
            _timer.Start();
        }
    }
}
