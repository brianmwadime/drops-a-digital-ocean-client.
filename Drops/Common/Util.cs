﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Drops.Common
{
    public static partial class Util
    {

        /// <summary>Animates the specified from.</summary>
        /// <param name="target">The target.</param>
        /// <param name="from">From.</param>
        /// <param name="to">To.</param>
        /// <param name="propertyPath">The property path.</param>
        /// <param name="duration">The duration.</param>
        /// <param name="startTime">The start time.</param>
        /// <param name="easing">The easing.</param>
        /// <param name="completed">The completed.</param>
        public static void Animate(this DependencyObject target, double? from, double to,
                                  object propertyPath, int duration, int startTime,
                                  IEasingFunction easing = null, Action completed = null)
        {
            if (easing == null)
            {
                easing = new SineEase();
            }

            var db = new DoubleAnimation();
            db.To = to;
            db.From = from;
            db.EasingFunction = easing;
            db.Duration = TimeSpan.FromMilliseconds(duration);
            Storyboard.SetTarget(db, target);
            Storyboard.SetTargetProperty(db, new PropertyPath(propertyPath));

            var sb = new Storyboard();
            sb.BeginTime = TimeSpan.FromMilliseconds(startTime);

            if (completed != null)
            {
                sb.Completed += (s, e) => completed();
            }

            sb.Children.Add(db);
            sb.Begin();
        }

        public static void SetHorizontalOffset(this FrameworkElement fe, double offset)
        {
            var translateTransform = fe.RenderTransform as TranslateTransform;
            if (translateTransform == null)
            {
                // create a new transform if one is not alreayd present
                var trans = new TranslateTransform()
                {
                    X = offset
                };
                fe.RenderTransform = trans;
            }
            else
            {
                translateTransform.X = offset;
            }
        }

        public static Offset GetHorizontalOffset(this FrameworkElement fe)
        {
            var trans = fe.RenderTransform as TranslateTransform;
            if (trans == null)
            {
                // create a new transform if one is not alreayd present
                trans = new TranslateTransform()
                {
                    X = 0
                };
                fe.RenderTransform = trans;
            }
            return new Offset()
            {
                Transform = trans,
                Value = trans.X
            };
        }

        public static void SetVerticalOffset(this FrameworkElement fe, double offset)
        {
            var translateTransform = fe.RenderTransform as TranslateTransform;
            if (translateTransform == null)
            {
                var trans = new TranslateTransform()
                {
                    Y = offset
                };
                fe.RenderTransform = trans;
            }
            else
            {
                translateTransform.Y = offset;
            }
        }

        public static Offset GetVerticalOffset(this FrameworkElement fe)
        {
            var trans = fe.RenderTransform as TranslateTransform;
            if (trans == null)
            {
                trans = new TranslateTransform()
                {
                    Y = 0
                };
                fe.RenderTransform = trans;
            }
            return new Offset()
            {
                Transform = trans,
                Value = trans.Y
            };
        }

        public struct Offset
        {
            public double Value { get; set; }
            public TranslateTransform Transform { get; set; }
        }

        public static void InvokeOnNextLayoutUpdated(this FrameworkElement element, Action action)
        {
            EventHandler handler = null;
            handler = (s, e2) =>
            {
                element.LayoutUpdated -= handler;
                action();
            };
            element.LayoutUpdated += handler;
        }

        public static bool DeleteFile(object syncRoot, string fileName)
        {
            try
            {
                lock (syncRoot)
                {
                    var storage = IsolatedStorageFile.GetUserStoreForApplication();

                    if (storage.FileExists(fileName))
                    {
                        storage.DeleteFile(fileName);
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine("FILE ERROR: cannot delete file " + fileName, LogSeverity.Error);
                WriteException(e);

                return false;
            }
            return true;
        }

        public static T OpenObjectFromFile<T>(object syncRoot, string fileName)
            where T : class
        {
            try
            {
                lock (syncRoot)
                {
                    var file = IsolatedStorageFile.GetUserStoreForApplication();

                    using (var fileStream = new IsolatedStorageFileStream(fileName, FileMode.OpenOrCreate, file))
                    {
                        if (fileStream.Length > 0)
                        {
                            var serializer = new DataContractSerializer(typeof(T));
                            return serializer.ReadObject(fileStream) as T;
                        }

                    }
                }
            }
            catch (Exception e)
            {
                WriteLine("FILE ERROR: cannot read " + typeof(T) + " from file " + fileName, LogSeverity.Error);
                WriteException(e);
            }
            return default(T);
        }

        public static void SaveObjectToFile<T>(object syncRoot, string fileName, T data)
        {
            try
            {
                lock (syncRoot)
                {
                    var file = IsolatedStorageFile.GetUserStoreForApplication();

                    using (var fileStream = new IsolatedStorageFileStream(fileName, FileMode.Create, file))
                    {
                        var dcs = new DataContractSerializer(typeof(T));
                        dcs.WriteObject(fileStream, data);
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine("FILE ERROR: cannot write " + typeof(T) + " to file " + fileName, LogSeverity.Error);
                WriteException(e);
            }
        }

        public static Brush ColorToBrush(string color) // color = "#E7E44D"
        {
            color = color.Replace("#", "");
            if (color.Length == 6)
            {
                return new SolidColorBrush(Color.FromArgb(255,
                    byte.Parse(color.Substring(0, 2), System.Globalization.NumberStyles.HexNumber),
                    byte.Parse(color.Substring(2, 2), System.Globalization.NumberStyles.HexNumber),
                    byte.Parse(color.Substring(4, 2), System.Globalization.NumberStyles.HexNumber)));
            }
            else
            {
                return null;
            }
        }
    }
}
