﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Common
{
    public class DataRequestHelper
    {
        public event EventHandler AsyncResponseCompleted;
        /// <summary>
        /// Excute Request async method operator [restshare version]
        /// </summary>
        /// <param name="requestUrl">request url</param>
        /// <param name="requestMethod">request method</param>
        /// <param name="postArgumentList">when use post method then argument collection</param>
        /// <param name="postFileList">upload files parameter collection</param>
        public void ExcuteAsyncRequest(string requestUrl, Method requestMethod, List<KeyValuePair<string, object>> postArgumentList = null, List<FileParameter> postFileList = null)
        {
            RestClient restClient = new RestClient(requestUrl);
            restClient.Timeout = TimeSpan.FromSeconds(10).Milliseconds;
            RestRequest restRequest = new RestRequest(requestMethod);

            if (postArgumentList != null)
                postArgumentList.ForEach(queryArgument => { restRequest.AddParameter(queryArgument.Key, queryArgument.Value); });

            if (postFileList != null)
                postFileList.ForEach(queryFile => { restRequest.Files.Add(queryFile); });

            try
            {
                restClient.ExecuteAsync(restRequest, (respontData) =>
                {
                    if (AsyncResponseCompleted != null)
                        AsyncResponseCompleted(respontData.Content, null);
                });
            }
            catch (WebException ex)
            {
                //if (AsyncResponseCompleted != null)
                    //if (ex.IsNetworkFailure())
                    //{
                    //    AsyncResponseCompleted(AppResources.NoInternetConnection, null);
                    //}
                    //else
                    //{
                    //    AsyncResponseCompleted(ex.Message, null);
                    //}
                //}
            }


        }
    }

    public enum RequestType
    {
        GET,
        POST
    }
}
