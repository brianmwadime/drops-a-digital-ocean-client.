﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace Drops.Common
{
    public class DropsUriMapper : UriMapperBase
    {
        public override Uri MapUri(Uri uri)
        {
            var tempUri = HttpUtility.UrlDecode(uri.ToString());
#if DEBUG
            Util.WriteLog(tempUri);
#endif
            return uri;
        }

        public static Dictionary<string, string> ParseQueryString(string uri)
        {

            string substring = uri.Substring(((uri.LastIndexOf('?') == -1) ? 0 : uri.LastIndexOf('?') + 1));

            string[] pairs = substring.Split('&');

            var output = new Dictionary<string, string>();

            foreach (string piece in pairs)
            {
                string[] pair = piece.Split('=');
                if (pair.Length > 1)
                {
                    output.Add(pair[0], pair[1]);
                }
            }

            return output;

        }
    
    }
}
