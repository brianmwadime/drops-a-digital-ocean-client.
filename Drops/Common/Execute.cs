﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Drops.Extensions;

namespace Drops.Common
{
    public static class Execute
    {
        public static void BeginOnThreadPool(TimeSpan delay, Action action)
        {
            ThreadPool.QueueUserWorkItem(state =>
            {
                Thread.Sleep(delay);
                try
                {
                    action.SafeInvoke();
                }
                catch (Exception ex)
                {
#if DEBUG
                    Debug.WriteLine(ex);
#endif
                }
            });
        }

        public static void BeginOnThreadPool(Action action)
        {
            ThreadPool.QueueUserWorkItem(state =>
            {
                try
                {
                    action.SafeInvoke();
                }
                catch (Exception ex)
                {
#if DEBUG
                    Debug.WriteLine(ex);
#endif
                }
            });
        }

        public static void BeginOnUIThread(Action action)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    action.SafeInvoke();
                }
                catch (Exception ex)
                {
#if DEBUG
                    Debug.WriteLine(ex);
#endif
                }
            });
        }

        public static void BeginOnUIThread(TimeSpan delay, Action action)
        {
            BeginOnThreadPool(() =>
            {
                Thread.Sleep(delay);
                BeginOnUIThread(action);
            });
        }

        public static void ShowDebugMessage(string message)
        {
#if DEBUG
            BeginOnUIThread(() => MessageBox.Show(message));
#endif
        }
    }
}
