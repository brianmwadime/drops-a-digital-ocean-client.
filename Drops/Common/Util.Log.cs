﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Security.Cryptography;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
#if SILVERLIGHT
using System.Windows;
#endif

namespace Drops.Common
{
    public enum LogSeverity
    {
        Error,
        Warning,
        Info
    }

    public static partial class Util
    {
        private static bool _isLogEnabled = false;

        public static bool IsLogEnabled
        {
            get { return _isLogEnabled; }
            set { _isLogEnabled = value; }
        }

        private static bool _isDebugEnabled = false;

        public static bool IsDebugEnabled
        {
            get { return _isDebugEnabled; }
            set { _isDebugEnabled = value; }
        }

        private static bool _isLongPollLogEnabled = false;

        public static bool IsLongPollDebugEnabled
        {
            get { return _isLongPollLogEnabled; }
            set { _isLongPollLogEnabled = value; }
        }
#if DEBUG
        private static bool _isPerformanceLogEnabled = true;
#else
        private static bool _isPerformanceLogEnabled = false;

#endif


        public static bool IsPerformanceLogEnabled
        {
            get { return _isPerformanceLogEnabled; }
            set { _isPerformanceLogEnabled = value; }
        }

        public static ObservableCollection<string> LongPollItems = new ObservableCollection<string>();

        public static ObservableCollection<string> PerformanceItems = new ObservableCollection<string>();

        public static ObservableCollection<string> DebugItems = new ObservableCollection<string>();

        public static ObservableCollection<string> LogItems = new ObservableCollection<string>();


        public static void WritePerformance(string str)
        {
            //#if RELEASE

            if (!IsPerformanceLogEnabled) return;


#if SILVERLIGHT
            Deployment.Current.Dispatcher.BeginInvoke(() => PerformanceItems.Add(str));
#else
            Console.WriteLine("  PERFORMANCE: " + str);
#endif

            //#endif
        }

        public static void WriteLog(string str)
        {
            if (!IsLogEnabled) return;

#if SILVERLIGHT
            var timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture);
            Deployment.Current.Dispatcher.BeginInvoke(() => LogItems.Add(timestamp + ": " + str));
#else
            Console.WriteLine("  PERFORMANCE: " + str);
#endif
        }

        public static void WriteLongPoll(string str)
        {
            if (!IsLongPollDebugEnabled) return;
#if SILVERLIGHT
            Deployment.Current.Dispatcher.BeginInvoke(() => LongPollItems.Add(str));
#else
            LongPollItems.Add(str);
#endif

        }

        public static void WriteLine(LogSeverity severity = LogSeverity.Info)
        {
#if SILVERLIGHT
            if (!IsDebugEnabled && severity != LogSeverity.Error) return;

            Deployment.Current.Dispatcher.BeginInvoke(() => DebugItems.Add(" "));
#else
            Console.WriteLine();
#endif
        }

        public static void WriteLineAtBegin(string str, LogSeverity severity = LogSeverity.Info)
        {
#if SILVERLIGHT
            if (!IsDebugEnabled && severity != LogSeverity.Error) return;

            Deployment.Current.Dispatcher.BeginInvoke(() => DebugItems.Insert(0, str));
#else
            Console.WriteLine(str);
#endif
        }

        public static void WriteException(string caption, Exception e)
        {
#if DEBUG
            Deployment.Current.Dispatcher.BeginInvoke(() => MessageBox.Show(caption + Environment.NewLine + e));
#endif


#if SILVERLIGHT
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    DebugItems.Add(e.ToString());
                    //BugSenseHandler.Instance.LogError(e, caption, new NotificationOptions { Type = enNotificationType.None });
                }
                catch (Exception)
                {

                }
            });
#else
            Console.WriteLine(str);
#endif
        }

        public static void WriteException(Exception e)
        {
#if DEBUG
            Deployment.Current.Dispatcher.BeginInvoke(() => MessageBox.Show(e.ToString()));
#endif


#if SILVERLIGHT
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                try
                {
                    DebugItems.Add(e.ToString());
                    //BugSenseHandler.Instance.LogError(e, null, new NotificationOptions { Type = enNotificationType.None });
                }
                catch (Exception)
                {

                }
            });
#else
            Console.WriteLine(str);
#endif
        }

        public static void WriteLine(string str, LogSeverity severity = LogSeverity.Info)
        {
#if DEBUG
#if SILVERLIGHT
            //Debug.WriteLine(str);

            if (!IsDebugEnabled && severity != LogSeverity.Error) return;

            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                DebugItems.Add(str);
            });
#else
            Console.WriteLine(str);
#endif
#endif
        }

        public static void WriteLine<T>(string fieldName, T fieldValue, LogSeverity severity = LogSeverity.Info)
        {
#if SILVERLIGHT
            if (!IsDebugEnabled && severity != LogSeverity.Error) return;

            Deployment.Current.Dispatcher.BeginInvoke(() => DebugItems.Add(String.Format("{0}: {1}", fieldName, fieldValue)));
#else

            Console.WriteLine("{0}: {1}", fieldName, fieldValue);
#endif
        }

        public static string WriteThreadInfo()
        {
            //return string.Empty;
            return "ThreadId " + Thread.CurrentThread.ManagedThreadId;
        }
    }
}
