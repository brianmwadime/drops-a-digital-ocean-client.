﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Drops.Controls
{
    public class FloatLabelTextBox : TextBox
    {

        public FloatLabelTextBox()
        {
            DefaultStyleKey = typeof(FloatLabelTextBox);
            SetCaretToAccentColor();
            TextChanged += OnTextChanged;
            Loaded += OnLoaded;
        }

        public static readonly DependencyProperty LabelProperty =
            DependencyProperty.Register("Label", typeof(string), typeof(FloatLabelTextBox), new PropertyMetadata(""));

        public static readonly DependencyProperty LabelFontSizeProperty =
            DependencyProperty.Register("LabelFontSize", typeof(string), typeof(FloatLabelTextBox), new PropertyMetadata(""));

        [Category("Common")]
        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        [Category("Common")]
        public string LabelFontSize
        {
            get { return (string)GetValue(LabelFontSizeProperty); }
            set { SetValue(LabelFontSizeProperty, value); }
        }

        public void OnLoaded(object sender, RoutedEventArgs e)
        {
            UpdateTextVisualState(false);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            UpdateTextVisualState(false);
        }

        private void OnTextChanged(object sender, TextChangedEventArgs args)
        {
            UpdateTextVisualState();
        }

        private void UpdateTextVisualState(bool isAnimated = true)
        {
            if (string.IsNullOrEmpty(Text))
                VisualStateManager.GoToState(this, "WithoutText", isAnimated);
            else
                VisualStateManager.GoToState(this, "WithText", isAnimated);
        }

        private void SetCaretToAccentColor()
        {
            var brush = (SolidColorBrush)Application.Current.Resources["PhoneAccentBrush"];
            CaretBrush = new SolidColorBrush(brush.Color);
        }
    }
}
