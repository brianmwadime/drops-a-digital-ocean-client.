﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Controls
{
    /// <summary>
    /// Has navigation-in
    /// <see cref="T:Microsoft.Phone.Controls.TransitionElement"/>s
    /// for the designer experiences.
    /// </summary>
    public class DropsNavigationInTransition : DropsNavigationTransition
    {
    }
}
