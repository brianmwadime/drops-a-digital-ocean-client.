﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Drops.Controls
{
    /// <summary>
    /// Provides attached properties for navigation
    /// <see cref="T:Microsoft.Phone.Controls.ITransition"/>s.
    /// </summary>
    /// <QualityBand>Preview</QualityBand>
    public static class DropsTransitionService
    {
        /// <summary>
        /// The
        /// <see cref="T:System.Windows.DependencyProperty"/>
        /// for the in <see cref="T:Microsoft.Phone.Controls.ITransition"/>s.
        /// </summary>
        public static readonly DependencyProperty NavigationInTransitionProperty =
            DependencyProperty.RegisterAttached("NavigationInTransition", typeof(DropsNavigationInTransition), typeof(DropsTransitionService), null);

        /// <summary>
        /// The
        /// <see cref="T:System.Windows.DependencyProperty"/>
        /// for the in <see cref="T:Microsoft.Phone.Controls.ITransition"/>s.
        /// </summary>
        public static readonly DependencyProperty NavigationOutTransitionProperty =
            DependencyProperty.RegisterAttached("NavigationOutTransition", typeof(DropsNavigationOutTransition), typeof(DropsTransitionService), null);

        /// <summary>
        /// Gets the
        /// <see cref="T:Microsoft.Phone.Controls.NavigationTransition"/>s
        /// of
        /// <see cref="M:Microsoft.Phone.Controls.TransitionService.NavigationInTransitionProperty"/>
        /// for a
        /// <see cref="T:System.Windows.UIElement"/>.
        /// </summary>
        /// <param name="element">The <see cref="T:System.Windows.UIElement"/>.</param>
        /// <returns>The </returns>
        public static DropsNavigationInTransition GetNavigationInTransition(UIElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (DropsNavigationInTransition)element.GetValue(NavigationInTransitionProperty);
        }

        /// <summary>
        /// Gets the
        /// <see cref="T:Microsoft.Phone.Controls.NavigationTransition"/>s
        /// of
        /// <see cref="M:Microsoft.Phone.Controls.TransitionService.NavigationOutTransitionProperty"/>
        /// for a
        /// <see cref="T:System.Windows.UIElement"/>.
        /// </summary>
        /// <param name="element">The <see cref="T:System.Windows.UIElement"/>.</param>
        /// <returns>The </returns>
        public static DropsNavigationOutTransition GetNavigationOutTransition(UIElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            return (DropsNavigationOutTransition)element.GetValue(NavigationOutTransitionProperty);
        }

        /// <summary>
        /// Sets a
        /// <see cref="T:Microsoft.Phone.Controls.NavigationTransition"/>
        /// to
        /// <see cref="M:Microsoft.Phone.Controls.TransitionService.NavigationInTransitionProperty"/>
        /// for a
        /// <see cref="T:System.Windows.UIElement"/>.
        /// </summary>
        /// <param name="element">The <see cref="T:System.Windows.UIElement"/>.</param>
        /// <param name="value">The <see cref="T:Microsoft.Phone.Controls.NavigationTransition"/>.</param>
        /// <returns>The </returns>
        public static void SetNavigationInTransition(UIElement element, DropsNavigationInTransition value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            element.SetValue(NavigationInTransitionProperty, value);
        }

        /// <summary>
        /// Sets a
        /// <see cref="T:Microsoft.Phone.Controls.NavigationTransition"/>s
        /// to
        /// <see cref="M:Microsoft.Phone.Controls.TransitionService.NavigationOutTransitionProperty"/>
        /// for a
        /// <see cref="T:System.Windows.UIElement"/>.
        /// </summary>
        /// <param name="element">The <see cref="T:System.Windows.UIElement"/>.</param>
        /// <param name="value">The <see cref="T:Microsoft.Phone.Controls.NavigationTransition"/>.</param>
        /// <returns>The </returns>
        public static void SetNavigationOutTransition(UIElement element, DropsNavigationOutTransition value)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }
            element.SetValue(NavigationOutTransitionProperty, value);
        }
    }
}
