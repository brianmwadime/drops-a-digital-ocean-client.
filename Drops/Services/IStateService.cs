﻿using Drops.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Services
{
    public interface IStateService
    {
        bool FirstRun { get; set; }
        string Token { get; set; }
        bool RemoveBackEntry { get; set; }
        bool RemoveBackEntries { get; set; }
        bool ClearNavigationStack { get; set; }

        bool Tombstoning { get; set; }

        Droplet CurrentDroplet { get; set; }

        Object With { get; set; }

        bool SuppressNotifications { get; set; }

        bool SaveByEnter { get; set; }

        //List<string> Sounds { get; }

        void GetNotifySettingsAsync(Action<Settings> callback);
        void SaveNotifySettingsAsync(Settings settings);
        void ClearState();
    }
}
