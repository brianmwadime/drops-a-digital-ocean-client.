﻿using Drops.Common;
using Microsoft.Phone.Notification;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Drops.Extensions;
using System.Threading.Tasks;

namespace Drops.Services
{
    public class PushService : IPushService
    {
        private HttpNotificationChannel _pushChannel;

        private readonly IDropsService _service;

        public PushService(IDropsService service)
        {
            _service = service;

            LoadOrCreateChannelAsync();
        }

        private void LoadOrCreateChannelAsync(Action callback = null)
        {
            Execute.BeginOnThreadPool(() =>
            {
                _pushChannel = HttpNotificationChannel.Find(Constants.ToastNotificationChannelName);

                if (_pushChannel == null)
                {
                    _pushChannel = new HttpNotificationChannel(Constants.ToastNotificationChannelName);
                    _pushChannel.HttpNotificationReceived += OnHttpNotificationReceived;
                    _pushChannel.ChannelUriUpdated += OnChannelUriUpdated;
                    _pushChannel.ErrorOccurred += OnErrorOccurred;
                    _pushChannel.ShellToastNotificationReceived += OnShellToastNotificationReceived;

                    try
                    {
                        _pushChannel.Open();
                    }
                    catch (Exception e)
                    {
                        Util.WriteException(e);
                    }

                    if (_pushChannel != null && _pushChannel.ChannelUri != null)
                    {
#if DEBUG
                        Debug.WriteLine("Channel URL : {0}",_pushChannel.ChannelUri.ToString());
#endif
                    }
                    _pushChannel.BindToShellToast();
                    _pushChannel.BindToShellTile();
                }
                else
                {
                    _pushChannel.HttpNotificationReceived += OnHttpNotificationReceived;
                    _pushChannel.ChannelUriUpdated += OnChannelUriUpdated;
                    _pushChannel.ErrorOccurred += OnErrorOccurred;
                    _pushChannel.ShellToastNotificationReceived += OnShellToastNotificationReceived;

                    if (!_pushChannel.IsShellTileBound)
                    {
                        _pushChannel.BindToShellToast();
                    }
                    if (!_pushChannel.IsShellTileBound)
                    {
                        _pushChannel.BindToShellTile();
                    }
                }
                callback.SafeInvoke();
            });
        }


        private void OnHttpNotificationReceived(object sender, HttpNotificationEventArgs e)
        {

        }

        private void OnShellToastNotificationReceived(object sender, NotificationEventArgs e)
        {

        }

        private void OnErrorOccurred(object sender, NotificationChannelErrorEventArgs e)
        {
            var message = string.Format("A push notification {0} error occurred.  {1} ({2}) {3}", e.ErrorType, e.Message, e.ErrorCode, e.ErrorAdditionalData);
            Execute.ShowDebugMessage(message);
#if DEBUG
            Debug.WriteLine(message);
#endif

            LoadOrCreateChannelAsync(RegisterDeviceAsync);
        }

        private void OnChannelUriUpdated(object sender, NotificationChannelUriEventArgs e)
        {
#if DEBUG
            //Debug.WriteLine(e.ChannelUri.ToString());
            Debug.WriteLine(String.Format("Channel Uri is {0}", e.ChannelUri));
#endif

            RegisterDeviceAsync();
        }

        private string _lastRegisteredUri;

        private readonly object _lastRegisteredUriRoot = new object();

        public void RegisterDeviceAsync()
        {
            Execute.BeginOnThreadPool(() =>
            {
                if (_pushChannel == null || _pushChannel.ChannelUri == null)
                {
                    return;
                }

                var isAuthorized =  SettingsHelper.GetValue<string>(Constants.TokenKey);
                if (!string.IsNullOrEmpty(isAuthorized))
                {
                    return;
                }

                lock (_lastRegisteredUriRoot)
                {
                    if (string.Equals(_lastRegisteredUri, _pushChannel.ChannelUri.ToString()))
                    {
                        return;
                    }

                    _lastRegisteredUri = _pushChannel.ChannelUri.ToString();
                }


                var systemVersion = PhoneHelper.GetOSVersion();
#if !WP8
                if (!systemVersion.StartsWith("7."))
                {
                    systemVersion = "7.0.0.0";
                }
#endif

                _service.RegisterDeviceAsync(
                    _lastRegisteredUri,
                    PhoneHelper.GetDeviceFullName(),
                    systemVersion,
                    PhoneHelper.GetAppVersion(),
                    result =>
                    {
                        //Execute.ShowDebugMessage("account.registerDevice result " + result);
                        Debug.WriteLine("account.registerDevice result " + result);
                    },
                    error =>
                    {
                        //Execute.ShowDebugMessage("account.registerDevice error " + error);
                        Debug.WriteLine("account.registerDevice error " + error);
                    });
            });
        }

        public void UnregisterDeviceAsync(Action callback)
        {
            Execute.BeginOnThreadPool(() =>
            {
                if (_pushChannel == null || _pushChannel.ChannelUri == null)
                {
                    return;
                }

                _service.UnregisterDeviceAsync(
                    _pushChannel.ChannelUri.ToString(),
                    result =>
                    {
                        Debug.WriteLine("account.unregisterDevice result " + result);
                        callback.SafeInvoke();
                    },
                    error =>
                    {
                        Debug.WriteLine("account.unregisterDevice error " + error);
                        callback.SafeInvoke();
                    });
            });
        }
    }
}
