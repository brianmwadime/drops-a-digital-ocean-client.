﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Services
{
    public interface IDropsService
    {
        void RegisterDeviceAsync(String token, String deviceModel, String systemVersion, String appVersion, Action<bool> callback, Action<Object> faultCallback = null);
        void UnregisterDeviceAsync(String token, Action<bool> callback, Action<Object> faultCallback = null);
    }
}
