﻿using Caliburn.Micro;
using Drops.Api.Common;
using Drops.Common;
using Drops.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Services
{
    class StateService : PropertyChangedBase, IStateService
    {
        public bool FirstRun { get; set; }
        public bool ClearNavigationStack { get; set; }

        //List<string> Sounds { get; set; }
        public Droplet CurrentDroplet { get; set; }

        public Object With { get; set; }

        public bool SuppressNotifications { get; set; }

        public bool Tombstoning { get; set; }

        public bool SaveByEnter
        {
            get
            {
                return SettingsHelper.GetValue<bool>(Constants.SendByEnterKey);
            }
            set
            {
                SettingsHelper.CrossThreadAccess(
                    settings =>
                    {
                        settings[Constants.SendByEnterKey] = value;
                        settings.Save();

                        NotifyOfPropertyChange(() => SaveByEnter);
                    });
            }
        }

        public bool RemoveBackEntry { get; set; }
        public bool RemoveBackEntries { get; set; }

        public string Token
        {
            get
            {
                return SettingsHelper.GetValue<string>(Constants.TokenKey);
            }
            set
            {
                SettingsHelper.CrossThreadAccess(
                    settings =>
                    {
                        settings[Constants.TokenKey] = value;
                        settings.Save();

                        NotifyOfPropertyChange(() => Token);
                    });
            }
        }

        #region Settings

        private readonly object _settingsRoot = new object();

        private Settings _settings;

        public void GetNotifySettingsAsync(Action<Settings> callback)
        {
            if (_settings != null)
            {
                callback(_settings);
                return;
            }

            Drops.Common.Execute.BeginOnThreadPool(() =>
            {
                _settings = Util.OpenObjectFromFile<Settings>(_settingsRoot, Constants.CommonNotifySettingsFileName) ?? new Settings();
                callback(_settings);
            });
        }

        public void SaveNotifySettingsAsync(Settings settings)
        {
            Util.SaveObjectToFile(_settingsRoot, Constants.CommonNotifySettingsFileName, settings);
        }

        public void ClearState()
        {
            //Utils.DeleteFile(_settingsRoot, Constants.StateFileName);
            //Utils.DeleteFile(_settingsRoot, Constants.CommonNotifySettingsFileName);
        }

        #endregion
    }
}
