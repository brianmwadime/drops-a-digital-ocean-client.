﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Services
{
    public interface IPushService
    {
        void RegisterDeviceAsync();
        void UnregisterDeviceAsync(System.Action callback);
    }
}
