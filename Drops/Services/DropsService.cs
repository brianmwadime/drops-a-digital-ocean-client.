﻿using Drops.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops.Services
{
    public class DropsService : ServiceBase, IDropsService, IDisposable
    {
        public void RegisterDeviceAsync(String token, String deviceModel, String systemVersion, String appVersion, Action<bool> callback, Action<Object> faultCallback = null)
        {
            var shortModel = PhoneHelper.GetShortPhoneModel(deviceModel.ToString());
            if (!string.IsNullOrEmpty(shortModel))
            {
                deviceModel = shortModel;
            }
        }

        public void UnregisterDeviceAsync(String token, Action<bool> callback, Action<Object> faultCallback = null)
        {

        }

        public void Dispose()
        {
        }
    }


}
