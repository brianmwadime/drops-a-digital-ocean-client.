﻿using Drops.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drops
{
    public static class Constants
    {
#if PRIVATE_BETA
        public const string LogEmail = "brianmwadime@gmail.com";
#else
        public const string LogEmail = "info@brianmwadime.com";
#endif
        public const string ConfigKey = "Config";
        public const string SettingsKey = "SettingsKey";
        public const string SendByEnterKey = "SaveByEnter";
        public const string TokenKey = "TokenKey";
        public const string CommonNotifySettingsFileName = "CommonNotifySettings.xml";
        public const string CachedServerFilesFileName = "cachedServerFiles.dat";
        public static string StateFileName = "state.dat";
        public const string DropsFolderName = "Drops";

        public const int CommitDBInterval = 3;              //seconds
        public const int GetConfigInterval = 60 * 60;       //seconds
        public const int TimeoutInterval = 25;              //seconds    

        public const bool IsLongPollEnabled = false;
        public const string ScheduledAgentTaskName = "DropsScheduledAgent";
        public const string ToastNotificationChannelName = "DropsNotificationChannel";

        public static string StaticGoogleMap = "https://maps.googleapis.com/maps/api/staticmap?center={0},{1}&zoom=12&size={2}x{3}&sensor=false&format=jpg";

        public static string _token = "5cbebc034b36b48809e8efbcc8a0dbfe0af1e55af0723465dc51a2e14fb75a0f"; //"0c6f5cf759db97d3b821df406c24b1c1085f7523dfa551449079920b9e61bb4d";
    }
}
