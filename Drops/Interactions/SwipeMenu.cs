﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Drops.Common;

namespace Drops.Interactions
{
    public static class SwipeMenu
    {
        static bool IsActive = false;

        public static bool IsEnabled { get; set; }

        private const double FlickVelocity = 2000.0;

        // the drag distance required to consider this a swipe interaction
        private static readonly double DragStartedDistance = 5.0;

        public static void DragDelta(object sender, DragDeltaGestureEventArgs e)
        {
            if (!IsEnabled)
                return;

            if (!IsActive)
            {
                // has the user dragged far enough?
                if (Math.Abs(e.HorizontalChange) < DragStartedDistance)
                    return;

                IsActive = true;

                // initialize the drag
                FrameworkElement fe = sender as FrameworkElement;
                fe.SetHorizontalOffset(0);

                
            }
            else
            {
                FrameworkElement fe = sender as FrameworkElement;
                double offset = fe.GetHorizontalOffset().Value + e.HorizontalChange;

                if (e.HorizontalChange < 0.0)
                {
                    if (offset > -258)
                    {
                        fe.SetHorizontalOffset(offset);
                    }
                }
                else
                {
                    if (offset <= 0)
                    {
                        fe.SetHorizontalOffset(offset);
                    }
                }
            }
        }

        public static void DragCompleted(object sender, DragCompletedGestureEventArgs e)
        {
            if (!IsActive)
                return;

            FrameworkElement fe = sender as FrameworkElement;
            double offset = fe.GetHorizontalOffset().Value + e.HorizontalChange;

            if (Math.Abs(e.HorizontalChange) > fe.ActualWidth / 2 ||
            Math.Abs(e.HorizontalVelocity) > FlickVelocity)
            {
                if (e.HorizontalChange < 0.0)
                {
                    MenuOpen(fe);
                }
                else
                {
                    MenuBounceBack(fe);
                    //DropletItemCompletedAction(fe);
                }
            }
            else
            {
                MenuBounceBack(fe);
            }
            //if (e.HorizontalChange < 0.0)
            //{
            //    if (offset < -125)
            //    {
            //        MenuOpen(fe);
            //    }
            //    else
            //    {
            //        MenuBounceBack(fe);
            //    }
            //}
            //else
            //{
            //    if (offset < -195)
            //    {
            //        MenuOpen(fe);
            //    }
            //    else
            //    {
            //        MenuBounceBack(fe);
            //    }
            //}
            

            IsActive = false;
        }

        public static void Flick(object sender, FlickGestureEventArgs e)
        {
            FrameworkElement fe = sender as FrameworkElement;
            double offset = fe.GetHorizontalOffset().Value;

            if (e.HorizontalVelocity < -FlickVelocity)
            {
                MenuOpen(fe);
            }
            else if (e.HorizontalVelocity > FlickVelocity)
            {
                MenuBounceBack(fe);
            }
        }

        public static void Hold(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            FrameworkElement fe = sender as FrameworkElement;
            double offset = fe.GetHorizontalOffset().Value;

            if (offset >= 0)
            {
                MenuOpen(fe);
            }
            else
            {
                MenuBounceBack(fe);
            }
        }

        public static void MenuOpen(FrameworkElement fe)
        {
            var trans = fe.GetHorizontalOffset().Transform;

            trans.Animate(trans.X, -258, TranslateTransform.XProperty, 300, 0, new SineEase()
                    {
                        EasingMode = EasingMode.EaseOut
                    });
        }

        public static void MenuBounceBack(FrameworkElement fe)
        {
            var trans = fe.GetHorizontalOffset().Transform;

            //trans.Animate(trans.X, 0, TranslateTransform.XProperty, 300, 0, new QuadraticEase());

            trans.Animate(trans.X, 0, TranslateTransform.XProperty, 300, 0, new BounceEase()
            {
                Bounciness = 5,
                Bounces = 2
            });
        }

    }
}
