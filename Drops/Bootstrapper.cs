﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Phone.Controls;
using Drops.ViewModels;
using System.ComponentModel;
using Drops.Services;
using Drops.Api;
using Caliburn.Micro;
using Drops.Controls;
using Drops.Resources;
using Drops.Common;
using System.Windows;
using Drops.Api.Cache;
using Drops.Api.Helpers;
using System.Diagnostics;
using Microsoft.Phone.Shell;
using Execute = Drops.Common.Execute;
#if WP8
using Windows.Phone.System.Power;
#endif
#if WP81
using Windows.UI.Notifications;
#endif
using System.Threading;
using Microsoft.Phone.Scheduler;
using System.Windows.Media;

namespace Drops
{
    public class Bootstrapper : PhoneBootstrapper
    {
        public PhoneContainer _container { get; set; }

        public static PhoneApplicationFrame PhoneFrame { get; protected set; }

        protected override PhoneApplicationFrame CreatePhoneApplicationFrame()
        {
            //return new PhoneApplicationFrame();
            //PhoneFrame = new DropsTransitionFrame { Title = AppResources.ApplicationTitle };
            PhoneFrame = new Microsoft.Phone.Controls.TransitionFrame();
            //PhoneFrame.Background = Util.ColorToBrush("#F6F7F7");
            PhoneFrame.UriMapper = new DropsUriMapper();
            //var stateService = IoC.Get<IStateService>();
            //var background = stateService.CurrentBackground;
            PhoneFrame.Navigate(new Uri("/Views/DashboardView.xaml", UriKind.Relative));
            return PhoneFrame;
        }

        protected override void Configure()
        {
            _container = new PhoneContainer();

            if (!DesignerProperties.IsInDesignTool)
                _container.RegisterPhoneServices(RootFrame);

            _container.PerRequest<DashboardViewModel>();
            _container.PerRequest<DropletDetailsViewModel>();
            _container.PerRequest<SettingsViewModel>();
            _container.PerRequest<CacheViewModel>();
            _container.PerRequest<TokenViewModel>();
            _container.PerRequest<BackupsViewModel>();
            _container.PerRequest<SnapshotsViewModel>();
            _container.PerRequest<AboutViewModel>();
            _container.Singleton<IDropsClient, DropsClient>();
            _container.Singleton<IStateService, StateService>();
            _container.Singleton<ICacheService, InMemoryCacheService>();
            _container.Singleton<IPushService, PushService>();
            _container.Singleton<IDropsService, DropsService>();

            SetupViewLocator();

            // avoid xaml ui designer crashes 
            if (Caliburn.Micro.Execute.InDesignMode) return;

            //AddCustomConventions();
        }

        static void AddCustomConventions()
        {
            //ellided  
        }

        protected override void OnUnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            base.OnUnhandledException(sender, e);

#if DEBUG
            Debug.WriteLine("Unhandled exception " + e);
#endif

            e.Handled = true;
        }

        protected override object GetInstance(Type service, string key)
        {
            return _container.GetInstance(service, key);
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
#if DEBUG
            Debug.WriteLine("App startup");
#endif

            base.OnStartup(sender, e);
        }

        protected override void OnLaunch(object sender, LaunchingEventArgs e)
        {
#if DEBUG
            Debug.WriteLine("App launch");
#endif

            var cacheService = IoC.Get<ICacheService>();
            cacheService.Init();

            
            LoadStateAndUpdateAsync();

            //ShowBatterySaverAlertAsync();

            //StartAgent();

            //SetInitTextScaleFactor();

            base.OnLaunch(sender, e);
        }

        protected override void OnActivate(object sender, ActivatedEventArgs e)
        {
#if DEBUG
            Debug.WriteLine("App activate IsAppInstancePreserved=" + e.IsApplicationInstancePreserved);
#endif
            if (!e.IsApplicationInstancePreserved)
            {
                var cacheService = IoC.Get<ICacheService>();
                cacheService.Init();
                Execute.ShowDebugMessage("Init database");
            }

            LoadStateAndUpdateAsync();

            //ShowBatterySaverAlertAsync();

            //CheckTextScaleFactorAsync();

            base.OnActivate(sender, e);
        }

        private void LoadStateAndUpdateAsync()
        {
            Execute.BeginOnThreadPool(() =>
            {
                var isAuthorized = SettingsHelper.GetValue<string>(Constants.TokenKey);
                if (!string.IsNullOrEmpty(isAuthorized)) return;

                Execute.BeginOnUIThread(() =>
                {
                    var stateService = IoC.Get<IStateService>();

                    var timer = Stopwatch.StartNew();
                });
            });
        }

        protected override void OnDeactivate(object sender, DeactivatedEventArgs args)
        {
#if DEBUG
            Debug.WriteLine("App deactivate");
#endif

            //UpdateMainTile();

            var cacheService = IoC.Get<ICacheService>();
            cacheService.TryCommit();

            //var mtProtoService = IoC.Get<IMTProtoService>();
            var isAuthorized = SettingsHelper.GetValue<string>(Constants.TokenKey);
            if (!string.IsNullOrEmpty(isAuthorized))
            {
               // mtProtoService.UpdateStatusAsync(new TLBool(true), result => { });
            }

            var manualResetEvent = new ManualResetEvent(false);
            ThreadPool.QueueUserWorkItem(state =>
            {
                try
                {

                    //mtProtoService.ClearHistory();
                }
                catch (Exception e)
                {
                    //TLUtils.WriteException(e);
                }
                finally
                {
                    manualResetEvent.Set();
                }
            });
            manualResetEvent.WaitOne(7000);

            base.OnDeactivate(sender, args);
        }

        private static void StartAgent()
        {
            if (ScheduledActionService.Find(Constants.ScheduledAgentTaskName) != null)
            {
                ScheduledActionService.Remove(Constants.ScheduledAgentTaskName);
            }

            var task = new PeriodicTask(Constants.ScheduledAgentTaskName)
            {
                Description = AppResources.TileUpdaterTaskDescription
            };

            try
            {
                ScheduledActionService.Add(task);

#if DEBUG
                // If we're debugging, attempt to start the task immediately 
                // this break our service and updates
                try
                {
                    ScheduledActionService.LaunchForTest(Constants.ScheduledAgentTaskName, new TimeSpan(0, 0, 10));
                }
                catch (Exception e)
                {

                    Debug.WriteLine(e);
                }
#endif
            }
            catch (Exception e)
            {
#if DEBUG
                Debug.WriteLine(e);
#endif
            }

            //SettingsHelper.SetValue(Constants.UnreadCountKey, 0);

        }

        private void ShowBatterySaverAlertAsync()
        {
            Execute.BeginOnThreadPool(() =>
            {
                if (!_showBatterySaverOnce)
                {
                    _showBatterySaverOnce = CheckBatterySaverState();
                }
            });
        }

        private bool _showBatterySaverOnce;

        private bool CheckBatterySaverState()
        {
#if WP8
            // The minimum phone version that supports the PowerSavingModeEnabled property
            var targetVersion = new Version(8, 0, 10492);

            //if (Environment.OSVersion.Version >= targetVersion)
            {
                // Use reflection to get the PowerSavingModeEnabled value
                //var powerSaveEnabled = (bool) typeof(PowerManager).GetProperty("PowerSavingModeEnabled").GetValue(null, null);
                var powerSaveOn = PowerManager.PowerSavingMode == PowerSavingMode.On;
                if (powerSaveOn)
                {
                    Execute.BeginOnUIThread(() => MessageBox.Show(AppResources.BatterySaverModeAlert, AppResources.Warning, MessageBoxButton.OK));
                    return true;
                }

                return false;
            }
#endif

            return true;
        }

        private static void SetupViewLocator()
        {
            ViewLocator.DeterminePackUriFromType = (viewModelType, viewType) =>
            {
                var assemblyName = viewType.Assembly.GetAssemblyName();
                var uri = viewType.FullName.Replace(assemblyName
#if WP8 //NOTE: at WP8 project deafult assembly name TelegramClient.WP8 instead of TelegramClient at WP7
.Replace(".WP8", string.Empty)
#endif
, string.Empty).Replace(".", "/") + ".xaml";

                if (!Application.Current.GetType().Assembly.GetAssemblyName().Equals(assemblyName))
                {
                    return "/" + assemblyName + ";component" + uri;
                }

                return uri;
            };
        }
    }
}
