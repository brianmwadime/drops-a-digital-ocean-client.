﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Drops.ViewModels;
using Drops.Resources;

namespace Drops.Views
{
    public partial class SnapshotsView : PhoneApplicationPage
    {
        private SnapshotsViewModel _viewModel { get { return this.DataContext as SnapshotsViewModel; } }

        private readonly ApplicationBarMenuItem _settingsItem = new ApplicationBarMenuItem
        {
            Text = AppResources.SettingsTitle,
        };

        private readonly ApplicationBarMenuItem _refreshItem = new ApplicationBarMenuItem
        {
            Text = AppResources.Refresh
        };

        public SnapshotsView()
        {
            InitializeComponent();

            Loaded += SnapshotsView_Loaded;
        }

        void SnapshotsView_Loaded(object sender, RoutedEventArgs e)
        {
            GoogleAnalytics.EasyTracker.GetTracker().SendView("Snapshots View");
        }
    }
}