﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace Drops.Views
{
    public class DropsViewBase : PhoneApplicationPage
    {
        // fast resume
#if WP8
        private bool _isFastResume;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Reset)
            {
                _isFastResume = true;
            }

            if (e.NavigationMode == NavigationMode.Refresh
                && _isFastResume)
            {
                _isFastResume = false;

                if (e.Uri.OriginalString.StartsWith("/Protocol?encodedLaunchUri"))
                {
                    
                }
                else if (e.Uri.OriginalString.StartsWith("/PeopleExtension?action=Show_Contact"))
                {
                    
                }
                else if (e.Uri.OriginalString.StartsWith("/Views/Additional/SettingsView.xaml?Action=DC_UPDATE"))
                {
                    
                }
            }

            if (e.NavigationMode == NavigationMode.New)
            {
                if (e.Uri.OriginalString.StartsWith("/Protocol?encodedLaunchUri"))
                {
                    
                }
                else if (e.Uri.OriginalString.StartsWith("/PeopleExtension?action=Show_Contact"))
                {
                    
                }
                else if (e.Uri.OriginalString.StartsWith("/Views/Additional/SettingsView.xaml?Action=DC_UPDATE"))
                {
                    
                }
            }

            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            if (_isFastResume
                && e.NavigationMode == NavigationMode.New
                && (e.Uri.OriginalString.EndsWith("DashboardView.xaml")))
            {
                _isFastResume = false;
                e.Cancel = true;

                if (e.Uri.OriginalString.StartsWith("/Protocol?encodedLaunchUri"))
                {
                    
                }
                else if (e.Uri.OriginalString.StartsWith("/PeopleExtension?action=Show_Contact"))
                {
                    
                }
                else if (e.Uri.OriginalString.StartsWith("/Views/Additional/SettingsView.xaml?Action=DC_UPDATE"))
                {
                    
                }

                return;
            }

            base.OnNavigatingFrom(e);
        }

#endif
    }
}
