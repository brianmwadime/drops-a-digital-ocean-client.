﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Drops.ViewModels;
using Drops.Resources;

namespace Drops.Views
{
    public partial class BackupsView : PhoneApplicationPage
    {
        private BackupsViewModel _viewModel { get { return this.DataContext as BackupsViewModel; } }

        private readonly ApplicationBarMenuItem _settingsItem = new ApplicationBarMenuItem
        {
            Text = AppResources.SettingsTitle,
        };

        private readonly ApplicationBarMenuItem _refreshItem = new ApplicationBarMenuItem
        {
            Text = AppResources.Refresh
        };

        public BackupsView()
        {
            InitializeComponent();
        }
    }
}