﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Drops.ViewModels;
using System.Windows.Media;
using System.Windows.Data;
using Drops.Controls;
using System.Windows.Media.Animation;
using System.Threading;

namespace Drops.Views
{
    public partial class TokenView : PhoneApplicationPage
    {
        private TokenViewModel ViewModel { get { return this.DataContext as TokenViewModel; } }

        private TranslateTransform _frameTransform;

        private static readonly Uri ExternalUri = new Uri(@"app://external/");

        private bool _fromExternalUri;

        public static readonly DependencyProperty RootFrameTransformProperty = DependencyProperty.Register(
            "RootFrameTransformProperty", typeof(double), typeof(TokenView), new PropertyMetadata(OnRootFrameTransformChanged));

        private static void OnRootFrameTransformChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = d as TokenView;
            if (view != null)
            {
                view._frameTransform.Y = 0;
            }
        }

        public double RootFrameTransform
        {
            get { return (double)GetValue(RootFrameTransformProperty); }
            set { SetValue(RootFrameTransformProperty, value); }
        }

        private void SetRootFrameBinding()
        {
            var frame = (Frame)Application.Current.RootVisual;
            _frameTransform = ((TranslateTransform)((TransformGroup)frame.RenderTransform).Children[0]);
            var binding = new Binding("Y")
            {
                Source = _frameTransform
            };
            SetBinding(RootFrameTransformProperty, binding);
        }

        private void RemoveRootFrameBinding()
        {
            ClearValue(RootFrameTransformProperty);
        }

        private bool _isBackwardOutAnimation;
        private bool _isBackwardInAnimation;
        private bool _isForwardInAnimation;
        private bool _isForwardOutAnimation;

        public TokenView()
        {
            InitializeComponent();

            Loaded += (sender, args) =>
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendView("Token View");

                SetRootFrameBinding();

                RunAnimation();

            };

            Unloaded += (sender, args) =>
            {
                RemoveRootFrameBinding();
            };
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.New)
            {
                //LayoutRoot.Opacity = 0.0;
                Logo.Opacity = 0.0;
                ContentPanel.Opacity = 0.0;
                _isForwardInAnimation = true;
            }
            else if (e.NavigationMode == NavigationMode.Back)
            {
                if (!_fromExternalUri)
                {
                    //LayoutRoot.Opacity = 0.0;
                    Logo.Opacity = 0.0;
                    ContentPanel.Opacity = 0.0;
                    _isBackwardInAnimation = true;
                }
                else
                {
                    ViewModel.BackwardInAnimationComplete();
                }
                _fromExternalUri = false;
            }
            else if (e.NavigationMode == NavigationMode.Forward && e.Uri != ExternalUri)
            {
                _isForwardOutAnimation = true;
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            _fromExternalUri = e.Uri == ExternalUri;
            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
        }

        private void Hyperlink_OnClick(object sender, RoutedEventArgs e)
        {

        }

        private void RunAnimation()
        {
            if (_isForwardInAnimation)
            {
                _isForwardInAnimation = false;

                var storyboard = new Storyboard();
                var continuumElementX1 = new DoubleAnimationUsingKeyFrames();
                continuumElementX1.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.0), Value = -300.0 });
                continuumElementX1.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.75), Value = 0.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 3.0 } });
                Storyboard.SetTarget(continuumElementX1, Logo);
                Storyboard.SetTargetProperty(continuumElementX1, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateY)"));
                storyboard.Children.Add(continuumElementX1);

                var continuumElementY = new DoubleAnimationUsingKeyFrames();
                continuumElementY.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.0), Value = -480.0 });
                continuumElementY.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.95), Value = 0.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 3.0 } });
                Storyboard.SetTarget(continuumElementY, ContentPanel);
                Storyboard.SetTargetProperty(continuumElementY, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateY)"));
                storyboard.Children.Add(continuumElementY);

                //var continuumLayoutRootY = new DoubleAnimationUsingKeyFrames();
                //continuumLayoutRootY.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.0), Value = 150.0 });
                //continuumLayoutRootY.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 0.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 3.0 } });
                //Storyboard.SetTarget(continuumLayoutRootY, LayoutRoot);
                //Storyboard.SetTargetProperty(continuumLayoutRootY, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateY)"));
                //storyboard.Children.Add(continuumLayoutRootY);

                var continuumLogoOpacity = new DoubleAnimation
                {
                    From = 0.0,
                    To = 1.0,
                    Duration = TimeSpan.FromSeconds(0.25),
                    EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 6.0 }
                };
                Storyboard.SetTarget(continuumLogoOpacity, Logo);
                Storyboard.SetTargetProperty(continuumLogoOpacity, new PropertyPath("(UIElement.Opacity)"));
                storyboard.Children.Add(continuumLogoOpacity);

                var continuumLayoutRootOpacity = new DoubleAnimation
                {
                    From = 0.0,
                    To = 1.0,
                    Duration = TimeSpan.FromSeconds(0.25),
                    EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 6.0 }
                };
                Storyboard.SetTarget(continuumLayoutRootOpacity, ContentPanel);
                Storyboard.SetTargetProperty(continuumLayoutRootOpacity, new PropertyPath("(UIElement.Opacity)"));
                storyboard.Children.Add(continuumLayoutRootOpacity);

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    Logo.Opacity = 1.0;
                    ContentPanel.Opacity = 1.0;
                    //InputMessage.IsHitTestVisible = false;
                    //InputMessageFocusHolder.Visibility = Visibility.Visible;
                    //_inputMessageDisabled = true;
                    storyboard.Completed += (o, e) =>
                    {
                        ViewModel.ForwardInAnimationComplete();
                    };
                    storyboard.Begin();
                });
            }
            else if (_isBackwardOutAnimation)
            {
                _isBackwardOutAnimation = false;

                var storyboard = new Storyboard();

                var translateAnimation = new DoubleAnimationUsingKeyFrames();
                translateAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.00), Value = 0.0 });
                translateAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.75), Value = -480.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseIn, Exponent = 6.0 } });
                Storyboard.SetTarget(translateAnimation, ContentPanel);
                Storyboard.SetTargetProperty(translateAnimation, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateY)"));
                storyboard.Children.Add(translateAnimation);

                var opacityAnimation = new DoubleAnimationUsingKeyFrames();
                opacityAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.00), Value = 1.0 });
                opacityAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.15), Value = 1.0 });
                opacityAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 0.0 });
                Storyboard.SetTarget(opacityAnimation, ContentPanel);
                Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath("(UIElement.Opacity)"));
                storyboard.Children.Add(opacityAnimation);

                storyboard.Begin();
            }
            else if (_isBackwardInAnimation)
            {
                _isBackwardInAnimation = false;

                var storyboard = DropsTurnstileAnimations.GetAnimation(LayoutRoot, TurnstileTransitionMode.BackwardIn);

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    Logo.Opacity = 1.0;
                    ContentPanel.Opacity = 1.0;
                    storyboard.Completed += (o, e) =>
                    {
                        ViewModel.BackwardInAnimationComplete();
                    };
                    storyboard.Begin();
                });
            }
        }
    }
}