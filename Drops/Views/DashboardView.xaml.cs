﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Drops.ViewModels;
using Microsoft.Xna.Framework;
using Drops.Interactions;
using Drops.Api;
using Drops.Extensions;
using Caliburn.Micro;
using System.Reactive.Linq;
using System.Reactive.Disposables;
using System.Diagnostics;
using Drops.Resources;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Drops.Controls;
using Drops.Api.Models.Responses;

namespace Drops.Views
{
    public partial class DashboardView : PhoneApplicationPage
    {
        private bool _firstRun = true;

        private object _sendertemp;
        Droplet _selecteditem;
        private DashboardViewModel _viewModel { get { return this.DataContext as DashboardViewModel; } }

        //private InteractionManager _interactionManager = new InteractionManager();


        /// <summary>
        /// The _settings appbar menu item
        /// </summary>
        private readonly ApplicationBarMenuItem _settingsItem = new ApplicationBarMenuItem
        {
            Text = AppResources.SettingsTitle,
        };

        private readonly ApplicationBarMenuItem _refreshItem = new ApplicationBarMenuItem
        {
            Text = AppResources.Refresh
        };

        /// <summary>
        /// The _review appbar menu item
        /// </summary>
        private readonly ApplicationBarMenuItem _reviewItem = new ApplicationBarMenuItem
        {
            Text = AppResources.ReviewTitle,
        };

        private Uri _nextUri;

        private static readonly Uri ExternalUri = new Uri(@"app://external/");

        public DashboardView()
        {
            InitializeComponent();

            // Initialize Drawer Layout
            //DrawerLayout.InitializeDrawerLayout();

            _settingsItem.Click += (sender, args) => _viewModel.Settings();

            _refreshItem.Click += (sender, args) => _viewModel.RefreshItems();

            _reviewItem.Click += (sender, args) => _viewModel.Review();

            Loaded += (sender, args) =>
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendView("Dashboard View");
#if WP81
                NavigationService.PauseOnBack = true;
#endif
                var result = RunAnimation((o, e) => ReturnItemsVisibility());
                if (!result)
                {
                    Execute.BeginOnUIThread(ReturnItemsVisibility);
                }

                _viewModel.OnAnimationComplete();

                if (!_firstRun)
                {
                    return;
                }

                _firstRun = false;
            };

            SwipeMenu.IsEnabled = true;

            BuildLocalizedAppBar();
        }

        private void ReturnItemsVisibility()
        {
            
        }

        private bool _isBackwardInAnimation;

        private bool RunAnimation(EventHandler callback)
        {
            if (_isBackwardInAnimation)
            {
                _isBackwardInAnimation = false;

                if (_nextUri != null && _nextUri.ToString().EndsWith("DropletDetailsView.xaml"))
                {
                    var storyboard = new Storyboard();

                    var translateAnimation = new DoubleAnimationUsingKeyFrames();
                    translateAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.00), Value = 150.0 });
                    translateAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.35), Value = 0.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 6.0 } });
                    Storyboard.SetTarget(translateAnimation, LayoutRoot);
                    Storyboard.SetTargetProperty(translateAnimation, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateY)"));
                    storyboard.Children.Add(translateAnimation);

                    var opacityAnimation = new DoubleAnimationUsingKeyFrames();
                    opacityAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.00), Value = 0.0 });
                    opacityAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.35), Value = 1.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 6.0 } });
                    Storyboard.SetTarget(opacityAnimation, LayoutRoot);
                    Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath("(UIElement.Opacity)"));
                    storyboard.Children.Add(opacityAnimation);
                    if (callback != null)
                    {
                        storyboard.Completed += callback;
                    }

                    LayoutRoot.Opacity = 0.0;
                    Deployment.Current.Dispatcher.BeginInvoke(storyboard.Begin);
                    return true;
                }

                if (_nextUri != null
                    && (_nextUri.ToString().EndsWith("AboutView.xaml")
                        || _nextUri.ToString().EndsWith("SettingsView.xaml")))
                {
                    var storyboard = DropsTurnstileAnimations.BackwardIn(LayoutRoot);
                    if (callback != null)
                    {
                        storyboard.Completed += callback;
                    }
                    //var rotationAnimation = new DoubleAnimationUsingKeyFrames();
                    //rotationAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.00), Value = 105.0 });
                    //rotationAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.35), Value = 0.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 6.0 } });
                    //Storyboard.SetTarget(rotationAnimation, LayoutRoot);
                    //Storyboard.SetTargetProperty(rotationAnimation, new PropertyPath("(UIElement.Projection).(PlaneProjection.RotationY)"));
                    //storyboard.Children.Add(rotationAnimation);
                    //if (callback != null)
                    //{
                    //    storyboard.Completed += callback;
                    //}

                    LayoutRoot.Opacity = 1.0;

                    Deployment.Current.Dispatcher.BeginInvoke(storyboard.Begin);
                    return true;
                }
                else
                {
                    ((CompositeTransform)LayoutRoot.RenderTransform).TranslateY = 0.0;
                    LayoutRoot.Opacity = 1.0;
                    return false;
                }
            }
            else
            {
                ((CompositeTransform)LayoutRoot.RenderTransform).TranslateY = 0.0;
                LayoutRoot.Opacity = 1.0;
                return false;
            }

            return false;
        }

        private void BuildLocalizedAppBar()
        {
            if (ApplicationBar != null) return;

            ApplicationBar = new ApplicationBar();
            ApplicationBar.Opacity = 0.65;
            ApplicationBar.StateChanged += ApplicationBar_StateChanged;
            ApplicationBar.Mode = ApplicationBarMode.Minimized;
            ApplicationBar.MenuItems.Add(_settingsItem);
            ApplicationBar.MenuItems.Add(_refreshItem);
            ApplicationBar.MenuItems.Add(_reviewItem);
            //ApplicationBar.MenuItems.Add(_aboutItem);
#if DEBUG

#endif
        }

        void ApplicationBar_StateChanged(object sender, ApplicationBarStateChangedEventArgs e)
        {
            var appBar = sender as ApplicationBar;
            if (appBar == null) return;

            appBar.Opacity = e.IsMenuVisible ? 1 : .65;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back)
            {
                //if (e.EndsWith("DialogDetailsView.xaml"))
                {
                    //LayoutRoot.Opacity = 0.0;
                    _isBackwardInAnimation = true;
                }
            }

            if (e.NavigationMode == NavigationMode.New)
            {
                
            }

            if (_lastTapedItem != null)
            {
                var transform = _lastTapedItem.RenderTransform as CompositeTransform;
                if (transform != null)
                {
                    transform.TranslateX = 0.0;
                    transform.TranslateY = 0.0;
                }
                _lastTapedItem.Opacity = 1.0;
            }

            base.OnNavigatedTo(e);
        }

        private static FrameworkElement _lastTapedItem;

        public static Storyboard StartContinuumForwardOutAnimation(FrameworkElement tapedItem, FrameworkElement tapedItemContainer = null)
        {
            _lastTapedItem = tapedItem;

            _lastTapedItem.CacheMode = new BitmapCache();

            var storyboard = new Storyboard();

            var timeline = new DoubleAnimationUsingKeyFrames();
            timeline.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.0), Value = 0.0 });
            timeline.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 73.0 });
            Storyboard.SetTarget(timeline, tapedItem);
            Storyboard.SetTargetProperty(timeline, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateY)"));
            storyboard.Children.Add(timeline);

            var timeline2 = new DoubleAnimationUsingKeyFrames();
            timeline2.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.0), Value = 0.0 });
            timeline2.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 425.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseIn, Exponent = 5.0 } });
            Storyboard.SetTarget(timeline2, tapedItem);
            Storyboard.SetTargetProperty(timeline2, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateX)"));
            storyboard.Children.Add(timeline2);

            var timeline3 = new DoubleAnimationUsingKeyFrames();
            timeline3.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.0), Value = 1.0 });
            timeline3.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.2), Value = 1.0 });
            timeline3.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 0.0 });
            Storyboard.SetTarget(timeline3, tapedItem);
            Storyboard.SetTargetProperty(timeline3, new PropertyPath("(UIElement.Opacity)"));
            storyboard.Children.Add(timeline3);

            if (tapedItemContainer != null)
            {
                var timeline4 = new ObjectAnimationUsingKeyFrames();
                timeline4.KeyFrames.Add(new DiscreteObjectKeyFrame { KeyTime = TimeSpan.FromSeconds(0.0), Value = 999.0 });
                timeline4.KeyFrames.Add(new DiscreteObjectKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 0.0 });
                Storyboard.SetTarget(timeline4, tapedItemContainer);
                Storyboard.SetTargetProperty(timeline4, new PropertyPath("(Canvas.ZIndex)"));
                storyboard.Children.Add(timeline4);
            }

            storyboard.Begin();

            return storyboard;
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);

            _nextUri = e.Uri;

            if (!e.Cancel)
            {
                if (e.Uri.ToString().EndsWith("DropletDetailsView.xaml"))
                {
                    var storyboard = new Storyboard();

                    var translateAnimation = new DoubleAnimationUsingKeyFrames();
                    translateAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.00), Value = 0.0 });
                    translateAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 150.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseIn, Exponent = 6.0 } });
                    Storyboard.SetTarget(translateAnimation, LayoutRoot);
                    Storyboard.SetTargetProperty(translateAnimation, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateY)"));
                    storyboard.Children.Add(translateAnimation);

                    var opacityAnimation = new DoubleAnimationUsingKeyFrames();
                    opacityAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.00), Value = 1.0 });
                    opacityAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 0.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseIn, Exponent = 6.0 } });
                    Storyboard.SetTarget(opacityAnimation, LayoutRoot);
                    Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath("(UIElement.Opacity)"));
                    storyboard.Children.Add(opacityAnimation);

                    storyboard.Begin();
                }
                else if (e.Uri.ToString().EndsWith("AboutView.xaml")
                    || e.Uri.ToString().EndsWith("SettingsView.xaml"))
                {
                    var storyboard = DropsTurnstileAnimations.ForwardOut(LayoutRoot);

                    storyboard.Begin();
                }

            }
        }


        /// <summary>
        /// Handles the Tapped event of the Item
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="Microsoft.Phone.Controls.GestureEventArgs"/> instance containing the event data.</param>
        private void ItemTapped(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            if (_sendertemp != null)
            {
                FrameworkElement fe = _sendertemp as FrameworkElement;
                SwipeMenu.MenuBounceBack(fe);
            }

            _selecteditem = dropletlist.SelectedItem as Droplet;

            if (_selecteditem != null)
            {
                _viewModel.ViewDroplet(_selecteditem);
            }

            _selecteditem = null;

        }

        /// <summary>
        /// Handles the DragDelta event of the GestureListener control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DragDeltaGestureEventArgs"/> instance containing the event data.</param>
        private void GestureListener_DragDelta(object sender, DragDeltaGestureEventArgs e)
        {
            SwipeMenu.DragDelta(sender, e);
        }

        /// <summary>
        /// Handles the DragCompleted event of the GestureListener control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DragCompletedGestureEventArgs"/> instance containing the event data.</param>
        private void GestureListener_DragCompleted(object sender, DragCompletedGestureEventArgs e)
        {
            SwipeMenu.DragCompleted(sender, e);
        }

        /// <summary>
        /// Handles the Flick event of the GestureListener control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FlickGestureEventArgs"/> instance containing the event data.</param>
        private void GestureListener_Flick(object sender, FlickGestureEventArgs e)
        {
            SwipeMenu.Flick(sender, e);
        }

        /// <summary>
        /// Handles the Hold event of the GestureListener control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Microsoft.Phone.Controls.GestureEventArgs"/> instance containing the event data.</param>
        private void GestureListener_Hold(object sender, Microsoft.Phone.Controls.GestureEventArgs e)
        {
            _selecteditem = dropletlist.SelectedItem as Droplet;

            if (_sendertemp != null)
            {
                FrameworkElement fe = _sendertemp as FrameworkElement;
                SwipeMenu.MenuBounceBack(fe);
            }

            _sendertemp = sender;

            SwipeMenu.Hold(sender, e);
        }

        /// <summary>
        /// Handles the DragStarted event of the GestureListener control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DragStartedGestureEventArgs"/> instance containing the event data.</param>
        private void GestureListener_DragStarted(object sender, DragStartedGestureEventArgs e)
        {
            _selecteditem = dropletlist.SelectedItem as Droplet;

            if (_sendertemp != null)
            {
                FrameworkElement fe = _sendertemp as FrameworkElement;
                SwipeMenu.MenuBounceBack(fe);
            }

            _sendertemp = sender;
        }


        /// <summary>
        /// Handles the Panel event of the Refresh control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Refresh_Panel(object sender, EventArgs e)
        {
            if (_sendertemp != null)
            {
                FrameworkElement fe = _sendertemp as FrameworkElement;
                SwipeMenu.MenuBounceBack(fe);
            }
            _viewModel.LoadDroplets();
#if DEBUG
            Debug.WriteLine("Refresh triggered");
#endif
        }


        /// <summary>
        /// Handles the Tapped event of the DrawerIcon control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.Input.GestureEventArgs"/> instance containing the event data.</param>
        private void DrawerIcon_Tapped(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //if (DrawerLayout.IsDrawerOpen)
            //    DrawerLayout.CloseDrawer();
            //else
            //    DrawerLayout.OpenDrawer();
        }

        public FrameworkElement TapedItem;

        private void dropletlist_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            TapedItem = (FrameworkElement)sender;

            var tapedItemContainer = TapedItem.FindParentOfType<ListBoxItem>();

            var result = _viewModel.ViewDroplet(TapedItem.DataContext as Droplet);
            if (result)
            {
                StartContinuumForwardOutAnimation(TapedItem, tapedItemContainer);
            }
        }

        private void AdFailed_event(object sender, GoogleAds.AdErrorEventArgs e)
        {
#if DEBUG
            Debug.WriteLine("Failed to receive ad with error " + e.ErrorCode);
#endif
        }

    }
}