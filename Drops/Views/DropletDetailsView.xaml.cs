﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Caliburn.Micro;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Drops.ViewModels;
using Drops.Controls;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;
using Drops.Resources;
using System.Diagnostics;

namespace Drops.Views
{
    public partial class DropletDetailsView : PhoneApplicationPage
    {
        private DropletDetailsViewModel _viewModel { get { return this.DataContext as DropletDetailsViewModel; } }

        private readonly ApplicationBarMenuItem _refreshItem = new ApplicationBarMenuItem
        {
            Text = AppResources.Refresh
        };

        private readonly ApplicationBarMenuItem _renameItem = new ApplicationBarMenuItem
        {
            Text = AppResources.Rename
        };

        private readonly ApplicationBarMenuItem _snapshotItem = new ApplicationBarMenuItem
        {
            Text = AppResources.TakeSnapshot
        };

        private readonly ApplicationBarMenuItem _resetItem = new ApplicationBarMenuItem
        {
            Text = AppResources.ResetPassword
        };

        private readonly ApplicationBarMenuItem _viewBackupItem = new ApplicationBarMenuItem
        {
            Text = AppResources.ViewBackup
        };

        private readonly ApplicationBarMenuItem _viewSnaphotItem = new ApplicationBarMenuItem
        {
            Text = AppResources.ViewSnaphot
        };

        private readonly ApplicationBarMenuItem _rebootItem = new ApplicationBarMenuItem
        {
            Text = AppResources.Reboot
        };

        private readonly ApplicationBarMenuItem _kernelItem = new ApplicationBarMenuItem
        {
            Text = AppResources.ChangeKernel
        };

        private readonly ApplicationBarMenuItem _ipv6Item = new ApplicationBarMenuItem
        {
            Text = AppResources.EnableIpv6
        };

        private TranslateTransform _frameTransform;

        public static readonly DependencyProperty RootFrameTransformProperty = DependencyProperty.Register(
            "RootFrameTransformProperty", typeof(double), typeof(DropletDetailsView), new PropertyMetadata(OnRootFrameTransformChanged));

        private static void OnRootFrameTransformChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var view = d as DropletDetailsView;
            if (view != null)
            {
                view._frameTransform.Y = 0;
            }
        }

        public double RootFrameTransform
        {
            get { return (double)GetValue(RootFrameTransformProperty); }
            set { SetValue(RootFrameTransformProperty, value); }
        }

        private void SetRootFrameBinding()
        {
            var frame = (Frame)Application.Current.RootVisual;
            _frameTransform = ((TranslateTransform)((TransformGroup)frame.RenderTransform).Children[0]);
            var binding = new Binding("Y")
            {
                Source = _frameTransform
            };
            SetBinding(RootFrameTransformProperty, binding);
        }

        private void RemoveRootFrameBinding()
        {
            ClearValue(RootFrameTransformProperty);
        }

        public DropletDetailsView()
        {
            InitializeComponent();

            _renameItem.Click += (sender, args) => _viewModel.Rename();
            _rebootItem.Click += (sender, args) => _viewModel.Reboot();
            _resetItem.Click += (sender, args) => _viewModel.Reset();
            _ipv6Item.Click += (sender, args) => _viewModel.EnableIpv6();
            _kernelItem.Click += (sender, args) => _viewModel.ChangeKernel();
            _viewBackupItem.Click += (sender, args) => _viewModel.ViewBackups();
            _viewSnaphotItem.Click += (sender, args) => _viewModel.ViewSnapshots();

            Loaded += (sender, args) =>
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendView("Droplet Details View");

                SetRootFrameBinding();

                RunAnimation();

            };

            Unloaded += (sender, args) =>
            {
                RemoveRootFrameBinding();
            };

            BuildLocalizedAppBar();
        }

        private void BuildLocalizedAppBar()
        {
            if (ApplicationBar != null) return;

            ApplicationBar = new ApplicationBar();
            ApplicationBar.Opacity = 0.65;
            ApplicationBar.StateChanged += ApplicationBar_StateChanged;
            ApplicationBar.Mode = ApplicationBarMode.Minimized;
            ApplicationBar.MenuItems.Add(_renameItem);
            ApplicationBar.MenuItems.Add(_resetItem);
            //ApplicationBar.MenuItems.Add(_snapshotItem);
            ApplicationBar.MenuItems.Add(_rebootItem);
            ApplicationBar.MenuItems.Add(_ipv6Item);
            ApplicationBar.MenuItems.Add(_viewBackupItem);
            ApplicationBar.MenuItems.Add(_viewSnaphotItem);
            //ApplicationBar.MenuItems.Add(_kernelItem);
            //ApplicationBar.MenuItems.Add(_aboutItem);
#if DEBUG

#endif
        }

        private void ApplicationBar_StateChanged(object sender, ApplicationBarStateChangedEventArgs e)
        {
            var appBar = sender as ApplicationBar;
            if (appBar == null) return;

            appBar.Opacity = e.IsMenuVisible ? 1 : .65;
        }

        private bool _isBackwardOutAnimation;
        private bool _isBackwardInAnimation;
        private bool _isForwardInAnimation;
        private bool _isForwardOutAnimation;

        private static readonly Uri ExternalUri = new Uri(@"app://external/");

        private bool _fromExternalUri;
        private bool _suppressNavigation;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.New)
            {
                LayoutRoot.Opacity = 0.0;
                _isForwardInAnimation = true;
            }
            else if (e.NavigationMode == NavigationMode.Back)
            {
                if (!_fromExternalUri)
                {
                    LayoutRoot.Opacity = 0.0;
                    _isBackwardInAnimation = true;
                }
                else
                {
                    _viewModel.BackwardInAnimationComplete();
                }
                _fromExternalUri = false;
            }
            else if (e.NavigationMode == NavigationMode.Forward && e.Uri != ExternalUri)
            {
                _isForwardOutAnimation = true;
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            _fromExternalUri = e.Uri == ExternalUri;
            base.OnNavigatedFrom(e);
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
        }


        private void DropletDetailsView_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_viewModel == null) return;

            if (!NavigationService.BackStack.Any())
            {
                e.Cancel = true;
                _viewModel.NavigateToDashboardViewModel();

                return;
            }

            _isBackwardOutAnimation = true;

            RunAnimation();
        }

        private void RunAnimation()
        {
            if (_isForwardInAnimation)
            {
                _isForwardInAnimation = false;

               
                var storyboard = new Storyboard();
                var continuumElementX = new DoubleAnimationUsingKeyFrames();
                continuumElementX.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.0), Value = 130.0 });
                continuumElementX.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 0.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 3.0 } });
                Storyboard.SetTarget(continuumElementX, DropletName);
                Storyboard.SetTargetProperty(continuumElementX, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateX)"));
                storyboard.Children.Add(continuumElementX);

                var continuumElementY = new DoubleAnimationUsingKeyFrames();
                continuumElementY.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.0), Value = -40.0 });
                continuumElementY.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 0.0 });
                Storyboard.SetTarget(continuumElementY, DropletName);
                Storyboard.SetTargetProperty(continuumElementY, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateY)"));
                storyboard.Children.Add(continuumElementY);

                var continuumLayoutRootY = new DoubleAnimationUsingKeyFrames();
                continuumLayoutRootY.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.0), Value = 150.0 });
                continuumLayoutRootY.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 0.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 3.0 } });
                Storyboard.SetTarget(continuumLayoutRootY, LayoutRoot);
                Storyboard.SetTargetProperty(continuumLayoutRootY, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateY)"));
                storyboard.Children.Add(continuumLayoutRootY);

                var continuumLayoutRootOpacity = new DoubleAnimation
                {
                    From = 0.0,
                    To = 1.0,
                    Duration = TimeSpan.FromSeconds(0.25),
                    EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseOut, Exponent = 6.0 }
                };
                Storyboard.SetTarget(continuumLayoutRootOpacity, LayoutRoot);
                Storyboard.SetTargetProperty(continuumLayoutRootOpacity, new PropertyPath("(UIElement.Opacity)"));
                storyboard.Children.Add(continuumLayoutRootOpacity);

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    LayoutRoot.Opacity = 1.0;
                    //InputMessage.IsHitTestVisible = false;
                    //InputMessageFocusHolder.Visibility = Visibility.Visible;
                    //_inputMessageDisabled = true;
                    storyboard.Completed += (o, e) =>
                    {
                        _viewModel.ForwardInAnimationComplete();
                    };
                    storyboard.Begin();
                });
            }
            else if (_isBackwardOutAnimation)
            {
                _isBackwardOutAnimation = false;

                var storyboard = new Storyboard();

                var translateAnimation = new DoubleAnimationUsingKeyFrames();
                translateAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.00), Value = 0.0 });
                translateAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 150.0, EasingFunction = new ExponentialEase { EasingMode = EasingMode.EaseIn, Exponent = 6.0 } });
                Storyboard.SetTarget(translateAnimation, LayoutRoot);
                Storyboard.SetTargetProperty(translateAnimation, new PropertyPath("(UIElement.RenderTransform).(CompositeTransform.TranslateY)"));
                storyboard.Children.Add(translateAnimation);

                var opacityAnimation = new DoubleAnimationUsingKeyFrames();
                opacityAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.00), Value = 1.0 });
                opacityAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.15), Value = 1.0 });
                opacityAnimation.KeyFrames.Add(new EasingDoubleKeyFrame { KeyTime = TimeSpan.FromSeconds(0.25), Value = 0.0 });
                Storyboard.SetTarget(opacityAnimation, LayoutRoot);
                Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath("(UIElement.Opacity)"));
                storyboard.Children.Add(opacityAnimation);

                storyboard.Begin();
            }
            else if (_isBackwardInAnimation)
            {
                _isBackwardInAnimation = false;

                var storyboard = DropsTurnstileAnimations.GetAnimation(LayoutRoot, TurnstileTransitionMode.BackwardIn);

                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    LayoutRoot.Opacity = 1.0;
                    storyboard.Completed += (o, e) =>
                    {
                        _viewModel.BackwardInAnimationComplete();
                    };
                    storyboard.Begin();
                });
            }
        }

        private void AdFailed_event(object sender, GoogleAds.AdErrorEventArgs e)
        {
#if DEBUG
            Debug.WriteLine("Failed to receive ad with error " + e.ErrorCode);
#endif
        }
    }
}